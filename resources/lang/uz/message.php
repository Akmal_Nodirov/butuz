<?php
	return [
		'menu_home' => 'Bosh sahifa',
        'menu_introduction' => 'Universitet haqida',
        'menu_education' => 'Ta`lim',
        'menu_news' => 'Yangiliklar',
        'menu_announcement' => 'E`lon',
        'menu_gallery' => 'Galereya',
        'menu_apply' => 'Ariza',
        'menu_contact_us' => 'Bog`lanish',
        'menu_entrance' => 'Universitet <br>Portali',
        'menu_faq' => 'T.S.S',
        'university_name' => 'Toshkent shahridagi Puchon universiteti',
        'welcome' => 'Toshkent shahridagi Puchon universitetiga xush kelibsiz',
        'announcement' => 'E`lon',
        'announcements' => 'E`lonlar',
        'read_more' => 'Batafsil',
        'programs_of_study' => 'O`quv dasturlari',
        'latest_announcements' => 'So`nggi e`lonlar',
        'latest_news' => 'So`nggi yangiliklar',
        'all_news' => 'Barcha yangiliklar',
        'frequently_asked_questions' => 'Tez so`raladigan savollar',
        'all_questions' => 'Barcha savollar',
        'follow_us_on' => 'Kuzatish',
        'contact_us' => 'Bog`lanish',
        'enter_your_search_term' => 'Izlagan narsangizni kiriting...',
        'portal_system' => 'Portal tizimi',
        'more' => 'Yana',
        'about_us' => 'Biz haqimizda',
        'administration' => 'Ma`muriyat',
        'telephone_number' => 'Telefon raqam',
        'faculties' => 'Fakultetlar',
        'what_are_we_professionals' => 'Bizning professionaligimiz',
        'university_programs' => 'Universitet programmalari',
        'archives' => 'Arxivlar',
        'archive' => 'Arxiv',
        'year' => 'Yil',
        'all_latest_announcements' => 'Barcha so`nggi e`lonlar',
        'all_news' => 'Barcha yangiliklar',
        'news' => 'Yangiliklar',
        'answer' => 'Javob',
        'all_questions' => 'Barcha savollar',
        'our_location' => 'Bizning manzil',
        'call_us_on' => 'Qo`ng`iroq qilish',
        'send_a_message' => 'Xabar jo`natish',
        'social_media' => 'Ijtimoiy media',
        'leave_a_message' => 'Xabar qoldirish',
        'feel_free_to_contact_us' => 'Bizga bilan bog`lanish',
        'your_name' => 'Ismingiz',
        'phone' => 'Telefon',
        'your_email' => 'Email adresingiz',
        'subject' => 'Mavzu',
        'message' => 'Xabar',
        'send_message' => 'Xabarni jo`natish',
        'events_and_gallery' => 'Tadbirlar va galereya',
        'event' => 'Tadbirlar',
        'rights_and_duties' => 'Universitet qoidalari',
        'apply_fixed' => 'O\'qishga ariza topshirish',
        
        /* choose_application (PAGE) */
        'main_page_but' => 'Toshkent shahridagi Puchon universiteti bosh sahifasi',
        'go_to_main_page' => 'Saytning bosh sahifasiga o`tish',
        'for_applicant' => 'Abituriyentlar uchun',
        'go_to_online_application' => 'Onlayn arizaga o`tish',
        
        // Modal 
        'modal_title' => "Birinchi bosqich kirish imtihonlari bo'lib o'tadigan sana",
        'modal_text_1' => "Aziz abiturientlar,",
        'modal_text_2' => "Kirish imtihonlarining birinchi bosqichi (test sinovi) 2019 yil 15-16 aprel kunlari O'zbekiston Respublikasi Vazirlar Mahkamasi huzuridagi Davlat test markazida o'tkaziladi.",
        'modal_text_korean' => "Koreys tili va Menejment yo'nalishida 15-aprel, 14:30.",
        'modal_text_korean_button' => "Koreys tili va Menejment yo'nalishi",
        'modal_text_architect' => "Arxitektura yo'nalishida 16-aprel, 14:30",
        'modal_text_architect_button' => "Arxitektura yo'nalishi",
        'modal_text_fulltime' => "Maktabgacha ta'lim (Kunduzgi bo'lim) yo'nalishida  16-aprel, 9:30 va 14:30 ( to'liq jadvalni ko'ring).",
        'modal_text_fulltime_button' => "Maktabgacha ta'lim yo'nalishi (Kunduzgi bo'lim)",
        'modal_text_evening' => "Maktabgacha ta'lim (Kechki bo'lim) yo'nalishida 16-aprel, 14:30.",
        'modal_text_evening_button' => "Maktabgacha ta'lim yo'nalishi (Kechgi bo'lim)",
        'modal_text_3_4' => "Imtihon o'tkaziladigan manzil- Davlat Test Markazi. (Toshkent, Yunusobod tumani, Bog'ishamol ko'chasi, 12)",
        'modal_text_04' => "ESLATMA!",
        'modal_text_4' => " Abiturientlarda bolishi kerak: passport asl nusxasi (pasport nusxasi ruxsat etilmaydi), hujjat topshirilgandan so'ng olingan ro'yxatga olinganlik hujjati, ko'k rangli ruchka.",
        'modal_text_5' => "Forma yo'q, ammo abiturientning ko'rinishi toza bo'lishi kerak (plyaj kiyimlari va poyafzal ruxsat berilmaydi).",
        'modal_text_6' => "Barcha aloqa vositalari yoki boshqa televizor va  radioaloqa qurilmalari qat'iyan taqiqlanadi.",
        'modal_text_7' => "Zarur bo'lganda, universitet xodimlari siz uchun sinovni tasdiqlovchi sertifikat, hozirgi o'qish joyiga taqdim etilishi uchun (testdan so'ng universitet xodimlari bilan bog'lanishlari mumkin) sertifikat berishi mumkin.",
        'modal_close' => "Yopish",
        'modal_text_8' => "Hurmat bilan,",
        'modal_text_9' => "Toshkentdagi Bucheon universiteti ma'muriyati.",
        'test_opublikovani' => "Test javoblari <span>22.04.2019</span> (Dushanba) kuni e'lon qilinadi",
        // Modal_2
        
        'modal_t1' => "Hurmatli abituriyentlar,",
        'modal_tex01' => "Universitetimizga kirish imtihonlarining birinchi bosqichi natijalarini taqdim etishdan mamnunmiz.",
        'modal_tex02' => "O’z natijalaringizni ko’rish uchun <a target='blank' href='http://cabinet.dtm.uz/site/login'> cabinet.dtm.uz </a> saytiga kiring hamda o’z passport nomeringiz va sayt ostida chiqadigan kodni kiriting.",
        'modal_tex03_bold' => "Suhbatga test topshirgan barcha abituriyentlar va shuningdek, til sertifikati(IELTS/TOPIK/CEFR)ga ega bo’lganlar kiritiladi.",
        'modal_tex04_bold' => "Suhbat o’tkazilish sanasi: 26-aprel. Suhbatning batafsil jadvali dushanba(22-aprel) kuni davomida e’lon qilinadi.",
        'modal_tex_ul1' => "O’zingiz bilan passport hamda ro’yxatdan o’tgan qog’ozingizni olib kelishingiz zarur (test kuni ro’yxatdan o’tgan qog’ozini qoldirib ketgan abituriyentlar suhbat kuniga qadar universitet xodimlariga murojaat qilishlari lozim).",
        'modal_tex_ul2' => "Forma yo'q, ammo abiturientning ko'rinishi tartibli bo'lishi kerak (sohil uchun mo’jallangan kiyimlar va poyafzallarga ruxsat berilmaydi).",
        'modal_tex_ul3' => "Barcha aloqa vositalari yoki boshqa tele va  radioaloqa qurilmalari qat'iyan taqiqlanadi.",
        'modal_tex_ul4' => "Zarur bo'lganda, hozirgi o'qish joyingizga taqdim etish uchun universitet xodimlari sinovni tasdiqlovchi sertifikat berishi mumkin (testdan so'ng universitet xodimlari bilan bog'lanishlari mumkin).",
        'modal_foot_1' => "Hurmat bilan, ",
        'modal_foot_2' => "Toshkent shahridagi Puchon Universiteti ma'muriyati.",
        
        
        /* Application_print (PAGE) */
    'print' => 'Chop etish',
    'registr_number' => "Ro`yhatdan o`tgan raqami",
    'famil_y' => 'Familiya',
    'first_name' => 'Ism',
    'date_birth' => "Tug'ilgan sana (yil, oy, kun)",
    'nationalit_y' => 'Fuqaroligi',
    'pass_serial_number' => 'Pasport seriya raqami (Parol)',
    'gende_r' => 'Jinsi',
    'tel_numb' => 'Telefon raqami (Login)',
    'el_addr' => 'Elektron manzili ',
    'lan_group' => 'Guruh tili',
    'addr_es' => 'Uy manzili ',
    'fac_name' => "Yo`nalish nomi",
    'name_comp' => 'Tashkilot nomi',
    'info_edu' => "O'rta ta'lim (kollej, litsey, maktab va boshqalar) haqida ma'lumot",
    'expir_date' => 'Tugash muddati',
    'special_ity' => 'Mutaxassisligi ',
    'fam_nam_parent' => 'Ota-ona FISHi',
    'par_ents' => 'Oilaviy munosabati ',
    'em_addrs' => 'Elektron manzili ',
    'pdf_upload_title' => 'Hujjatlarni faqat PDF formatida yuklash lozim',
    'vash_login' => "Sizning loginingiz:",
    'vash_parol' => "Sizning parolingiz:",
    'login_url' => "O'z ma'lumotlaringizni o'zgartirish va kvitansiyani chop etish uchun  www.but.uz/login sahifasiga kiring",
    
    
       /* APPLICATION (PAGE) */
    'app_form_adm' => "O'qishga kirish uchun ariza",
    'upl_req_doc' => 'Hujjatlarni onlayn tarzda yuklash',
    'edu_back' => 'Tahsil olgan joyi',
    'hig_sch' => "O'rta ma'lumoti",
    'higs_col_ly' => 'Nomi (kollej, litsey yoki maktab)',
    'tel_numb_parent' => 'Ota-ona telefon raqami',
     'unvers' => 'Universitet portali',
    'unvers_p' => 'Universitet portaliga kirish',
    'gradu_year' => 'Tugatgan yili',
    'major_ma' => 'Mutaxassisligi',
    'univ_inf' => "Oliy ta'lim ma'lumoti",
    'univ_name' => 'Universitet nomi',
/*    'univ_grad_ye' => 'University Graduation Year',*/
    'unv_maj' => 'Mutaxassisligi',
    'lan_skl1' => 'Lingvistik qobiliyati',
    'exa_min' => 'Imtihon nomi',
    'org_iss_ue' => "Imtihon o'tkazgan tashkilot",
    'gra_de' => 'Daraja',
    'sc_ore' => 'ball',
    'lan_skl' => 'Lingvistik qobiliyati',
    'par_gua_inf' => "Ota-onasi haqidagi ma'lumot",
    'nam_sur' => 'Ota-ona FISHi',
    'date_edu_end' => 'Sana',
    'send_app' => 'Arizani yuborish',
    'notice_text' => "Onlayn arizaga saralash komissiyasiga hujjatlar topshirilgunga qadar o'zgartirish kiritilishi mumkin.",
    
    'bl_appl_mai' => 'Barcha hujjatlar skanerlangan holatda bo‘lishi kerak. Ro‘yxatga olish to‘lovi turar joyga yaqin bankda to‘lanishi lozim. 
Hisob raqami: 20 214 000 800 903 308 001,
Kapital bank, Sirg‘ali filiali
 МФО: 01042, ИНН: 305648748',
     'err_va1' => 'Pasportga muvofiq to\'ldiring',
    
    
    
    
      /* agreement (PAGE) */
    'first_stud' => '2019-2020 – o`quv yili uchun birinchi qabul jarayoni',
    'offline_type' => 'Oflayn hujjatlar topshirish',
    'online_type' => 'Onlayn hujjatlar topshirish',
    'facult_stea' => 'Yo’nalishlar va kvotalar soni: ',
    'fac_prescl' => 'Maktabgacha ta`lim yo`nalishi ( kunduzgi bo`lim)',
    'fac_ev' => 'Maktabgacha ta`lim yo`nalishi ( kechki bo`lim)',
    'fac_arch' => 'Arxitektura yo`nalishi',
    'fac_kore' => 'Koreys tili va Menejment yo`nalishi',
    'req_doc' => 'Kerakli hujjatlar ro’yxati',
    'onl_ap' => 'Onlayn ariza',
    'cop_ats' => 'O’rta ta’lim muassasasi attestat nusxasi yoki oxirgi 
                    ta’lim muassasasidan ma’lumotnoma ',
    // 'cop_pas' => 'Passport nusxasi',
    'cop_inn' => 'STIR (INN) nusxasi',
     'cop_of_dip' => 'Diplom',
     
    'cop_of_pa' => 'Pasport nusxasi',
 'phot_o' => 'Rasm (3*4 / 3.5*4.5)',
    'phot_3x4' => '3x4 shakldagi rasm – 3 dona',
    'time_prem' => 'Qabul vaqti',
    'pon_pya' => '	Dushanba – Juma, 10:00 – 17:00',
    'tim_prem2' => 'Qabul vaqti',
    'fullt' => '24 soat davomida',
    'lea_day' => 'Dam olish 
                    kunlari',
    'srok_pod' => 'Hujjatlar topshirish: 4 martdan – 5 aprelgacha',
    'sunday_ye' => 'Shanba, Yakshanba, 8, 21, 22 mart kunlari',
    'vyxod_dni' => 'Dam olish 
                    kunlari',
    'bez_vyx' => 'Dam olish kunlarisiz',
    'spos_poda' => 'Hujjatlarni 
                    topshirish usuli',
    'zap_onl' => '- Onlayn arizani www.but.uz saytida 
                      to’ldirish ,<br>
                    - Hujjatlarni qabul komissiyasiga 
                      abiturientning shaxsan o’zi topshirishi,<br>
                    - Ro’yxatdan o’tganlik kvitansiyasini olish.',
    'spos_podac' => 'Hujjatlarni 
                        topshirish usuli',
    'zap_on_zaya' => '- www.but.uz saytida onlayn ariza  
                                  to`ldirish;<br>
                                - hujjatlarning skanerlangan nusxasini 
                                  onlayn arizaga  biriktirish;<br>
                                - ro`yxatdan o`tish uchun badalni to`laganlik
                                  haqidagi bank kvitansiyasini +99895 1963601 
                                  telefon raqamiga telegram orqali yuborish;<br>  
                                - ro`yxatdan o`tganlik kvitansiyasini 
                                  telegramdan qabul qilish;<br>
                                * Ro`yxatga olish to`lovini quyidagi bank 
                                  hisob raqamiga to`lanadi: 
                                  Hisob raqami: 20 214 000 800 903 308 001, 
                                  Kapital bank Sirg`ali filiali,  
                                  Bank kodi: 01042, STIR (ИНН): 305648748',
    'ad_re_s' => 'Manzil',
    'uch_cen_hag' => '«HAGWON» o’quv markazi 
                        Toshkent, Mirobod tumani, 
                        Afrosiyob ko’chasi, 6<br>
                        Mo’ljal: “Sal Sal” restorani orqasida',
    'vzn_regs' => 'Ro’yxatga olish to’lovi',
    'ot_na_dan' => 'Ro’yxatga olish to’lovini quyidagi bank 
                      hisob raqmiga to’lanadi 
                      hisob raqam: 20 214 000 800 903 308 001, 
                      Kapital bank Sirg’ali filiali,  
                      Bank kodi: 01042, STIR (INN): 305648748',
    'pro_t_s' => 'Jarayon',
    'pocedura_title' => "Hujjatlarni qabul qilish va o`qishga kirish jarayoni",
    'sr_ok' => 'Muddatlar',
    'sod_erj' => 'Mazmuni',
    'primech' => 'Qo’shimcha ma’lumotlar',
    'onl_zaya_v' => 'Onlayn ariza',
    'date_tim_e' => '6 martdan - 5 aprelgacha',
    'on_re_g_sayt' => 'Onlayn ariza topshirish www.but.uz sayti orqali 
                        amalga oshiriladi',
    'off_on_pod' => 'Onlayn va oflayn 
                        Hujjatlarni qabul qilish',
    'date_time_e2' => '6 martdan - 5 aprelgacha',
    'sdac_docc' => 'Qabul kommisiyasiga hujjat
                    topshirish jarayoni ',
    'reg_vzns' => "<b>Barcha kerakli hujjatlar bo'lishi shart</b><br>Ro'yhatga olish to'lovi: EKIH ni 1 barobari (203 000 sum)",
    'test_irov' => 'Test',
    'date_tim_e3' => '13 - 19 aprel',
    'ponap_dosh' => '«Maktabgacha ta’lim» , «Arxitektura» 
                        yo’nalishlari bo’yicha : 
                        Ingliz tilida <br> «Koreys tili va Menejment»
                         yo’nalishi bo’yicha : 
                        Koreys tilida',
    'usl_osv' => 'Yozma imtihon topshirishdan ozod 
                    bo`lish shartlari:',
    'pona_pra' => '«Maktabgacha ta`lim », «Arxitektura» 
                    yo`nalishilari bo`yicha: Ingliz tili darajasi
                     IELTS 5, CEFR B1  sertifikatiga yoki undan 
                    yuqori darajasiga ega bo`lganlar',          
    'pona_kor' => '«Koreys tili va Menejment»  yo`nalishi 
                    bo`yicha: Koreys tili darajasi TOPIK 3
                    sertifikatiga  yoki undan yuqori 
                    darajaga ega bo`lganlar ',
    'sobes_nie' => 'Suhbat',
    'date_tim_e4' => '26 - 27 aprel',
    'sobes_pered' => 'Universitet o`qtuvchilari bilan suhbat ',
    'rez_obya' => 'Natijalarni e’lon
                    qilish',
    'rez_univer' => 'Natijalar Universitet saytida e`lon 
                    qilinadi',
    'predopl_kontr' => 'Shartnoma to`lovini 
                        oldindan to`lash',
    'date_tim_e5' => '6 - 17 may',
    'pred_op_l' => "Shartnoma pulining oldindan 
                    to`lanadigan miqdori 2 million so`m.
                    3-iyundan 7-iyungacha",
    'title_koreys_2_2' => "Koreys tili va Menejment yo'nalishi (2 + 2)",
    'fac_doshk' => "Maktabgacha ta`lim yo'nalishi (3 + 1) 
                    (kunduzgi va kechki bo`limlar)",
    'stoim_kon' => "O'qish uchun oldindan shartnoma 
                    to’lovi 2 million so’m",
    'but_3_goda' => '3 yil  BUT da: BUT bakalavr diplomi + Associate 
                    Bachelor Degree of Bucheon University 
                    (Republic of Korea)',
    'but_1_goda' => "1 yil: Puchon universiteti Bakalavr diplomi 
                    (Koreya Respublikasi)<br><br> 
                    Ishga joylashishdagi afzalliklar: mamlakat bo`yicha 
                    maktabgacha ta’lim muassasalariga ishga joylashish, 
                    maktabgacha ta’lim tizimi bo`yicha ta’lim berish 
                    dasturlarini ishlab chiquvchi muassasalarda ishlash 
                    imkoniyati mavjud.",
    'stoim_kontr' => "O'qish uchun shartnoma puli narxi",
    'two_year' => '2 yil: BUT Associate Bachelor Degree  + 
                    Associate Bachelor Degree of Bucheon University 
                    (Republic of Korea)',
    'two_year_but' => '2 yil Puchon universitetida 
                        (Кoreya Respublikasi) – 
                        Puchon Universiteti 
                        bakalavr diplomi
                        (Koreya Respublikasi)',
    'two_year_but2' => '2 yil  BUT da – BUT 
                        bakalavr diplomi',
    'pers_pekt_iv' => 'Ishga joylashishdagi afzalliklar:  koreys korxonalariga 
                        ishga joylashish imkoni O`zbekistonda ham Koreya 
                        Respublikasida ham mavjud.',
    'arch_fac_3' => 'Arxitektura yo’nalishi  (3 + 1)',
    'st_con_tr' => "O'qish uchun shartnoma puli narxi",
    'one_year_but' => '1 yil Puchon universitetida 
                        (Koreya Respublikasi):  
                        Puchon Universiteti 
                        bakalavr diplomi 
                        (Koreya Respublikasi)',
    'but_one_year_dip' => '1 yil  BUT da:  
                            BUT bakalavr diplomi',
    'pers_pekt_trud' => 'Ishga joylashishdagi afzalliklar:  loyihalashtirish, 
                            qurilish yoki qurilish mollari ishlab chiqarish 
                            yo`nalishlarida ish o`rinlarini topish, undan 
                            tashqari, qurilish bo`yicha viloyatlar va shahar 
                            tashkilotlarida ishlash imkoniyati mavjud.',
    'count_nu' => 'Davom etish',
    'tocontinue' => "Yuqoridagi barcha ma'lumotlar bilan tanishdim, arizani toldirishga tayyorman",
    
    'fak_arch_info' => '3 yil: BUT bakalavr diplomi + Associate Bachelor 
                        Degree of Bucheon University (Republic of Korea) ',
    'people' => ' ta',
    'vgod' => 'Yiliga',
    'ariza_yuborildi' => 'Sizning arizangiz muvaffaqiyatli saqlandi va yuborildi',
    'ariza_ozgartirildi' => 'Sizning arizangiz muvaffaqiyatli tahrirlandi',
    'stoim_kontr1' => "BUT da o'qish uchun kontrakt to'lovi-<span> 2,200 / Yiliga</span> Janubiy Koreadagi Puchon universitetining tarifiga asoslanib (BUT studentlari uchun -30% ",
    'stoim_kontr2' => "BUT da o'qish uchun kontrakt to'lovi-<span> 3,300 / Yiliga</span> Janubiy Koreadagi Puchon universitetining tarifiga asoslanib (BUT studentlari uchun -30% ",
	];