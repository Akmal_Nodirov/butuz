@extends('layouts.app')
@section('title',$announcement['title_'.app()->getLocale()])
@section('content')
<!-- Announcement -->

	<div class="container-fluid eventlist upcoming-event latest-blog no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="section-header">
				<h3>{{__('message.announcement')}}</h3>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-6 col-xs-6 content-area">
					<article class="type-post">
						<h3 class="padding_10">{{ $announcement['title_'.app()->getLocale()] }}</h3>
						<p class="f_family c_code">
							<span><i class="fa fa-calendar-alt"></i>{{$created_at->format('d-m-Y')}}-{{__('message.year')}}</span>
							<span><i class="fa fa-clock"></i>{{$created_at->format('H:i')}}</span>
						</p>
						<img src="{{asset('/storage/announcements/'.$announcement->image)}}" class="img_news" alt="{{ $announcement['title_'.app()->getLocale()] }}">
						{!! $announcement['content_'.app()->getLocale()] !!}
					</article>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6 widget-area">
					<aside class="widget widget_similar_event">
						<div class="widget-title">
							<span class="fa fa-file-archive"></span>
							<h3>{{__('message.archives')}}</h3>
						</div>						
						<ul>
							@foreach($archive as $key => $archive)
								<li><a title="{{__('message.archive')}}-{{$archive->year}}" href="{{url('archive/announcement/'.$archive->year)}}"><span class="fa fa-file-archive"></span><h4>{{__('message.archive')}}-{{$archive->year}}</h4></a></li>
							@endforeach
						</ul>
					</aside>
					
				</div>
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Event List /- -->

	<!-- NEWS Section /- -->

@endsection