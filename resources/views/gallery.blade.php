@extends('layouts.app')
@section('title',__('message.menu_gallery'))
@section('content')
	<div class="container-fluid page-banner banner_gallery about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{__('message.menu_gallery')}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{__('message.menu_gallery')}}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- PageBanner /- -->
	<div class="container">
		<div class="row pt">
			<div class="col-lg-12 text-center pt pb">
				<h1>{{$event['title_'.app()->getLocale()]}}</h1>
				{!! $event['content_'.app()->getLocale()] !!}
			</div>
		</div>
		<div class="row pt">
			<div class="col-lg-12 pt">
				<div class="gallery_container">
					<div class="gallery d-lg-flex flex_wrap justify-content-center">
						@foreach($images as $key => $image)
							<a href="{{asset('/storage/images/'.$image->image)}}" data-fancybox="gallery">
								<img src="{{asset('/storage/images/'.$image->image)}}" alt="">
							</a>
							@if($key+1%4 == 0)
									</div>
							</div>

							<div class="gallery_container">
								<div class="gallery d-lg-flex justify-content-center">
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@push('js')
	<!-- FancyBox -->
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js"></script>
@endpush