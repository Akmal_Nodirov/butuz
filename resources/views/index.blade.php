@extends('layouts.app')
@section('title',__('message.university_name'))
@section('content')
<!-- Slider Section -->
    <div id="slider-section" class="slider-section container-fluid no-padding">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                @foreach($slider as $key => $slide)
                    <div class="item {{$key==0 ? 'active' : ''}}">
                        <img src="{{Storage::url('slider/'.$slide->slide)}}" alt="{{__('message.university_name')}}" width="1720" height="770"/>
                    </div>
                @endforeach
            </div>
            <!-- Controls -->
            <div class="container">
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <i style="    padding: 8px 14px;" class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div><!-- Slider Section /- -->

    <!-- Howwecan Section -->
    <div class="container-fluid no-padding howwecan-section">
        <div class="section-padding"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="d-lg-flex justify-content-between">
                        <div class="img-box">
                            <img src="but/11.jpg" alt="howwecan1" />
                        </div>
                        <div class="img-box">
                            <img src="but/12.jpg" alt="howwecan1" />
                        </div>
                        <div class="img-box">
                            <img src="but/13.jpg" alt="howwecan1"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 col-xs-12">
                    <div class="howwecan-right about_">
                        <div class="section-header">
                            <h3>{{__('message.welcome')}}</h3>
                        </div>

                        <p>{!! str_limit(strip_tags($about['content_'.app()->getLocale()]),400,'...') !!}</p>

                        <a href="{{url('/about/1')}}" class="more_b" title="{{__('message.more')}}">{{__('message.read_more')}}</a>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-padding"></div>
</div><!-- Howwecan Section /- -->

<!-- PROGRAMS OF STUDY Section -->
<div class="section-header">
    <h2 style="font-size:34px; text-transform: uppercase">{{__('message.programs_of_study')}}</h2>
    <!--<span>Our Schedule table</span>-->
</div>
<div class="container-fluid no-padding introduction-section">
    <div class="introduction-carousel">
        @foreach($programs as $program)
        <div class="col-md-4 no-padding">
            <div class="introduction-block">
                <a href="{{url('/program/'.$program->id)}}"><center><img src="{{asset('storage/programs/'.$program->image)}}">
                    <h3 style="text-align: center" class="block-title">{{$program['title_'.app()->getLocale()]}}</h3></center>

                    <!--{!! str_limit(strip_tags($program['description_'.app()->getLocale()]),20,'...') !!}-->

                </a>

            </div>
        </div>
        @endforeach    
    </div>
</div><!-- PROGRAMS OF STUDY Section /- -->

            <!-- Announcements -->
            <div class="container-fluid latest-blog latest-blog-section no-padding">
                <div class="section-padding"></div>
                <div class="container">
                    <div class="section-header">
                        <h3>{{__('message.latest_announcements')}}</h3>

                    </div>
                    <div class="row">
                        @foreach($announcements as $key => $announcement)
                            <?php $created_at = new \datetime($announcement->created_at) ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <article class="type-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="entry-cover news_">
                                            <a href="{{url('/announcement/'.$announcement->id)}}">
                                                <img src="{{asset('/storage/announcements/'.$announcement->image)}}" alt="{{$announcement['title_'.app()->getLocale()]}}"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="entry-title">
                                            <a href="{{url('/announcement/'.$announcement->id)}}" title="{{$announcement['title_'.app()->getLocale()]}}">
                                                <h3>{{$announcement['title_'.app()->getLocale()]}}</h3>
                                            </a>
                                        </div>
                                        <div class="entry-meta">
                                            <div class="post-date">
                                                <p>{{$created_at->format('M')}} <span>{{$created_at->format('d')}}</span></p>
                                            </div>
                                            <div class="post-metablock">
                                                <div class="post-time">
                                                    <span><i class="fa fa-clock"></i>{{$created_at->format('H:i')}}</span>
                                                </div>
                                                <div class="post-location">
                                                    <span><i class="fa fa-calendar-alt"></i>{{$created_at->format('Y')}}-year</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-content">
                                            <p> {!! str_limit(strip_tags($announcement['content_'.app()->getLocale()]),170,'...') !!}</p>
                                        </div>
                                        <a href="{{url('/announcement/'.$announcement->id)}}" class="learn-more" title="{{__('message.read_more')}}">{{__('message.read_more')}}</a>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                        <div class="clear_"></div>
                        <center><a href="{{url('/announcements')}}" class="more_b" title="{{__('message.all_latest_announcements')}}">{{__('message.all_latest_announcements')}}</a></center>

                    </div>
                </div>
                <div class="section-padding"></div>
            </div><!-- Announcements /- -->


            <!-- Latest News -->
            <div class="container-fluid latest-blog latest-blog-section no-padding">
                <div class="section-padding"></div>
                <div class="container">
                    <div class="section-header">
                        <h3>{{__('message.latest_news')}} </h3>
                    </div>
                    <div class="row">
                        @foreach($news as $key => $news)
                            <?php $created_at = new \datetime($news->created_at) ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <article class="type-post">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="entry-cover news_">
                                            <a href="{{url('/news/'.$news->id)}}">
                                                <img src="{{asset('/storage/news/'.$news->image)}}" alt="{{$news['title_'.app()->getLocale()]}}"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="entry-title">
                                            <a href="{{url('/news/'.$news->id)}}" title="{{$news['title_'.app()->getLocale()]}}">
                                                <h3>{{$news['title_'.app()->getLocale()]}}</h3>
                                            </a>
                                        </div>
                                        <div class="entry-meta">
                                            <div class="post-date">
                                                <p>{{$created_at->format('M')}} <span>{{$created_at->format('d')}}</span></p>
                                            </div>
                                            <div class="post-metablock">
                                                <div class="post-time">
                                                    <span><i class="fa fa-clock"></i>{{$created_at->format('H:i')}}</span>
                                                </div>
                                                <div class="post-location">
                                                    <span><i class="fa fa-calendar-alt"></i>{{$created_at->format('Y')}}-year</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="entry-content">
                                            <p> {!! str_limit(strip_tags($news['content_'.app()->getLocale()]),170,'...') !!}</p>
                                        </div>
                                        <a href="{{url('/news/'.$news->id)}}" class="learn-more" title="{{__('message.read_more')}}">{{__('message.read_more')}}</a>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                        <div class="clear_"></div>
                        <center><a href="{{url('/all_news')}}" class="more_b" title="More">{{__('message.all_news')}}</a></center>


                    </div>
                </div>
                <div class="section-padding"></div>
            </div><!-- Latest News /- -->


<!-- F.A.Q. -->
<div class="container-fluid faq_ no-padding">
    <div class="section-padding"></div>
    <div class="container">
        <div class="section-header">
            <h3>{{__('message.menu_faq')}}</h3>
            <span>{{__('message.frequently_asked_questions')}}</span>
        </div>

        <div class="panel-group" id="faqAccordion">
            @foreach($faq as $key => $faq)
                <div class="panel panel-default ">
                    <div class="panel-heading accordion-toggle question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question{{$key}}" aria-expanded="true">
                        <h4 class="panel-title">
                            {{ strip_tags($faq['question_'.app()->getLocale()]) }}
                        </h4>

                    </div>
                    <div id="question{{$key}}" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                            <h5><span class="label label-primary">{{__('message.answer')}}</span></h5>

                            {!! $faq['answer_'.app()->getLocale()] !!}
                        </div>
                    </div>
                </div>
            @endforeach    

        </div>
        <!--/panel-group-->


        <center><a href="{{url('/faq')}}" class="more_b" title="{{__('message.all_questions')}}">{{__('message.all_questions')}}</a></center>


    </div>
    <div class="section-padding"></div>
</div>
<!-- F.A.Q. /- -->
@endsection