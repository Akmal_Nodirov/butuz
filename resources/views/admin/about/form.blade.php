@push('css')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
    <label for="logo" class="control-label">{{ 'Logo' }}</label>
    <input class="form-control" name="logo" type="file" id="logo" value="{{ isset($about->logo) ? $about->logo : ''}}" >
    {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tel1') ? 'has-error' : ''}}">
    <label for="tel1" class="control-label">{{ 'Tel1' }}</label>
    <input class="form-control" name="tel1" type="text" id="tel1" value="{{ isset($about->tel1) ? $about->tel1 : ''}}" >
    {!! $errors->first('tel1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tel2') ? 'has-error' : ''}}">
    <label for="tel2" class="control-label">{{ 'Tel2' }}</label>
    <input class="form-control" name="tel2" type="text" id="tel2" value="{{ isset($about->tel2) ? $about->tel2 : ''}}" >
    {!! $errors->first('tel2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slogan_uz') ? 'has-error' : ''}}">
    <label for="slogan_uz" class="control-label">{{ 'Slogan Uz' }}</label>
    <input class="form-control" name="slogan_uz" type="text" id="slogan_uz" value="{{ isset($about->slogan_uz) ? $about->slogan_uz : ''}}" >
    {!! $errors->first('slogan_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slogan_ru') ? 'has-error' : ''}}">
    <label for="slogan_ru" class="control-label">{{ 'Slogan Ru' }}</label>
    <input class="form-control" name="slogan_ru" type="text" id="slogan_ru" value="{{ isset($about->slogan_ru) ? $about->slogan_ru : ''}}" >
    {!! $errors->first('slogan_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slogan_en') ? 'has-error' : ''}}">
    <label for="slogan_en" class="control-label">{{ 'Slogan En' }}</label>
    <input class="form-control" name="slogan_en" type="text" id="slogan_en" value="{{ isset($about->slogan_en) ? $about->slogan_en : ''}}" >
    {!! $errors->first('slogan_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('slogan_kr') ? 'has-error' : ''}}">
    <label for="slogan_kr" class="control-label">{{ 'Slogan Kr' }}</label>
    <input class="form-control" name="slogan_kr" type="text" id="slogan_kr" value="{{ isset($about->slogan_kr) ? $about->slogan_kr : ''}}" >
    {!! $errors->first('slogan_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    <label for="facebook" class="control-label">{{ 'Facebook' }}</label>
    <input class="form-control" name="facebook" type="text" id="facebook" value="{{ isset($about->facebook) ? $about->facebook : ''}}" >
    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telegram') ? 'has-error' : ''}}">
    <label for="telegram" class="control-label">{{ 'Telegram' }}</label>
    <input class="form-control" name="telegram" type="text" id="telegram" value="{{ isset($about->telegram) ? $about->telegram : ''}}" >
    {!! $errors->first('telegram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('youtube') ? 'has-error' : ''}}">
    <label for="youtube" class="control-label">{{ 'Youtube' }}</label>
    <input class="form-control" name="youtube" type="text" id="youtube" value="{{ isset($about->youtube) ? $about->youtube : ''}}" >
    {!! $errors->first('youtube', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_uz') ? 'has-error' : ''}}">
    <label for="address_uz" class="control-label">{{ 'Address Uz' }}</label>
    <textarea class="form-control" rows="5" name="address_uz" type="textarea" id="address_uz" >{{ isset($about->address_uz) ? $about->address_uz : ''}}</textarea>
    {!! $errors->first('address_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_ru') ? 'has-error' : ''}}">
    <label for="address_ru" class="control-label">{{ 'Address Ru' }}</label>
    <textarea class="form-control" rows="5" name="address_ru" type="textarea" id="address_ru" >{{ isset($about->address_ru) ? $about->address_ru : ''}}</textarea>
    {!! $errors->first('address_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_en') ? 'has-error' : ''}}">
    <label for="address_en" class="control-label">{{ 'Address En' }}</label>
    <textarea class="form-control" rows="5" name="address_en" type="textarea" id="address_en" >{{ isset($about->address_en) ? $about->address_en : ''}}</textarea>
    {!! $errors->first('address_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_kr') ? 'has-error' : ''}}">
    <label for="address_kr" class="control-label">{{ 'Address Kr' }}</label>
    <textarea class="form-control" rows="5" name="address_kr" type="textarea" id="address_kr" >{{ isset($about->address_kr) ? $about->address_kr : ''}}</textarea>
    {!! $errors->first('address_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_uz') ? 'has-error' : ''}}">
    <label for="rights_uz" class="control-label">{{ 'Rights and duties Uz' }}</label>
    <textarea class="form-control" rows="5" name="rights_uz" type="textarea" id="rights_uz" >{{ isset($about->rights_uz) ? $about->rights_uz : ''}}</textarea>
    {!! $errors->first('address_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_ru') ? 'has-error' : ''}}">
    <label for="rights_ru" class="control-label">{{ 'Rights and duties Ru' }}</label>
    <textarea class="form-control" rows="5" name="rights_ru" type="textarea" id="rights_ru" >{{ isset($about->rights_ru) ? $about->rights_ru : ''}}</textarea>
    {!! $errors->first('address_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_en') ? 'has-error' : ''}}">
    <label for="rights_en" class="control-label">{{ 'Rights and duties En' }}</label>
    <textarea class="form-control" rows="5" name="rights_en" type="textarea" id="rights_en" >{{ isset($about->rights_en) ? $about->rights_en : ''}}</textarea>
    {!! $errors->first('address_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address_kr') ? 'has-error' : ''}}">
    <label for="rights_kr" class="control-label">{{ 'Rights and duties Kr' }}</label>
    <textarea class="form-control" rows="5" name="rights_kr" type="textarea" id="rights_kr" >{{ isset($about->rights_kr) ? $about->rights_kr : ''}}</textarea>
    {!! $errors->first('address_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($about->email) ? $about->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    <label for="location" class="control-label">{{ 'Location' }}</label>
    <textarea class="form-control" rows="5" name="location" type="textarea" id="location" >{{ isset($about->location) ? $about->location : ''}}</textarea>
    {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#address_uz').summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#address_ru").summernote({
               callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               } 
            });
            
            $("#address_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#address_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#rights_uz").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#rights_ru").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#rights_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#rights_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
        });

    </script>
@endpush