@push('css')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($event->image) ? $event->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_uz') ? 'has-error' : ''}}">
    <label for="title_uz" class="control-label">{{ 'Title Uz' }}</label>
    <input class="form-control" name="title_uz" type="text" id="title_uz" value="{{ isset($event->title_uz) ? $event->title_uz : ''}}" >
    {!! $errors->first('title_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_ru') ? 'has-error' : ''}}">
    <label for="title_ru" class="control-label">{{ 'Title Ru' }}</label>
    <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ isset($event->title_ru) ? $event->title_ru : ''}}" >
    {!! $errors->first('title_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_en') ? 'has-error' : ''}}">
    <label for="title_en" class="control-label">{{ 'Title En' }}</label>
    <input class="form-control" name="title_en" type="text" id="title_en" value="{{ isset($event->title_en) ? $event->title_en : ''}}" >
    {!! $errors->first('title_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_kr') ? 'has-error' : ''}}">
    <label for="title_kr" class="control-label">{{ 'Title Kr' }}</label>
    <input class="form-control" name="title_kr" type="text" id="title_kr" value="{{ isset($event->title_kr) ? $event->title_kr : ''}}" >
    {!! $errors->first('title_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_uz') ? 'has-error' : ''}}">
    <label for="content_uz" class="control-label">{{ 'content Uz' }}</label>
    <textarea class="form-control" rows="5" name="content_uz" type="textarea" id="content_uz" >{{ isset($event->content_uz) ? $event->content_uz : ''}}</textarea>
    {!! $errors->first('content_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_ru') ? 'has-error' : ''}}">
    <label for="content_ru" class="control-label">{{ 'content Ru' }}</label>
    <textarea class="form-control" rows="5" name="content_ru" type="textarea" id="content_ru" >{{ isset($event->content_ru) ? $event->content_ru : ''}}</textarea>
    {!! $errors->first('content_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_en') ? 'has-error' : ''}}">
    <label for="content_en" class="control-label">{{ 'content En' }}</label>
    <textarea class="form-control" rows="5" name="content_en" type="textarea" id="content_en" >{{ isset($event->content_en) ? $event->content_en : ''}}</textarea>
    {!! $errors->first('content_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_kr') ? 'has-error' : ''}}">
    <label for="content_kr" class="control-label">{{ 'content Kr' }}</label>
    <textarea class="form-control" rows="5" name="content_kr" type="textarea" id="content_kr" >{{ isset($event->content_kr) ? $event->content_kr : ''}}</textarea>
    {!! $errors->first('content_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0":"No","1":"Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($event->published) && $event->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#content_uz').summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#content_ru").summernote({
               callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               } 
            });
            
            $("#content_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#content_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
    });
    </script>
@endpush