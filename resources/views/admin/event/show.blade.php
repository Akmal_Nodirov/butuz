@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Event {{ $event->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/event') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/event/' . $event->id . '/edit') }}" title="Edit Event"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/event' . '/' . $event->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Event" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    {{-- <tr>
                                        <th>ID</th><td>{{ $event->id }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th> Image </th><td> <img style="height:100px" src="{{ url('storage/events/'.$event->image) }}"> </td>
                                    </tr>
                                    <tr>
                                        <th>Published</th><td>{!! ($event->published == '0') ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!}</td>
                                    </tr>
                                    <tr>
                                        <th> Title Uz </th><td> {{ $event->title_uz }} </td>
                                    </tr>
                                    <tr>
                                        <th> Title Ru </th><td> {{ $event->title_ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Title En </th><td> {{ $event->title_en }} </td>
                                    </tr>
                                    <tr>
                                        <th> Title Kr </th><td> {{ $event->title_kr }} </td>
                                    </tr>
                                    <tr>
                                        <th> Content Uz </th><td> {!! $event->content_uz !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Content Ru </th><td> {!! $event->content_ru !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Content En </th><td> {!! $event->content_en !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Content Kr </th><td> {!! $event->content_kr !!} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
