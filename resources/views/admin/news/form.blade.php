@push('css')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($news->image) ? $news->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_uz') ? 'has-error' : ''}}">
    <label for="title_uz" class="control-label">{{ 'Title Uz' }}</label>
    <input class="form-control" name="title_uz" type="text" id="title_uz" value="{{ isset($news->title_uz) ? $news->title_uz : ''}}" >
    {!! $errors->first('title_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_ru') ? 'has-error' : ''}}">
    <label for="title_ru" class="control-label">{{ 'Title Ru' }}</label>
    <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ isset($news->title_ru) ? $news->title_ru : ''}}" >
    {!! $errors->first('title_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_en') ? 'has-error' : ''}}">
    <label for="title_en" class="control-label">{{ 'Title En' }}</label>
    <input class="form-control" name="title_en" type="text" id="title_en" value="{{ isset($news->title_en) ? $news->title_en : ''}}" >
    {!! $errors->first('title_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_kr') ? 'has-error' : ''}}">
    <label for="title_kr" class="control-label">{{ 'Title Kr' }}</label>
    <input class="form-control" name="title_kr" type="text" id="title_kr" value="{{ isset($news->title_kr) ? $news->title_kr : ''}}" >
    {!! $errors->first('title_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_uz') ? 'has-error' : ''}}">
    <label for="content_uz" class="control-label">{{ 'Content Uz' }}</label>
    <textarea class="form-control" rows="5" name="content_uz" id="content_uz" type="textarea" id="content_uz" >{{ isset($news->content_uz) ? $news->content_uz : ''}}</textarea>
    {!! $errors->first('content_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_ru') ? 'has-error' : ''}}">
    <label for="content_ru" class="control-label">{{ 'Content Ru' }}</label>
    <textarea class="form-control" rows="5" name="content_ru" id="content_ru" type="textarea" id="content_ru" >{{ isset($news->content_ru) ? $news->content_ru : ''}}</textarea>
    {!! $errors->first('content_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_en') ? 'has-error' : ''}}">
    <label for="content_en" class="control-label">{{ 'Content En' }}</label>
    <textarea class="form-control" rows="5" name="content_en" id="content_en" type="textarea" id="content_en" >{{ isset($news->content_en) ? $news->content_en : ''}}</textarea>
    {!! $errors->first('content_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_kr') ? 'has-error' : ''}}">
    <label for="content_kr" class="control-label">{{ 'Content Kr' }}</label>
    <textarea class="form-control" rows="5" name="content_kr" id="content_kr" type="textarea" id="content_kr" >{{ isset($news->content_kr) ? $news->content_kr : ''}}</textarea>
    {!! $errors->first('content_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0":"No","1":"Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($news->published) && $news->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('js')
    {{-- <script src="{{asset('ckeditor/ckeditor.js')}}"></script> --}}
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#content_uz').summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#content_ru").summernote({
               callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               } 
            });
            
            $("#content_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#content_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
    });

    </script>
@endpush