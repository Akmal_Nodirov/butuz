<?php
/**
 * HTTP SMS Client
 * @author Ismoil Bobojonov ismoiljoni@gmail.com
 */

if (!class_exists("UserArguments")) {
/**
 * UserArguments
 */
class UserArguments {
	/**
	 * @access public
	 * @var string
	 */
	public $username;
	/**
	 * @access public
	 * @var string
	 */
	public $password;
}}

if (!class_exists("ServiceArguments")) {
/**
 * ServiceArguments
 */
class ServiceArguments {
	/**
	 * @access public
	 * @var integer
	 */
	public $service;
	/**
	 * @access public
	 * @var string
	 */
	public $numbera;
	/**
	 * @access public
	 * @var string
	 */
	public $nickname;
	/**
	 * @access public
	 * @var integer
	 */
	public $mclass;
}}

if (!class_exists("SMSArguments")) {
/**
 * SMSArguments
 */
class SMSArguments {
	/**
	 * @access public
	 * @var integer
	 */
	public $id;
	/**
	 * @access public
	 * @var string
	 */
	public $phone;
	/**
	 * @access public
	 * @var string
	 */
	public $text;
	/**
	 * @access public
	 * @var dateTime
	 */
	public $validityTime;
}}

if (!class_exists("SMSStatus")) {
/**
 * SMSStatus
 */
class SMSStatus {
	/**
	 * @access public
	 * @var integer
	 */
	public $id;
	/**
	 * @access public
	 * @var integer
	 */
	public $status;
	/**
	 * @access public
	 * @var dateTime
	 */
	public $timeStamp;
}}

if (!class_exists("TransmitSMSArguments")) {
/**
 * TransmitSMSArguments
 */
class TransmitSMSArguments extends UserArguments {
	/**
	 * @access public
	 * @var ServiceArguments
	 */
	public $service;
	/**
	 * @access public
	 * @var integer
	 */
	public $count;
	/**
	 * @access public
	 * @var SMSArguments[]
	 */
	public $messages;
}}

if (!class_exists("TransmitSMSResult")) {
/**
 * TransmitSMSResult
 */
class TransmitSMSResult {
	/**
	 * @access public
	 * @var integer
	 */
	public $status;
	/**
	 * @access public
	 * @var string
	 */
	public $errorMsg;
}}

if (!class_exists("StatusSMSArguments")) {
/**
 * StatusSMSArguments
 */
class StatusSMSArguments extends UserArguments {
	/**
	 * @access public
	 * @var integer
	 */
	public $count;
	/**
	 * @access public
	 * @var integer[]
	 */
	public $ids;
}}

if (!class_exists("StatusSMSResult")) {
/**
 * StatusSMSResult
 */
class StatusSMSResult {
	/**
	 * @access public
	 * @var integer
	 */
	public $status;
	/**
	 * @access public
	 * @var string
	 */
	public $errorMsg;
	/**
	 * @access public
	 * @var integer
	 */
	public $count;
	/**
	 * @access public
	 * @var SMSStatus[]
	 */
	public $messages;
}}

/**
 * Default class map
 * @access public
 * @var array
 */
$classmap = array(
    "UserArguments"         => "UserArguments",
    "ServiceArguments"      => "ServiceArguments",
    "SMSArguments"          => "SMSArguments",
    "SMSStatus"             => "SMSStatus",
    "TransmitSMSArguments"  => "TransmitSMSArguments",
    "TransmitSMSResult"     => "TransmitSMSResult",
    "StatusSMSArguments"    => "StatusSMSArguments",
    "StatusSMSResult"       => "StatusSMSResult",
);

if (!function_exists("TransmitSMS")) {
    /**
	 * Service Call: TransmitSMS
	 * Parameter options:
     * @param $soapClient SoapClient
     * @param $arguments TransmitSMSArguments
	 * @return TransmitSMSResult
	 * @throws Exception invalid function signature message
	 */
	function TransmitSMS(SoapClient $soapClient, TransmitSMSArguments $arguments) {
        return $soapClient->TransmitSMS($arguments);
	}
}

if (!function_exists("StatusSMS")) {
	/**
	 * Service Call: StatusSMS
	 * Parameter options:
     * @param $soapClient SoapClient
     * @param $arguments StatusSMSArguments
	 * @return StatusSMSResult
	 * @throws Exception invalid function signature message
	 */
	function StatusSMS(SoapClient $soapClient, StatusSMSArguments $arguments) {
        return $soapClient->StatusSMS($arguments);
	}
}

?>