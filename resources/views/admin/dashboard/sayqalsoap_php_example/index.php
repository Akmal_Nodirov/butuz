<?php
/**
 * SEND HTTP SMS Example
 * @author Ismoil Bobojonov ismoiljoni@gmail.com
 */

header("Content-Type: text/html; charset=UTF-8");
header('Cache-Control: no-store, no-cache');
header('Expires: ' . date('r'));

ini_set('soap.wsdl_cache_ttl', 0);
ini_set("soap.wsdl_cache_enabled", "0");

include("HttpSMSFunctions.php");

try {
    $params = array(
        'verifypeer'    => false,
        'verifyhost'    => true,
        'authentication' => SOAP_AUTHENTICATION_DIGEST,
        'cache_wsdl'    => WSDL_CACHE_NONE,
        'trace'         => 1,
        'exceptions'    => 1,
        'classmap'      => $classmap
    );

    // Connection SOAP Server
    $client = new SoapClient('httpsmsv1.wsdl', $params);

    /*
     *
     * Send SMS ( TransmitSMS )
     *
     * */

    // General params
    $transmit = new TransmitSMSArguments();
    $transmit->username = "puchon";
    $transmit->password = "c4Og0nMd";
    // $transmit->host = "https://89.236.254.24/httpsmsv1.php";

    $service = new ServiceArguments();
    $service->service = 1;
    $transmit->service = $service;

    // SMS params
    $messages = array();

    $message = new SMSArguments();
    $message->id = 1;
    $message->phone = "998932498889";
    $message->text = "test message 1";
    $messages[0] = $message;

    $transmit->messages = $messages;
    $transmit->count = count($messages); // 1

    // Send SMS
    $transmitResult = TransmitSMS($client, $transmit);
    if ($transmitResult->status == 0) {
        echo "Success";
    } else {
        echo "Error: code={$transmitResult->status}, Message={$transmitResult->errorMsg}";
    }




    //
    //          Others exsamples and SOAP functions
    //
    /*
    echo "<pre>";
    $transmitResult = TransmitSMS($client, $transmit);
    echo "send result status = {$transmitResult->status}\n\n";
    var_dump($transmitResult);
    echo "</pre>";
    */

    /*
     *
     * Functions and Types
     *
     * */

    /*echo "<pre>";
    var_dump($client->__getLastRequest());
    var_dump($client->__getLastResponse());
    var_dump($client->__getFunctions());
    var_dump($client->__getTypes());
    echo "</pre>";*/
} catch (SoapFault $e) {
    echo "<pre>";
    var_dump($e);
    echo "</pre>";
}