@extends('layouts.admin.app')
@section('title','Applicants')
@section('content')
    <style>
        .toggle-off {
            background-color: red;
            color: #fff;
        }

        @media print {

        }

        @media (min-width: 576px) {
            .modal-dialog {
                max-width: 550px;
            }

            .modal-dialog .table thead th {
                vertical-align: middle;
            }

        }
    </style>
    <link href="/css/bootstrap-switch/bootstrap-switch.css" rel="stylesheet">

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Applicants</h3>

                <form method="GET" action="{{ url('/admin/applicant') }}" accept-charset="UTF-8"
                      class="form-inline pull-left" role="search" style="display: none;">
                    <div class="input-group ">
                        <?php
                        $facultydata = isset($_GET['facultydata']) ? $_GET['facultydata'] : '';
                        $paid = isset($_GET['paid']) ? $_GET['paid'] : '';
                        ?>
                        {{--<select name="facultydata" class="form-control faculty_sl">--}}
                            {{--<option value="">All Applicants</option>--}}
                            {{--@foreach($faculties as $faculty)--}}
                                {{--<option {{($faculty->id==$facultydata)?"selected":""}} value="{{ $faculty->id }}">{{ $faculty->name}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        <select name="paid" class="form-control faculty_sl">
                            <option value="">All Applicants</option>
                            <option {{($paid=="payed")?"selected":""}} value="payed">Paid</option>
                            <option {{($paid=="unpayed")?"selected":""}}  value="unpayed">Not paid</option>
                        </select>

                        <span class="input-group-append">
                            <button class="btn btn-secondary show_button" type="submit">
                                SHOW <APPLICANTS></APPLICANTS>
                            </button>
                        </span>
                    </div>
                </form>
                <form method="GET" action="{{ url('/admin/applicant') }}" accept-charset="UTF-8"
                      class="form-inline pull-right" role="search" style="display: none;">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               value="{{ request('search') }}">
                        <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                    </div>
                </form>
                <h3 align="right">Faculty: {{$faculty ? $faculty->name : 'No faculty name provided'}}</h3>
                <br>
                <div style="overflow:auto">
                    <div class="center">
                        <?php
                        $sign = (isset($_GET['page'])) ? '?page=' . $_GET['page'] . '&' : '?';
                        $faculty_id = (isset($_GET['faculty_id']) && $_GET['faculty_id'] == 'asc') ? 'asc' : 'desc';
                        ?>
                        <a href='{{url('/admin/excel'.$sign.'facultydata='.$facultydata.'&paid='.$paid)}}'
                           class="btn btn-success">
                            Export to Excel
                        </a>
                    </div>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>Order no.</th>
                            <th>Name</th>
                            <th>Faculty</th>
                            <th>Apply no.</th>
                            <th>Passport no.</th>
                            <th>Birth date</th>
                            <th>Comp. to the fac.</th>
                            <th>Int. in the fac.</th>
                            <th>Beha.</th>
                            <th>Self. exp.</th>
                            <th>Status</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = (isset($_GET['page']) && $_GET['page'] > 1) ? $applicants->total() - ($_GET['page'] - 1) * 20 : $applicants->total() ?>

                        @foreach($applicants as $key => $applicant)
                            <?php
                            $timestamp = strtotime($applicant->created_at);
                            $ts5 = $timestamp + 5 * (3600);
                            $datetime5 = date('Y-m-d H:i:s', $ts5);
                            ?>
                            <tr>
                                <td>{{$applicant->order_num}}</td>
                                <td>{{$applicant->firstname.' '.$applicant->lastname}}</td>
                                <td>{{$applicant->faculty->name}}</td>
                                <td>{{$applicant->apply_no}}</td>
                                <td>{{$applicant->passport}}</td>
                                <td>{{$applicant->date_of_birth}}</td>
                                <td class="criteria1" style="align-items: center; text-align: center;">
                                    <input type="number" max="15" value="<?=$applicant->talk['criteria1'] ? $applicant->talk['criteria1'] : '' ?>" style="width: 60px">
                                </td>
                                <td class="criteria2" style="align-items: center; text-align: center;">
                                    <input type="number" max="15" value="<?=$applicant->talk['criteria2'] ? $applicant->talk['criteria2'] : '' ?>" style="width: 60px">
                                </td>
                                <td class="criteria3" style="align-items: center; text-align: center;">
                                    <input type="number" max="15" value="<?=$applicant->talk['criteria3'] ? $applicant->talk['criteria3'] : '' ?>" style="width: 60px">
                                </td>
                                <td class="criteria4" style="align-items: center; text-align: center;">
                                    <input type="number" max="15" value="<?=$applicant->talk['criteria4'] ? $applicant->talk['criteria4'] : '' ?>" style="width: 60px">
                                </td>
                                <td>
                                    <input class="viewable_status_change" applicant_id="<?=$applicant->id?>" type="checkbox" value="<?=$applicant->viewable_status?>">
                                </td>
                                <td>
                                    {{ $applicant->interview}}
                                </td>
                                <td>
                                    <!--<a href="" class="btn btn-primary btn-sm" title="Paid/Not paid"><i class='fa fa-usd'></i></a>-->
                                    <form style="display: none;" method="POST" action="{{ url('/admin/set_payment' . '/' . $applicant->id) }}"
                                          accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-warning btn-sm" title="Paid/Not Paid"><i
                                                    class="fa fa-usd" aria-hidden="true"></i></button>
                                    </form>
                                    <a href="{{url('admin/application/'.$applicant->id)}}"
                                       class="btn btn-info btn-sm" style="display: none;"><i class="fa fa-eye"></i> View</a>
                                    <a href="#"
                                       class="btn btn-success btn-sm criteria_save_button"
                                       applicant_id="<?=$applicant->id?>"
                                    ></i> Save</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$applicants->links('admin.dashboard.links')}}
            </div>
        </div>
    </div>

    <style>
        .modal-dialog {
            width: 90%;
            margin: auto;
            padding: 0;
        }

        .slow .toggle-group {
            transition: left 0.7s;
            -webkit-transition: left 0.7s;
        }

        .fast .toggle-group {
            transition: left 0.1s;
            -webkit-transition: left 0.1s;
        }

        .quick .toggle-group {
            transition: none;
            -webkit-transition: none;
        }
    </style>
    <script src="{!!url('/js/jquery.min.js')!!}"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {

            $(".criteria1").on('keyup', function () {
                var criteria1 = parseInt($(this).find('input').val());

                if (criteria1 >= 15) {
                    $(this).find('input').val(15);
                }

            });

            $(".criteria2").on('keyup', function () {
                var criteria1 = parseInt($(this).find('input').val());

                if (criteria1 >= 15) {
                    $(this).find('input').val(15);
                }

            });

            $(".criteria3").on('keyup', function () {
                var criteria1 = parseInt($(this).find('input').val());

                if (criteria1 >= 15) {
                    $(this).find('input').val(15);
                }

            });

            $(".criteria4").on('keyup', function () {
                var criteria1 = parseInt($(this).find('input').val());

                if (criteria1 >= 15) {
                    $(this).find('input').val(15);
                }

            });

            $('.criteria_save_button').on('click', function (e) {
                e.preventDefault();
                let applicant_id = $(this).attr('applicant_id');
                let criteria_one = $(this).parent().siblings('.criteria1').find('input').val();
                let criteria_two = $(this).parent().siblings('.criteria2').find('input').val();
                let criteria_three = $(this).parent().siblings('.criteria3').find('input').val();
                let criteria_four = $(this).parent().siblings('.criteria4').find('input').val();

                $.ajax({
                    url: '/admin/set_test_ajax',
                    data: {
                        applicant_id: applicant_id,
                        criteria_one: criteria_one,
                        criteria_two: criteria_two,
                        criteria_three: criteria_three,
                        criteria_four: criteria_four,
                    },
                    dataType: 'json',
                    success: function (response) {
                        location.reload();
                    }
                });
            })

            $('body').on('change', '.viewable_status_change', function (e) {
                let applicant_id = $(this).attr('applicant_id');
                let status = 0;

                if($(this).prop('checked')){
                    console.log('checked');
                    status = 1;
                }


                $.ajax({
                    url: '/admin/set_status_ajax',
                    data: {
                        applicant_id: applicant_id,
                        status: status,

                    },
                    dataType: 'json',
                    success: function (response) {
                        location.reload();
                    }
                });
            })


        })
        ;
    </script>
@endsection