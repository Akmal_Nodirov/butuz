@extends('layouts.admin.app')
@section('title','Applicants')
@section('content')
    <style>
        .toggle-off {
            background-color: red;
            color: #fff;
        } 
        
        
            
        @media print {
            
            
        }
        
        @media (min-width: 576px)
    {
    .modal-dialog {
        max-width: 550px;
       }
       
    .modal-dialog .table thead th {
        vertical-align: middle;
    } 
    
        
    }   
    </style>
    <link href="/css/bootstrap-switch/bootstrap-switch.css" rel="stylesheet">

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Applicants</h3>

                <form method="GET" action="{{ url('/admin/appresult') }}" accept-charset="UTF-8"
                      class="form-inline pull-left" role="search">
                    <div class="input-group ">
                        <?php
                        $facultydata = isset($_GET['facultydata']) ? $_GET['facultydata'] : '';
                        ?>
                        <select name="facultydata" class="form-control faculty_sl">
                            <option value="">All Applicants</option>
                            @foreach($faculties as $faculty)
                                <option {{($faculty->id==$facultydata)?"selected":""}} value="{{ $faculty->id }}">{{ $faculty->name}}</option>
                            @endforeach
                        </select>
                        <span class="input-group-append">
                            <button class="btn btn-secondary show_button" type="submit">
                                SHOW APPLICANTS
                            </button>
                        </span>
                    </div>
                </form>
                <form method="GET" action="{{ url('/admin/appresult') }}" accept-charset="UTF-8"
                      class="form-inline pull-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               value="{{ request('search') }}">
                        <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                    </div>
                </form>
                <br>
                <br>
                <br>
                <div style="overflow:auto">
                    <div class="center">
                        <?php
                        $sign = (isset($_GET['page'])) ? '?page=' . $_GET['page'] . '&' : '?';
                        $faculty_id = (isset($_GET['faculty_id']) && $_GET['faculty_id'] == 'asc') ? 'asc' : 'desc';
                        ?>
                        <a href='{{url('/admin/excel'.$sign.'facultydata='.$facultydata)}}' class="btn btn-success">
                            Export to Excel
                        </a>
                    </div>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Apply no.</th>
                            <th>Faculty</th>
                            <th>Language of study group</th>
                            <th>Time</th>
                            <th>Test</th>
                            <th>Interview</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = (isset($_GET['page']) && $_GET['page'] > 1) ? $applicants->total() - ($_GET['page'] - 1) * 20 : $applicants->total() ?>

                        @foreach($applicants as $key => $applicant)
                            <?php
                            $timestamp = strtotime($applicant->created_at);
                            $ts5 = $timestamp + 5 * (3600);
                            $datetime5 = date('Y-m-d H:i:s', $ts5);
                            ?>
                            <tr>
                                <td>{{$count--}}</td>
                                <td>{{$applicant->firstname.' '.$applicant->lastname}}</td>
                                <td>{{$applicant->apply_no}}</td>
                                <td>{{!empty($applicant->faculty->name) ? $applicant->faculty->name : ''}}</td>
                                <td>
                                    <?php
                                    switch ($applicant->examination_language) {
                                        case '1':
                                            echo "Uzbek";
                                            break;
                                        case '2':
                                            echo "Russian";
                                            break;
                                        case '3':
                                            echo "Korean";
                                            break;
                                    }
                                    ?>
                                </td>
                                <td>{{$datetime5}}</td>
                                <td>{{($applicant->test)??''}}</td>
                                <td>{{($applicant->interview)??''}}</td>
                                <td>{{ $applicant->interview + $applicant->test}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$applicants->links('admin.dashboard.links')}}
            </div>
        </div>
    </div>

    <style>
        .modal-dialog {
            width: 90%;
            margin: auto;
            padding: 0;
        }

        .slow .toggle-group {
            transition: left 0.7s;
            -webkit-transition: left 0.7s;
        }

        .fast .toggle-group {
            transition: left 0.1s;
            -webkit-transition: left 0.1s;
        }

        .quick .toggle-group {
            transition: none;
            -webkit-transition: none;
        }
    </style>
    <script src="{!!url('/js/jquery.min.js')!!}"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(function () {
            $('.toggle').click(function (e) {

                if ($(this).prop("checked") == true) {
                    alert('dsad')
                } else {
                    $("#formname" + 551).submit();
                }
                console.log($(this).attr("applicant"));

            });

            $('[name="place[]"]').change(function () {
                console.log(this.value)
            });

            $("#favoritesModalLabel").html();
            $("#applicant_id").val('');
            $("#test_score").on('keyup', function () {
                var test_score = $("#test_score").val();
                if (test_score >= 60) {
                    $("#test_score").val('60')
                }
                if (test_score < 0) {
                    $("#test_score").val('0')
                }
            });
            $("#criteria1").on('keyup', function () {
                var total = parseInt($("#total").html());
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria1 >= 15) {
                    $("#total").html(15 + criteria2 + criteria3 + criteria4);
                    // $("#total").html(15+total);
                    $("#criteria1").val('15');
                }
                if (criteria1 < 0) {
                    $("#total").html(0 + criteria2 + criteria3 + criteria4);
                    // $("#total").html(0+total);
                    $("#criteria1").val('0');
                }
            });
            $("#criteria2").on('keyup', function () {
                var total = $("#total").html();
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria2 >= 15) {
                    $("#total").html(criteria1 + 15 + criteria3 + criteria4);
                    $("#criteria2").val('15');
                }
                if (criteria2 < 0) {
                    $("#total").html(criteria1 + 0 + criteria3 + criteria4);
                    $("#criteria2").val('0');
                }
            });
            $("#criteria3").on('keyup', function () {
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria3 >= 15) {
                    $("#total").html(criteria1 + criteria2 + 15 + criteria4);
                    $("#criteria3").val('15');
                }
                if (criteria3 < 0) {
                    $("#total").html(criteria1 + criteria2 + 0 + criteria4);
                    $("#criteria3").val('0');
                }
            });

            $("#criteria4").on('keyup', function () {
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria4 >= 15) {
                    $("#total").html(criteria1 + criteria2 + criteria3 + 15);
                    $("#criteria4").val('15');
                }
                if (criteria4 < 0) {
                    $("#total").html(criteria1 + criteria2 + criteria3 + 0);
                    $("#criteria4").val('0');
                }
            });

            $("#criteria1").on('change', function () {
                var total = parseInt($("#total").html());
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria1 >= 15) {
                    // $("#total").html(15+total);
                    $("#total").html(15 + criteria2 + criteria3 + criteria4);
                    $("#criteria1").val('15');
                }
                if (criteria1 < 0) {
                    $("#total").html(0 + criteria2 + criteria3 + criteria4);
                    // $("#total").html(0+total);
                    $("#criteria1").val('0');
                }
            });
            $("#criteria2").on('change', function () {
                var total = $("#total").html();
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria2 >= 15) {
                    $("#total").html(criteria1 + 15 + criteria3 + criteria4);
                    $("#criteria2").val('15');
                }
                if (criteria2 < 0) {
                    $("#total").html(criteria1 + 0 + criteria3 + criteria4);
                    $("#criteria2").val('0');
                }
            });
            $("#criteria3").on('change', function () {
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria3 >= 15) {
                    $("#total").html(criteria1 + criteria2 + 15 + criteria4);
                    $("#criteria3").val('15');
                }
                if (criteria3 < 0) {
                    $("#total").html(criteria1 + criteria2 + 0 + criteria4);
                    $("#criteria3").val('0');
                }
            });

            $("#criteria4").on('change', function () {
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria4 >= 15) {
                    $("#total").html(criteria1 + criteria2 + criteria3 + 15);
                    $("#criteria4").val('15');
                }
                if (criteria4 < 0) {
                    $("#total").html(criteria1 + criteria2 + criteria3 + 0);
                    $("#criteria4").val('0');
                }
            });


            $('#favoritesModal').on("show.bs.modal", function (e) {
                var fio = $(e.relatedTarget).data('fio');
                var id = $(e.relatedTarget).data('id');
                var test = $(e.relatedTarget).data('test');
                $("#test_score").val(test)
                $("#favoritesModalLabel").html(fio);
                $("#applicant_id").val(id);
            });
            $('#interview').on("show.bs.modal", function (e) {

                var fio = $(e.relatedTarget).data('fio');
                var id = $(e.relatedTarget).data('id');
                var total = parseInt($("#total").html());
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#favoritesModalLabel").html(fio);
                $("#applicant_id2").val(id);
                var user_id = $("#user_id").val();
                var myJsonData = {user_id: user_id, applicant_id: id}
                $.get('getInterviewData', myJsonData, function (response) {
                    $("#criteria1").val('');
                    $("#criteria2").val('');
                    $("#criteria3").val('');
                    $("#criteria4").val('');
                    $("#comment").val('');
                    $("#total").html(0);
                    if (response.interview.length) {
                        $("#criteria1").val('');
                        $("#criteria2").val('');
                        $("#criteria3").val('');
                        $("#criteria4").val('');
                        $("#comment").val('');
                        $("#total").html('');
                        var criteria1 = response.interview[0].criteria1;
                        var criteria2 = response.interview[0].criteria2;
                        var criteria3 = response.interview[0].criteria3;
                        var criteria4 = response.interview[0].criteria4;
                        var score = response.interview[0].score;
                        var comment = response.interview[0].comment;
                        $("#criteria1").val(criteria1);
                        $("#criteria2").val(criteria2);
                        $("#criteria3").val(criteria3);
                        $("#criteria4").val(criteria4);
                        $("#comment").val(comment);
                        $("#total").html(score);
                    }

                });
            });
        })
        ;
    </script>
@endsection