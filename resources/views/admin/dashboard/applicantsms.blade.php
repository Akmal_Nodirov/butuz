@extends('layouts.admin.app')
@section('title','Applicantsms')
@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <h3 align="center">Send SMS to Applicants</h3>

                <form method="GET" action="{{ url('/admin/applicantsms') }}" accept-charset="UTF-8"
                      class="form-inline pull-left" role="search">
                    <div class="input-group ">
                        <?php
                        $facultydata = isset($_GET['facultydata']) ? $_GET['facultydata'] : '';
                        $paid = isset($_GET['paid']) ? $_GET['paid'] : '';
                        ?>
                        <select name="facultydata" class="form-control faculty_sl">
                            <option value="">All Applicants</option>
                            @foreach($faculties as $faculty)
                                <option {{($faculty->id==$facultydata)?"selected":""}} value="{{ $faculty->id }}">{{ $faculty->name}}</option>
                            @endforeach
                        </select>
                        <select name="paid" class="form-control faculty_sl">
                            <option value="">All Applicants</option>
                            <option {{($paid=="payed")?"selected":""}} value="payed">Paid</option>
                            <option {{($paid=="unpayed")?"selected":""}}  value="unpayed">Not paid</option>
                        </select>

                        <span class="input-group-append">
                            <button class="btn btn-secondary show_button" type="submit">
                                SHOW APPLICANTS
                            </button>
                        </span>
                    </div>
                </form>
                <form method="GET" action="{{ url('/admin/applicantsms') }}" accept-charset="UTF-8"
                      class="form-inline pull-right" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               value="{{ request('search') }}">
                        <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                    </div>
                </form>
                <br>
                <br>
                <br>
                <div style="overflow:auto">
                    <div class="center">
                        <div class="center">
                            <?php
                            $sign = (isset($_GET['page'])) ? '?page=' . $_GET['page'] . '&' : '?';
                            $faculty_id = (isset($_GET['faculty_id']) && $_GET['faculty_id'] == 'asc') ? 'asc' : 'desc';
                            ?>
                            <a href='{{url('/admin/excel'.$sign.'facultydata='.$facultydata.'&paid='.$paid)}}' class="btn btn-success">
                                Export to Excel
                            </a>
                        </div>
                        <button
                                type="button"
                                class="btn btn-primary btn-sm sms_all"
                                data-toggle="modal"
                                data-target="#favoritesModal"
                                id="checkbox_list_btn"
                        >
                            Send SMS
                        </button>
                    </div>

                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))
                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#"
                                                                                                         class="close"
                                                                                                         data-dismiss="alert"
                                                                                                         aria-label="close">&times;</a>
                                </p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>
                                Choose <br>
                                {{-- <input type="checkbox" class="sub_chk" data-id="all" id="checkAll"> --}}
                            </th>
                            <th>Name</th>
                            <th>Apply no.</th>
                            <th>

                                <?php
                                $sign = (isset($_GET['page'])) ? '?page=' . $_GET['page'] . '&' : '?';
                                $faculty_id = (isset($_GET['faculty_id']) && $_GET['faculty_id'] == 'asc') ? 'desc' : 'asc';
                                ?>
                                <a href='{{url('/admin/applicantsms'.$sign.'faculty_id='.$faculty_id)}}'>
                                    Faculty
                                </a>
                            </th>
                            <th>
                                <?php
                                $sign = (isset($_GET['page'])) ? '?page=' . $_GET['page'] . '&' : '?';
                                $inserted_time = (isset($_GET['inserted_time']) && $_GET['inserted_time'] == 'asc') ? 'desc' : 'asc';
                                ?>
                                <a href='{{url('/admin/applicantsms'.$sign.'inserted_time='.$inserted_time)}}'>
                                    Time
                                </a>
                            </th>
                            <th>
                                <?php
                                $payment_order = (isset($_GET['payment_status']) && $_GET['payment_status'] == 'asc') ? 'desc' : 'asc';
                                ?>
                                <a href='{{url('/admin/applicantsms'.$sign.'payment_status='.$payment_order)}}'>
                                    Payment status
                                </a>
                            </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = (isset($_GET['page']) && $_GET['page'] > 1) ? $applicants->total() - ($_GET['page'] - 1) * 20 : $applicants->total() ?>

                        @foreach($applicants as $key => $applicant)
                            <?php
                            $timestamp = strtotime($applicant->created_at);
                            $ts5 = $timestamp + 5 * (3600);
                            $datetime5 = date('Y-m-d H:i:s', $ts5);
                            ?>
                            <tr>
                                <td>{{$count--}}</td>
                                <td><input type="checkbox" class="sub_chk" data-id="{{$applicant->id}}"></td>
                                <td>{{$applicant->firstname.' '.$applicant->lastname}}</td>
                                <td>{{$applicant->apply_no}}</td>
                                <td>{{$applicant->faculty->name}}</td>
                                <td>{{$datetime5}}</td>
                                <td>{!! ($applicant->payment_status) ? '<span class="badge badge-success">Paid</span>' : '<span class="badge badge-danger">Not Paid</span>' !!}</td>
                                <td>
                                    <!--<a href="" class="btn btn-primary btn-sm" title="Paid/Not paid"><i class='fa fa-usd'></i></a>-->
                                    <form method="POST" action="{{ url('/admin/set_payment' . '/' . $applicant->id) }}"
                                          accept-charset="UTF-8" style="display:inline">
                                        {{ method_field('PUT') }}
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-warning btn-sm" title="Paid/Not Paid"><i
                                                    class="fa fa-usd" aria-hidden="true"></i></button>
                                    </form>
                                    <a href="{{url('admin/application/'.$applicant->id)}}"
                                       class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{$applicants->links('admin.dashboard.links')}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="favoritesModal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="favoritesModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                </div>
                <form method="POST" action="{{ url('/admin/send_sms') }}" accept-charset="UTF-8" style="display:inline">
                    <div class="modal-body">
                        <input type="hidden" name="applicant_id" id="applicant_id">
                        <div class="form-group">
                            <label for="title">SMS Text</label>
                            <br>
                            <input type="hidden" name="checkedlist" id="checkedlist">
                            <textarea onKeyUp="count_it()"  name="text_message" id="test_score" class="form-control send_sms_input"
                                       required="required"></textarea>
                             <br>          
                             <p>Количество символов:  <span id="counter"></span></p> 
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id='closeModal'>Close
                        </button>
                        <span class="pull-right">
                {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">
				    Save
				</button>
	        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <style>
        .modal-dialog {
            width: 90%;
            margin: auto;
            padding: 0;
        }
        
        #counter {
            color: #ff0c00;
            font-size: 16px;
            font-weight: bold;
        }
    </style>
    <script src="{!!url('/js/jquery.min.js')!!}"></script>
    <script>
    
    function count_it() {
    document.getElementById('counter').innerHTML = document.getElementById('test_score').value.length;
    }
    count_it();
    
    
        $(function () {

            $("#checkAll").click(function(){
                $("#checkedlist").val('0');
                if ($('#checkAll').prop('checked')){
                    $("#checkedlist").val('all');
                }else{
                    $("#checkedlist").val('0');
                }

                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $(".sms_all").on('click', function (e) {
                var allVals = [];
                $(".sub_chk:checked").each(function () {
                    allVals.push($(this).attr('data-id'));
                });

                if (allVals.length == 0) {
                    alert('Please Choose one applicant at least!!!');
//                    $('#favoritesModal').modal('toggle');
                    // $("#favoritesModal").hide();
                } else {
                    $("#checkedlist").val(allVals);
                }

            });
            $("#favoritesModalLabel").html();
            $("#applicant_id").val('');
            $("#test_score").on('keyup', function () {
                var test_score = $("#test_score").val();
                if (test_score >= 60) {
                    $("#test_score").val('60')
                }
                if (test_score < 0) {
                    $("#test_score").val('0')
                }
            });
            $("#criteria1").on('keyup', function () {
                var total = parseInt($("#total").html());
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria1 >= 15) {
                    $("#total").html(15 + criteria2 + criteria3 + criteria4);
                    // $("#total").html(15+total);
                    $("#criteria1").val('15');
                }
                if (criteria1 < 0) {
                    $("#total").html(0 + criteria2 + criteria3 + criteria4);
                    // $("#total").html(0+total);
                    $("#criteria1").val('0');
                }
            });
            $("#criteria2").on('keyup', function () {
                var total = $("#total").html();
                var criteria1 = parseInt($("#criteria1").val());
                var criteria2 = parseInt($("#criteria2").val());
                var criteria3 = parseInt($("#criteria3").val());
                var criteria4 = parseInt($("#criteria4").val());

                $("#total").html(criteria1 + criteria2 + criteria3 + criteria4);
                if (criteria2 >= 15) {
                    $("#total").html(criteria1 + 15 + criteria3 + criteria4);
                    $("#criteria2").val('15');
                }
                if (criteria2 < 0) {
                    $("#total").html(criteria1 + 0 + criteria3 + criteria4);
                    $("#criteria2").val('0');
                }
            });

        });
    </script>
@endsection
