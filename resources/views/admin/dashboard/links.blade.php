<?php
	$payment_status = (isset($_GET['payment_status'])) ? '&payment_status='.$_GET['payment_status'] : '';
	$inserted_time = (isset($_GET['inserted_time'])) ? '&inserted_time='.$_GET['inserted_time'] : '';
	$faculty_id = (isset($_GET['faculty_id'])) ? '&faculty_id='.$_GET['faculty_id'] : '';
	$facultydata = (isset($_GET['facultydata'])) ? '&facultydata='.$_GET['facultydata'] : '';
	$paid = (isset($_GET['paid'])) ? '&paid='.$_GET['paid'] : '';
?>
<ul class="pagination">
	<!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <li class="disabled page-item"><span class="page-link">&laquo;</span></li>
    @else
        <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
    @endif

	{{-- @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="page-item">
            <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor --}}

     <!-- Pagination Elements -->
    @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        @if (is_string($element))
            <li class="page-item disabled"><span>{{ $element }}</span></li>
        @endif

        <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item active">
                    	<span class="page-link">
                    		{{ $page }}
                    	</span>
                    </li>
                @else
                    <li class="page-item">
                    	<a class="page-link" href="{{ $url . $payment_status.$facultydata.$faculty_id.$paid}}">
                    		{{ $page }}
                    	</a>
                    </li>
                @endif
            @endforeach
        @endif
    @endforeach

    <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
    @else
        <li class="disabled page-item"><span class="page-link">&raquo;</span></li>
    @endif
</ul>