@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Faq {{ $faq->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/faq') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/faq/' . $faq->id . '/edit') }}" title="Edit Faq"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/faq' . '/' . $faq->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Faq" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    {{-- <tr>
                                        <th>ID</th><td>{{ $faq->id }}</td>
                                    </tr> --}}
                                    <tr><th> Question Uz </th><td> {!! $faq->question_uz !!} </td></tr>
                                    <tr><th> Question Ru </th><td> {!! $faq->question_ru !!} </td></tr>
                                    <tr><th> Question En </th><td> {!! $faq->question_en !!} </td></tr>
                                    <tr><th> Question Kr </th><td> {!! $faq->question_kr !!} </td></tr>
                                    <tr><th> Answer Uz </th><td> {!! $faq->answer_uz !!} </td></tr>
                                    <tr><th> Answer Ru </th><td> {!! $faq->answer_ru !!} </td></tr>
                                    <tr><th> Answer En </th><td> {!! $faq->answer_en !!} </td></tr>
                                    <tr><th> Answer Kr </th><td> {!! $faq->answer_kr !!} </td></tr>
                                    <tr><th> Published </th><td>{!! ($faq->published == '0') ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!}</td>
                                        <td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
