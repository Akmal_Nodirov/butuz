@push('css')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('question_uz') ? 'has-error' : ''}}">
    <label for="question_uz" class="control-label">{{ 'Question Uz' }}</label>
    <textarea class="form-control" rows="5" name="question_uz" type="textarea" id="question_uz" >{{ isset($faq->question_uz) ? $faq->question_uz : ''}}</textarea>
    {!! $errors->first('question_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('question_ru') ? 'has-error' : ''}}">
    <label for="question_ru" class="control-label">{{ 'Question Ru' }}</label>
    <textarea class="form-control" rows="5" name="question_ru" type="textarea" id="question_ru" >{{ isset($faq->question_ru) ? $faq->question_ru : ''}}</textarea>
    {!! $errors->first('question_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('question_en') ? 'has-error' : ''}}">
    <label for="question_en" class="control-label">{{ 'Question En' }}</label>
    <textarea class="form-control" rows="5" name="question_en" type="textarea" id="question_en" >{{ isset($faq->question_en) ? $faq->question_en : ''}}</textarea>
    {!! $errors->first('question_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('question_kr') ? 'has-error' : ''}}">
    <label for="question_kr" class="control-label">{{ 'Question Kr' }}</label>
    <textarea class="form-control" rows="5" name="question_kr" type="textarea" id="question_kr" >{{ isset($faq->question_kr) ? $faq->question_kr : ''}}</textarea>
    {!! $errors->first('question_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('answer_uz') ? 'has-error' : ''}}">
    <label for="answer_uz" class="control-label">{{ 'Answer Uz' }}</label>
    <textarea class="form-control" rows="5" name="answer_uz" type="textarea" id="answer_uz" >{{ isset($faq->answer_uz) ? $faq->answer_uz : ''}}</textarea>
    {!! $errors->first('answer_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('answer_ru') ? 'has-error' : ''}}">
    <label for="answer_ru" class="control-label">{{ 'Answer Ru' }}</label>
    <textarea class="form-control" rows="5" name="answer_ru" type="textarea" id="answer_ru" >{{ isset($faq->answer_ru) ? $faq->answer_ru : ''}}</textarea>
    {!! $errors->first('answer_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('answer_en') ? 'has-error' : ''}}">
    <label for="answer_en" class="control-label">{{ 'Answer En' }}</label>
    <textarea class="form-control" rows="5" name="answer_en" type="textarea" id="answer_en" >{{ isset($faq->answer_en) ? $faq->answer_en : ''}}</textarea>
    {!! $errors->first('answer_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('answer_kr') ? 'has-error' : ''}}">
    <label for="answer_kr" class="control-label">{{ 'Answer Kr' }}</label>
    <textarea class="form-control" rows="5" name="answer_kr" type="textarea" id="answer_kr" >{{ isset($faq->answer_kr) ? $faq->answer_kr : ''}}</textarea>
    {!! $errors->first('answer_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0":"No","1":"Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($faq->published) && $faq->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('js')
    {{-- <script src="{{asset('ckeditor/ckeditor.js')}}"></script> --}}
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script type="text/javascript">
        
        $(document).ready(function() {

            $('#question_uz').summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#question_ru").summernote({
               callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               } 
            });
            
            $("#question_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#question_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#answer_uz").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#answer_ru").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#answer_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#answer_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
    });
    </script>
@endpush