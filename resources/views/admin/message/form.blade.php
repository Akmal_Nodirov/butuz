<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($message->name) ? $message->name : ''}}" readonly>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($message->phone) ? $message->phone : ''}}" readonly>
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($message->email) ? $message->email : ''}}" readonly>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('subject:') ? 'has-error' : ''}}">
    <label for="subject:" class="control-label">{{ 'Subject:' }}</label>
    <input class="form-control" name="subject" type="text" id="subject" value="{{ isset($message->subject) ? $message->subject : ''}}" readonly>
    {!! $errors->first('subject:', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('message') ? 'has-error' : ''}}">
    <label for="message:" class="control-label">{{ 'Message:' }}</label>
    <textarea class="form-control" rows="5" name="message" type="textarea" id="message" readonly>{{ isset($message->message) ? $message->message : ''}}</textarea>
    {!! $errors->first('message:', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('answer') ? 'has-error' : ''}}">
    <label for="answer:" class="control-label">{{ 'Answer:' }}</label>
    <textarea class="form-control" rows="5" name="answer" type="textarea" id="answer" >{{ isset($message->answer) ? $message->answer : ''}}</textarea>
    {!! $errors->first('answer:', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Answer' : 'Create' }}">
</div>
