@push('css')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($program->image) ? $program->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_uz') ? 'has-error' : ''}}">
    <label for="title_uz" class="control-label">{{ 'Title Uz' }}</label>
    <input class="form-control" name="title_uz" type="text" id="title_uz" value="{{ isset($program->title_uz) ? $program->title_uz : ''}}" >
    {!! $errors->first('title_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_ru') ? 'has-error' : ''}}">
    <label for="title_ru" class="control-label">{{ 'Title Ru' }}</label>
    <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ isset($program->title_ru) ? $program->title_ru : ''}}" >
    {!! $errors->first('title_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_en') ? 'has-error' : ''}}">
    <label for="title_en" class="control-label">{{ 'Title En' }}</label>
    <input class="form-control" name="title_en" type="text" id="title_en" value="{{ isset($program->title_en) ? $program->title_en : ''}}" >
    {!! $errors->first('title_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_kr') ? 'has-error' : ''}}">
    <label for="title_kr" class="control-label">{{ 'Title Kr' }}</label>
    <input class="form-control" name="title_kr" type="text" id="title_kr" value="{{ isset($program->title_kr) ? $program->title_kr : ''}}" >
    {!! $errors->first('title_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_uz') ? 'has-error' : ''}}">
    <label for="description_uz" class="control-label">{{ 'Description Uz' }}</label>
    <textarea class="form-control" rows="5" name="description_uz" type="textarea" id="description_uz" >{{ isset($program->description_uz) ? $program->description_uz : ''}}</textarea>
    {!! $errors->first('description_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_ru') ? 'has-error' : ''}}">
    <label for="description_ru" class="control-label">{{ 'Description Ru' }}</label>
    <textarea class="form-control" rows="5" name="description_ru" type="textarea" id="description_ru" >{{ isset($program->description_ru) ? $program->description_ru : ''}}</textarea>
    {!! $errors->first('description_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_en') ? 'has-error' : ''}}">
    <label for="description_en" class="control-label">{{ 'Description En' }}</label>
    <textarea class="form-control" rows="5" name="description_en" type="textarea" id="description_en" >{{ isset($program->description_en) ? $program->description_en : ''}}</textarea>
    {!! $errors->first('description_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description_kr') ? 'has-error' : ''}}">
    <label for="description_kr" class="control-label">{{ 'Description Kr' }}</label>
    <textarea class="form-control" rows="5" name="description_kr" type="textarea" id="description_kr" >{{ isset($program->description_kr) ? $program->description_kr : ''}}</textarea>
    {!! $errors->first('description_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0":"No","1":"Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($program->published) && $program->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#description_uz').summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#description_ru").summernote({
               callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               } 
            });
            
            $("#description_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#description_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
    });
    </script>
@endpush