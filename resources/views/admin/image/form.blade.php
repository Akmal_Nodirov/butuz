<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($image->image) ? $image->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('event_id') ? 'has-error' : ''}}">
    <label for="event_id" class="control-label">{{ 'Event Id' }}</label>
    <select class="form-control" name="event_id" type="text" id="event_id">
        <option>Choose event...</option>
        @foreach($event as $key=>$event)
            <option value="{{$event->id}}" {{isset($image->event_id) && $image->event_id == $event->id ? 'selected' : ''}}>{{$event->title_en}}</option>
        @endforeach
    </select>
    {!! $errors->first('event_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0":"No","1":"Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($image->published) && $image->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
