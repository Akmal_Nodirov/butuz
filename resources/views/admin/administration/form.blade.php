<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Image' }}</label>
    <input class="form-control" name="image" type="file" id="image" value="{{ isset($administration->image) ? $administration->image : ''}}" >
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fullname_uz') ? 'has-error' : ''}}">
    <label for="fullname_uz" class="control-label">{{ 'Fullname Uz' }}</label>
    <input class="form-control" name="fullname_uz" type="text" id="fullname_uz" value="{{ isset($administration->fullname_uz) ? $administration->fullname_uz : ''}}" >
    {!! $errors->first('fullname_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fullname_ru') ? 'has-error' : ''}}">
    <label for="fullname_ru" class="control-label">{{ 'Fullname Ru' }}</label>
    <input class="form-control" name="fullname_ru" type="text" id="fullname_ru" value="{{ isset($administration->fullname_ru) ? $administration->fullname_ru : ''}}" >
    {!! $errors->first('fullname_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fullname_en') ? 'has-error' : ''}}">
    <label for="fullname_en" class="control-label">{{ 'Fullname En' }}</label>
    <input class="form-control" name="fullname_en" type="text" id="fullname_en" value="{{ isset($administration->fullname_en) ? $administration->fullname_en : ''}}" >
    {!! $errors->first('fullname_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fullname_kr') ? 'has-error' : ''}}">
    <label for="fullname_kr" class="control-label">{{ 'Fullname Kr' }}</label>
    <input class="form-control" name="fullname_kr" type="text" id="fullname_kr" value="{{ isset($administration->fullname_kr) ? $administration->fullname_kr : ''}}" >
    {!! $errors->first('fullname_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($administration->phone) ? $administration->phone : ''}}" >
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('position_id') ? 'has-error' : ''}}">
    <label for="position_id" class="control-label">{{ 'Position Id' }}</label>
    <select class="form-control" name="position_id" type="text" id="position_id" value="{{ isset($administration->position_id) ? $administration->position_id : ''}}" >
        <option>Select position...</option>
        @foreach(\App\Position::published()->get() as $position)
            <option value="{{$position->id}}" {{( isset($administration) && $position->id == $administration->position_id) ? 'selected' : ''}} >{{$position['position_en']}}</option>
        @endforeach
    </select>    
    {!! $errors->first('position_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0": "No", "1": "Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($administration->published) && $administration->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
