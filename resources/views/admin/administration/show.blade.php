@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Administration {{ $administration->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/administration') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/administration/' . $administration->id . '/edit') }}" title="Edit Administration"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/administration' . '/' . $administration->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Administration" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    {{-- <tr>
                                        <th>ID</th><td>{{ $administration->id }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th> Image </th><td> <img src="{!! asset('storage/administration/'.$administration->image) !!}" style="width:100px"> </td>
                                    </tr>
                                    <tr>
                                        <th> Fullname Uz </th><td> {{ $administration->fullname_uz }} </td>
                                    </tr>
                                    <tr>
                                        <th> Fullname Ru </th><td> {{ $administration->fullname_ru }} </td>
                                    </tr>
                                    <tr>
                                        <th> Fullname En </th><td> {{ $administration->fullname_en }} </td>
                                    </tr>
                                    <tr>
                                        <th> Fullname Kr </th><td> {{ $administration->fullname_kr }} </td>
                                    </tr>
                                    <tr>
                                        <th> Phone </th><td> {{ $administration->phone }} </td>
                                    </tr>
                                    <tr>
                                        <th> Position </th><td> {{ $administration->position->position_en }} </td>
                                    </tr>
                                    <tr>
                                        <th> Published </th><td> {!! ($administration->published == false) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>'!!} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
