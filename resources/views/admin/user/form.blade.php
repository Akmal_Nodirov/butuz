<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <select class="form-control" name="type" type="text" id="type">
        <option>Choose user type...</option>
        <option value="rec">Rector</option>
        <option value="dean">Dean</option>
        <option value="admin">Admin</option>
        <option value="prof">Professor</option>
        <option value="stu">Student</option>
    </select>    
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
    <label for="type_id" class="control-label">{{ 'Type Id' }}</label>
    <select class="form-control" name="type_id" type="text" id="type_id">
        <option>Choose user</option>
    </select>    
    {!! $errors->first('type_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($user->phone) ? $user->phone : ''}}" >
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($user->email) ? $user->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email_verified_at') ? 'has-error' : ''}}">
    <label for="email_verified_at" class="control-label">{{ 'Email Verified At' }}</label>
    <input class="form-control" name="email_verified_at" type="text" id="email_verified_at" value="{{ isset($user->email_verified_at) ? $user->email_verified_at : ''}}" >
    {!! $errors->first('email_verified_at', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Password' }}</label>
    <input class="form-control" name="password" type="text" id="password" value="{{ isset($user->password) ? $user->password : ''}}" >
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('remember_token') ? 'has-error' : ''}}">
    <label for="remember_token" class="control-label">{{ 'Remember Token' }}</label>
    <input class="form-control" name="remember_token" type="text" id="remember_token" value="{{ isset($user->remember_token) ? $user->remember_token : ''}}" >
    {!! $errors->first('remember_token', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
