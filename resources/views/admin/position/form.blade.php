<div class="form-group {{ $errors->has('position_uz') ? 'has-error' : ''}}">
    <label for="position_uz" class="control-label">{{ 'Position Uz' }}</label>
    <input class="form-control" name="position_uz" type="text" id="position_uz" value="{{ isset($position->position_uz) ? $position->position_uz : ''}}" >
    {!! $errors->first('position_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('position_ru') ? 'has-error' : ''}}">
    <label for="position_ru" class="control-label">{{ 'Position Ru' }}</label>
    <input class="form-control" name="position_ru" type="text" id="position_ru" value="{{ isset($position->position_ru) ? $position->position_ru : ''}}" >
    {!! $errors->first('position_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('position_en') ? 'has-error' : ''}}">
    <label for="position_en" class="control-label">{{ 'Position En' }}</label>
    <input class="form-control" name="position_en" type="text" id="position_en" value="{{ isset($position->position_en) ? $position->position_en : ''}}" >
    {!! $errors->first('position_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('position_kr') ? 'has-error' : ''}}">
    <label for="position_kr" class="control-label">{{ 'Position Kr' }}</label>
    <input class="form-control" name="position_kr" type="text" id="position_kr" value="{{ isset($position->position_kr) ? $position->position_kr : ''}}" >
    {!! $errors->first('position_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0": "No", "1":"Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($position->published) && $position->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
