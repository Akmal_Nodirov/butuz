@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ $subject->subject }}</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/subject') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                    	<table class="table">
                    	<thead>
                    		<tr>
                    			<th>#</th><th>Professor</th>
                    		</tr>
                    	</thead>	
                    	<tbody>
                    		@foreach($subject->professors as $key => $professor)
                    			<tr>
                    				<td>{{ ++$key }}</td>
                    				<td>{{ $professor->firstname.' '.$professor->lastname }}</td>
                    			</tr>
                    		@endforeach
                    	</tbody>
                    </table>	
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection                    