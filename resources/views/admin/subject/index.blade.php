@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Subject</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/subject/create') }}" class="btn btn-success btn-sm" title="Add New Subject">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/admin/subject') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Subject</th>
                                        <th>Type</th>
                                        <th>Homework</th>
                                        <th>Assignment</th>
                                        <th>Code</th>
                                        <th>Credit</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $iteration = isset($_GET['page']) ? ($_GET['page']-1) * 25 : 0;
                                ?>
                                @foreach($subject as $key => $item)
                                    <tr>
                                        <td>{{ ++$iteration }}</td>
                                        {{-- <td><a href="{{url('admin/professorsBySubject/'.$item->id)}}">{{ $item->subject }}</a></td> --}}
                                        <td>{{ $item->subject }}</td>
                                        <td>{{ $item->type == '0' ? 'Elective' : 'Major' }}</td>
                                        <td>{!! ($item->homework == '0' || $item->homework == null) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!}</td>
                                        <td>{!! ($item->bonus == '0' || $item->bonus == null) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!}</td>
                                        <td>{{ $item->code }}</td>
                                        <td>{{ $item->credit }}</td>
                                        <td>
                                            <a href="{{ url('/admin/subject/' . $item->id) }}" title="View Subject"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/admin/subject/' . $item->id . '/edit') }}" title="Edit Subject"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/admin/subject' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Subject" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $subject->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
