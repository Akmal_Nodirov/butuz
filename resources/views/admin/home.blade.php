@extends('layouts.admin.app')
@push('css')
<style type="text/css">
    .ul_liga{
    list-style:none !important; 
    display:flex;
}
.ul_liga li{
 width:100%;
 padding:25px;
 position: relative;
 display: flex;
 flex-direction: column;
 text-align:center;
 color: #3a4651;
 font-size: 14px;
 font-weight: bold;
}
.ul_liga li span.posrelate{
    position: realtive;
    margin: 10px auto;
    padding: 25px;
    border-radius: 50%;
    -webkit-box-shadow: 0px 0px 6px 3px #00000038 inset;
    box-shadow: 0px 0px 6px 3px #00000038 inset;
    background-color: #85ce35;
    width: 150px;
    height: 150px;
    font-size: 38px;
    line-height: 102px;
    color: #ffffff;
    text-align: center;
}
.prelate{
    position:relative;
}
.ul_liga li span.pabsol{
    box-shadow: 0 0 9px 2px rgb(107, 139, 157);
    position: absolute;
    top: 4%;
    line-height: 78px;
    font-size: 20px;
    text-align: center;
    right: 3%;
    border-radius: 50%;
    width: 80px;
    height: 80px;
    background-color: #3a4651;
    color: #fff;
    
}
h3.h3gre span{
    width:30px;
    height:30px;
    border-radius: 50%;
    background-color:#85ce35;
    margin: 5px;
}
h3.h3blu span{
    width:30px;
    height:30px;
    border-radius: 50%;
    background-color:#3a4651;
    
    margin: 5px;
}
.h3classga{
display:flex;
align-items:center;
}
.totappl{
    border-radius: 20px;
    background-color: #85ce35;
    width: 800px;
    color:#fff;
    text-align: center;
    line-height: 67px;
    font-size: 31px;
    margin: 20px auto;
    box-shadow: 1px 0px 6px 3px #00000038 inset;
}
h3{
    color:#3a4651;
}
</style>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <h3 class="h3gre h3classga"><span></span>Number of applications | 학과별 접수인원</h3>
                <h3 class="h3blu h3classga"><span></span>The ratio of applications to one place | 경쟁률</h3>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    	<ul class="ul_liga"> 
                    	<?php $total = 0; ?>
                	    @foreach($applications as $key => $application)
                	        <?php $total += $application->total; ?>
                	        <li> 
                	            <span class="count posrelate">{{$application->total}}</span>
                	            {{$application->faculty->name}}
                	            <?php 
                	            $ratio = 0;
                	                switch($application->faculty_id){
                	                    case '1':
                	                        $ratio = round($application->total/120,1);
                	                        break;
                	                    case '2':
                	                        $ratio = round($application->total/75,1);
                	                        break;
                	                    case '3':
                	                        $ratio = round($application->total/45,1);
                	                        break;
                	                    case '4':
                	                        $ratio = round($application->total/45,1);
                	                        break;
                	                        
                	                }
                	            ?>
                	            <span class="pabsol">{{$ratio}}</span>
                	        </li>
                	    @endforeach
                	    </ul>
                		
                    <center class="totappl"> Total applications | 총 접수인원 : &nbsp; &nbsp;{{$total}}</center>
                    
                    <center class="totappl"> Paid applications | 전형료 납부 상황 : &nbsp; &nbsp;{{$paid_applications}}</center>
                    {{-- {{Auth::user()->type}} --}}

                </div>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-title-block">
                            <h3 class="title"> Number of applications by day | 하루의 신청 건수</h3>
                        </div>
                        <section class="example">
                            <div id="morris-one-line-chart"></div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Paid Applications</div>
                <h3 class="h3gre h3classga"><span></span>Number of applications | 학과별 접수인원</h3>
                <h3 class="h3blu h3classga"><span></span>The ratio of applications to one place | 경쟁률</h3>

                <div class="card-body ">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                        <ul class="ul_liga"> 
                        <?php $total = 0; ?>
                        @foreach($applications_paid as $key => $application)
                            <li> 
                                <span class="count posrelate">{{$application->total}}</span>
                                {{$application->faculty->name}}
                                <?php 
                                $ratio = 0;
                                    switch($application->faculty_id){
                                        case '1':
                                            $ratio = round($application->total/120,1);
                                            break;
                                        case '2':
                                            $ratio = round($application->total/75,1);
                                            break;
                                        case '3':
                                            $ratio = round($application->total/45,1);
                                            break;
                                        case '4':
                                            $ratio = round($application->total/45,1);
                                            break;
                                            
                                    }
                                ?>
                                <span class="pabsol">{{$ratio}}</span>
                            </li>
                        @endforeach
                        </ul>                    
                    <center class="totappl"> Paid applications | 전형료 납부 상황 : &nbsp; &nbsp;{{$paid_applications}}</center>
                    {{-- {{Auth::user()->type}} --}}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script type="text/javascript">
		$(document).ready(function () {
			$('.count').each(function () {
			  $(this).prop('Counter',0).animate({
				  Counter: $(this).text()
				  }, {
				  duration: 3000,
				  easing: 'swing',
				  step: function (now) {
				  	$(this).text(Math.ceil(now));
			  	  }
			  });
			});
		});
	</script>
	
	<script type="text/javascript">
	    $(function() {
    
    if (!$('#morris-one-line-chart').length) {
        return false;
    }

    function drawMorrisCharts() {

        $('#morris-one-line-chart').empty();
        
        Morris.Line({
            element: 'morris-one-line-chart',
                data: [
                    @foreach($applications_by_date as $key => $application)
                        <?php $day = new datetime($application->day) ?>
                        { period: '{{$application->day}}', value: {{$application->total}} },
                    @endforeach
                ],
            xkey: 'period',
            ykeys: ['value'],
            resize: true,
            lineWidth:4,
            labels: ['Applicant count'],
            lineColors: [config.chart.colorPrimary.toString()],
            pointSize:5,
            xLabels: 'day',
            xLabelAngle: 60,
            xLabelFormat: function (d) {
                return ("0" + (d.getDate())).slice(-2) + '.' + 
                       ("0" + (d.getMonth() + 1)).slice(-2) + '.' + 
                       d.getFullYear();
              },
  
        });

    }

    drawMorrisCharts();

    $(document).on("themechange", function(){
        drawMorrisCharts();
    });
});
	</script>
@endpush