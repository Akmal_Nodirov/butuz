<div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
    <label for="firstname" class="control-label">{{ 'Firstname' }}</label>
    <input class="form-control" name="firstname" type="text" id="firstname" value="{{ isset($application->firstname) ? $application->firstname : ''}}" >
    {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
    <label for="lastname" class="control-label">{{ 'Lastname' }}</label>
    <input class="form-control" name="lastname" type="text" id="lastname" value="{{ isset($application->lastname) ? $application->lastname : ''}}" >
    {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('order_num') ? 'has-error' : ''}}">
    <label for="date3" class="control-label">{{ 'Order number' }}</label>
    <input class="form-control" name="date3" type="text" id="date3" value="{{ isset($application->order_num) ? $application->order_num : ''}}" >
    {!! $errors->first('order_num', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    <label for="gender" class="control-label">{{ 'Gender' }}</label>
    <select class="form-control" name="gender" type="text" id="gender" value="{{ isset($application->gender) ? $application->gender : ''}}" >
        <option>Choose gender...</option>
        <option value="1" {{ (!Request::is('admin/application/create') && $application->gender == '1') ? "selected" : ''}} >Male</option>
        <option value="0" {{ (!Request::is('admin/application/create') && $application->gender == '0') ? "selected" : ''}} >Female</option>
    </select>
    {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    <label for="photo" class="control-label">{{ 'Photo' }}</label>
    <input class="form-control" name="photo" type="file" id="photo" value="{{ isset($application->photo) ? $application->photo : ''}}" >
    {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('passport') ? 'has-error' : ''}}">
    <label for="passport" class="control-label">{{ 'Passport' }}</label>
    <input class="form-control" name="passport" type="text" id="passport" value="{{ isset($application->passport) ? $application->passport : ''}}" >
    {!! $errors->first('passport', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nationality') ? 'has-error' : ''}}">
    <label for="nationality" class="control-label">{{ 'Nationality' }}</label>
    <input class="form-control" name="nationality" type="text" id="nationality" value="{{ isset($application->nationality) ? $application->nationality : ''}}" >
    {!! $errors->first('nationality', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone_number') ? 'has-error' : ''}}">
    <label for="phone_number" class="control-label">{{ 'Phone Number' }}</label>
    <input class="form-control" name="phone_number" type="text" id="phone_number" value="{{ isset($application->phone_number) ? $application->phone_number : ''}}" >
    {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($application->email) ? $application->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'Address' }}</label>
    <textarea class="form-control" rows="5" name="address" type="textarea" id="address" >{{ isset($application->address) ? $application->address : ''}}</textarea>
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('faculty_id') ? 'has-error' : ''}}">
    <label for="faculty_id" class="control-label">{{ 'Faculty Id' }}</label>
    <select class="form-control" name="faculty_id" type="text" id="faculty_id" value="{{ isset($application->faculty_id) ? $application->faculty_id : ''}}" >
        <option>Select faculty...</option>
        @foreach(\App\Faculty::all() as $faculty)
            <option value="{{$faculty->id}}"
                @if(!Request::is('admin/application/create'))
                    {{ $application->faculty_id == $faculty->id ? 'selected' : '' }}
                @endif    
            >{{$faculty->name}}</option>
        @endforeach
    </select>    
    {!! $errors->first('faculty_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('group_id') ? 'has-error' : ''}}">
    <label for="group_id" class="control-label">{{ 'Group Id' }}</label>
    <select class="form-control" name="group_id" type="text" id="group_id" value="{{ isset($application->group_id) ? $application->group_id : ''}}" >
        <option></option>
    </select>    
    {!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('examination_language') ? 'has-error' : ''}}">
    <label for="examination_language" class="control-label">{{ 'Examination Language' }}</label>
    <select class="form-control" name="examination_language" type="text" id="examination_language" value="" >
        <option>Choose language...</option>
        <option value="1" {{ isset($application->examination_language) && $application->examination_language == "1" ? 'selected' : ''}} >Uzbek</option>
        <option value="2" {{ isset($application->examination_language) && $application->examination_language == "2" ? 'selected' : ''}} >Russian</option>
        <option value="3" {{ isset($application->examination_language) && $application->examination_language == "3" ? 'selected' : ''}} >Korean</option>
    </select>
    {!! $errors->first('examination_language', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : ''}}">
    <label for="date_of_birth" class="control-label">{{ 'Date Of Birth' }}</label>
    <input class="form-control" name="date_of_birth" type="text" id="date_of_birth" value="{{ isset($application->date_of_birth) ? $application->date_of_birth : ''}}" >
    {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('work_place') ? 'has-error' : ''}}">
    <label for="work_place" class="control-label">{{ 'Work Place' }}</label>
    <input class="form-control" name="work_place" type="text" id="work_place" value="{{ isset($application->work_place) ? $application->work_place : ''}}" >
    {!! $errors->first('work_place', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('work_phone_number') ? 'has-error' : ''}}">
    <label for="work_phone_number" class="control-label">{{ 'Work Phone Number' }}</label>
    <input class="form-control" name="work_phone_number" type="text" id="work_phone_number" value="{{ isset($application->work_phone_number) ? $application->work_phone_number : ''}}" >
    {!! $errors->first('work_phone_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('high_school_name') ? 'has-error' : ''}}">
    <label for="high_school_name" class="control-label">{{ 'High School Name' }}</label>
    <input class="form-control" name="high_school_name" type="text" id="high_school_name" value="{{ isset($application->high_school_name) ? $application->high_school_name : ''}}" >
    {!! $errors->first('high_school_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('high_school_graduation_year') ? 'has-error' : ''}}">
    <label for="high_school_graduation_year" class="control-label">{{ 'High School Graduation Year' }}</label>
    <input class="form-control" name="high_school_graduation_year" type="text" id="high_school_graduation_year" value="{{ isset($application->high_school_graduation_year) ? $application->high_school_graduation_year : ''}}" >
    {!! $errors->first('high_school_graduation_year', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('high_school_major') ? 'has-error' : ''}}">
    <label for="high_school_major" class="control-label">{{ 'High School Major' }}</label>
    <input class="form-control" name="high_school_major" type="text" id="high_school_major" value="{{ isset($application->high_school_major) ? $application->high_school_major : ''}}" >
    {!! $errors->first('high_school_major', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('university_name') ? 'has-error' : ''}}">
    <label for="university_name" class="control-label">{{ 'University Name' }}</label>
    <input class="form-control" name="university_name" type="text" id="university_name" value="{{ isset($application->university_name) ? $application->university_name : ''}}" >
    {!! $errors->first('university_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('university_graduation_year') ? 'has-error' : ''}}">
    <label for="university_graduation_year" class="control-label">{{ 'University Graduation Year' }}</label>
    <input class="form-control" name="university_graduation_year" type="text" id="university_graduation_year" value="{{ isset($application->university_graduation_year) ? $application->university_graduation_year : ''}}" >
    {!! $errors->first('university_graduation_year', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('university_major') ? 'has-error' : ''}}">
    <label for="university_major" class="control-label">{{ 'University Major' }}</label>
    <input class="form-control" name="university_major" type="text" id="university_major" value="{{ isset($application->university_major) ? $application->university_major : ''}}" >
    {!! $errors->first('university_major', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('parent_name') ? 'has-error' : ''}}">
    <label for="parent_name" class="control-label">{{ 'Parent Name' }}</label>
    <input class="form-control" name="parent_name" type="text" id="parent_name" value="{{ isset($application->parent_name) ? $application->parent_name : ''}}" >
    {!! $errors->first('parent_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('relationship') ? 'has-error' : ''}}">
    <label for="relationship" class="control-label">{{ 'Relationship' }}</label>
    <input class="form-control" name="relationship" type="text" id="relationship" value="{{ isset($application->relationship) ? $application->relationship : ''}}" >
    {!! $errors->first('relationship', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('parent_phone_number') ? 'has-error' : ''}}">
    <label for="parent_phone_number" class="control-label">{{ 'Parent Phone Number' }}</label>
    <input class="form-control" name="parent_phone_number" type="text" id="parent_phone_number" value="{{ isset($application->parent_phone_number) ? $application->parent_phone_number : ''}}" >
    {!! $errors->first('parent_phone_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('parent_email') ? 'has-error' : ''}}">
    <label for="parent_email" class="control-label">{{ 'Parent Email' }}</label>
    <input class="form-control" name="parent_email" type="text" id="parent_email" value="{{ isset($application->parent_email) ? $application->parent_email : ''}}" >
    {!! $errors->first('parent_email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date3') ? 'has-error' : ''}}">
    <label for="date3" class="control-label">{{ 'Date3' }}</label>
    <input class="form-control" name="date3" type="text" id="date3" value="{{ isset($application->date3) ? $application->date3 : ''}}" >
    {!! $errors->first('date3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('examination1') ? 'has-error' : ''}}">
    <label for="examination1" class="control-label">{{ 'Examination1' }}</label>
    <input class="form-control" name="examination1" type="text" id="examination1" value="{{ isset($application->examination1) ? $application->examination1 : ''}}" >
    {!! $errors->first('examination1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('organization_of_issue1') ? 'has-error' : ''}}">
    <label for="organization_of_issue1" class="control-label">{{ 'Organization Of Issue1' }}</label>
    <input class="form-control" name="organization_of_issue1" type="text" id="organization_of_issue1" value="{{ isset($application->organization_of_issue1) ? $application->organization_of_issue1 : ''}}" >
    {!! $errors->first('organization_of_issue1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('grade1') ? 'has-error' : ''}}">
    <label for="grade1" class="control-label">{{ 'Grade1' }}</label>
    <input class="form-control" name="grade1" type="text" id="grade1" value="{{ isset($application->grade1) ? $application->grade1 : ''}}" >
    {!! $errors->first('grade1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('score1') ? 'has-error' : ''}}">
    <label for="score1" class="control-label">{{ 'Score1' }}</label>
    <input class="form-control" name="score1" type="text" id="score1" value="{{ isset($application->score1) ? $application->score1 : ''}}" >
    {!! $errors->first('score1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date1') ? 'has-error' : ''}}">
    <label for="date1" class="control-label">{{ 'Date1' }}</label>
    <input class="form-control" name="date1" type="text" id="date1" value="{{ isset($application->date1) ? $application->date1 : ''}}" >
    {!! $errors->first('date1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('examination2') ? 'has-error' : ''}}">
    <label for="examination2" class="control-label">{{ 'Examination2' }}</label>
    <input class="form-control" name="examination2" type="text" id="examination2" value="{{ isset($application->examination2) ? $application->examination2 : ''}}" >
    {!! $errors->first('examination2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('organization_of_issue2') ? 'has-error' : ''}}">
    <label for="organization_of_issue2" class="control-label">{{ 'Organization Of Issue2' }}</label>
    <input class="form-control" name="organization_of_issue2" type="text" id="organization_of_issue2" value="{{ isset($application->organization_of_issue2) ? $application->organization_of_issue2 : ''}}" >
    {!! $errors->first('organization_of_issue2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('grade2') ? 'has-error' : ''}}">
    <label for="grade2" class="control-label">{{ 'Grade2' }}</label>
    <input class="form-control" name="grade2" type="text" id="grade2" value="{{ isset($application->grade2) ? $application->grade2 : ''}}" >
    {!! $errors->first('grade2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('score2') ? 'has-error' : ''}}">
    <label for="score2" class="control-label">{{ 'Score2' }}</label>
    <input class="form-control" name="score2" type="text" id="score2" value="{{ isset($application->score2) ? $application->score2 : ''}}" >
    {!! $errors->first('score2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date2') ? 'has-error' : ''}}">
    <label for="date2" class="control-label">{{ 'Date2' }}</label>
    <input class="form-control" name="date2" type="text" id="date2" value="{{ isset($application->date2) ? $application->date2 : ''}}" >
    {!! $errors->first('date2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('examination3') ? 'has-error' : ''}}">
    <label for="examination3" class="control-label">{{ 'Examination3' }}</label>
    <input class="form-control" name="examination3" type="text" id="examination3" value="{{ isset($application->examination3) ? $application->examination3 : ''}}" >
    {!! $errors->first('examination3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('organization_of_issue3') ? 'has-error' : ''}}">
    <label for="organization_of_issue3" class="control-label">{{ 'Organization Of Issue3' }}</label>
    <input class="form-control" name="organization_of_issue3" type="text" id="organization_of_issue3" value="{{ isset($application->organization_of_issue3) ? $application->organization_of_issue3 : ''}}" >
    {!! $errors->first('organization_of_issue3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('grade3') ? 'has-error' : ''}}">
    <label for="grade3" class="control-label">{{ 'Grade3' }}</label>
    <input class="form-control" name="grade3" type="text" id="grade3" value="{{ isset($application->grade3) ? $application->grade3 : ''}}" >
    {!! $errors->first('grade3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('score3') ? 'has-error' : ''}}">
    <label for="score3" class="control-label">{{ 'Score3' }}</label>
    <input class="form-control" name="score3" type="text" id="score3" value="{{ isset($application->score3) ? $application->score3 : ''}}" >
    {!! $errors->first('score3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date3') ? 'has-error' : ''}}">
    <label for="date3" class="control-label">{{ 'Date3' }}</label>
    <input class="form-control" name="date3" type="text" id="date3" value="{{ isset($application->date3) ? $application->date3 : ''}}" >
    {!! $errors->first('date3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('changed') ? 'has-error' : ''}}">
    <label for="changed" class="control-label">{{ 'Changed' }}</label>
    <select class="form-control" name="changed" type="text" id="changed" value="{{ isset($application->changed) ? $application->changed : ''}}" >
        <option value="0">No</option>    
        <option value="1">Yes</option>    
    </select>
    {!! $errors->first('changed', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@if(!Request::is('admin/application/create'))
    @push('js')
        <script type="text/javascript">
            $.get(
                '/admin/getgroup',
                {
                    'faculty_id' : {{ $application->faculty_id }},
                    'group_id' : {{ $application->group_id }}
                },
                function(response){
                    $('#group_id').html(response);
                }
            );
        </script>    
    @endpush
@endif