@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Application {{ $application->id }}</div>
                    <div class="card-body">

                        <a href="{{ url()->previous() }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/application/' . $application->id . '/edit') }}" title="Edit Application"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/application' . '/' . $application->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Application" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                   {{--  <tr>
                                        <th>ID</th><td>{{ $application->id }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th> Photo </th>
                                        <td> 
                                            <img src="{!! $application->photo ? url('storage/photos/'.$application->photo) : url('storage/photos/no-photo.png') !!}" class="img-fluid img-thumbnail" style="height: 200px; width:200px; object-fit: cover;"> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Firstname </th>
                                        <td> {{ $application->firstname }} </td>
                                    </tr>
                                    <tr>
                                        <th> Lastname </th>
                                        <td> {{ $application->lastname }} </td>
                                    </tr>
                                    <tr>
                                        <th> Apply no. </th>
                                        <td> {{ $application->apply_no }} </td>
                                    </tr>
                                    <tr>
                                        <th> Student ID </th>
                                        <td> {{ $application->student_id }} </td>
                                    </tr>
                                    <tr>
                                        <th> Gender </th>
                                        <td> {{ $application->gender ? 'Male' : 'Female' }} </td>
                                    </tr>
                                    <tr>
                                        <th> Date of birth </th>
                                        <td> {{ $application->date_of_birth }} </td>
                                    </tr>
                                    <tr>
                                        <th> Status </th>
                                        <td> {{ $application->status ? 'Student' : '' }} </td>
                                    </tr>
                                    <tr>
                                        <th> Faculty </th>
                                        <td> {{ $application->faculty->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Course </th>
                                        <td> {{ isset($application->group->course) ? $application->group->course : '' }} </td>
                                    </tr>
                                    <tr>
                                        <th> Group </th>
                                        <td> {{ isset($application->group->name)  ? $application->group->course : '' }} </td>
                                    </tr>
                                    <tr>
                                        <th> Examination language </th>
                                        <td> 
                                            <?php 
            			                        switch($application->examination_language){
            			                            case '1':
            			                                echo "Uzbek";
            			                                break;
            			                            case '2':
            			                                echo "Russian";
            			                                break;    
            			                            case '3':
            			                                echo "Korean";
            			                                break;
            			                        }
            			                    ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Application status </th>
                                        <td> {{ $application->changed ? 'Changed' : 'Not changed' }} </td>
                                    </tr>
                                    <tr>
                                        <th> Nationality </th>
                                        <td> {{ $application->nationality }} </td>
                                    </tr>
                                    <tr>
                                        <th> Passport </th>
                                        <td> {{ $application->passport }} </td>
                                    </tr>
                                    <tr>
                                        <th> Phone number </th>
                                        <td> {{ $application->phone_number }} </td>
                                    </tr>
                                    <tr>
                                        <th> Email </th>
                                        <td> {{ $application->email }} </td>
                                    </tr>
                                    <tr>
                                        <th> Address </th>
                                        <td> {{ $application->address }} </td>
                                    </tr>
                                    @if($application->photo)
                                    <tr>
                                        <th> Photo (3x4) </th>
                                        <td> <a href="{{url('storage/applications/'.$application->photo)}}" download="{{url('storage/applications/'.$application->photo)}}">Download pdf</a></td>
                                    </tr>
                                    @endif
                                    @if($application->diploma_copy)
                                    <tr>
                                        <th> Diploma copy </th>
                                        <td> <a href="{{url('storage/applications/'.$application->diploma_copy)}}" download="{{url('storage/applications/'.$application->diploma_copy)}}">Download pdf</a></td>
                                    </tr>
                                    @endif
                                    @if($application->passport_copy)
                                    <tr>
                                        <th> Passport copy </th>
                                        <td> <a href="{{url('storage/applications/'.$application->passport_copy)}}" download="{{url('storage/applications/'.$application->passport_copy)}}">Download pdf</a></td>
                                    </tr>
                                    @endif
                                    @if($application->inn_copy)
                                    <tr>
                                        <th> Inn copy </th>
                                        <td> <a href="{{url('storage/applications/'.$application->inn_copy)}}" download="{{url('storage/applications/'.$application->inn_copy)}}">Download pdf</a></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <th> Work place </th>
                                        <td> {{ $application->work_place }} </td>
                                    </tr>
                                    <tr>
                                        <th> Work phone number </th>
                                        <td> {{ $application->work_phone_number }} </td>
                                    </tr>
                                    <tr>
                                        <th> High school name </th>
                                        <td> {{ $application->high_school_name }} </td>
                                    </tr>
                                    <tr>
                                        <th> High school graduation year </th>
                                        <td> {{ $application->high_school_graduation_year }} </td>
                                    </tr>
                                    <tr>
                                        <th> High school major </th>
                                        <td> {{ $application->high_school_major }} </td>
                                    </tr>
                                    <tr>
                                        <th> University name </th>
                                        <td> {{ $application->university_name }} </td>
                                    </tr>
                                    <tr>
                                        <th> University graduation year </th>
                                        <td> {{ $application->university_graduation_year }} </td>
                                    </tr>
                                    <tr>
                                        <th> University major </th>
                                        <td> {{ $application->university_major }} </td>
                                    </tr>
                                    <tr>
                                        <th> Parent name </th>
                                        <td> {{ $application->parent_name }} </td>
                                    </tr>
                                    <tr>
                                        <th> Parent phone number </th>
                                        <td> {{ $application->parent_phone_number }} </td>
                                    </tr>
                                    <tr>
                                        <th> Parent email </th>
                                        <td> {{ $application->parent_email }} </td>
                                    </tr>
                                    <tr>
                                        <th> Examination 1 </th>
                                        <td> {{ $application->examination1 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Organization of issue 1</th>
                                        <td> {{ $application->organization_of_issue1 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Grade 1</th>
                                        <td> {{ $application->grade1 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Score 1</th>
                                        <td> {{ $application->score1 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Date 1</th>
                                        <td> {{ $application->date1 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Examination 2 </th>
                                        <td> {{ $application->examination2 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Organization of issue 2</th>
                                        <td> {{ $application->organization_of_issue2 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Grade 2</th>
                                        <td> {{ $application->grade2 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Score 2</th>
                                        <td> {{ $application->score2 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Date 2</th>
                                        <td> {{ $application->date2 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Examination 3 </th>
                                        <td> {{ $application->examination3 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Organization of issue 3</th>
                                        <td> {{ $application->organization_of_issue3 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Grade 3</th>
                                        <td> {{ $application->grade3 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Score 3</th>
                                        <td> {{ $application->score3 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Date 3</th>
                                        <td> {{ $application->date3 }} </td>
                                    </tr>
                                    <tr>
                                        <th> Test</th>
                                        <td> {{ $application->test }} </td>
                                    </tr>
                                    <tr>
                                        <th> Interview</th>
                                        <td> {{ $application->interview }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @if(count($talk_old)>0)
                            <div class="table-responsive" style="padding: 5px; border: solid #85ce36;">
                            @foreach ($talk_old as $key => $val)
                                <h3 class="text-left">{{$val->user->phone}}</h3>
                                <table class="table table-condensed table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>CRITERIA-1</th>
                                        <th>CRITERIA-2</th>
                                        <th>CRITERIA-3</th>
                                        <th>CRITERIA-4</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="number" name="criteria1" id="criteria1" class="form-control" value="{{$val->criteria1}}" readonly="true">
                                        </td>
                                        <td>
                                            <input type="number" name="criteria2" id="criteria2" class="form-control" value="{{$val->criteria2}}" readonly="true">
                                        </td>
                                        <td>
                                            <input type="number" name="criteria3" id="criteria3" class="form-control" value="{{$val->criteria3}}" readonly="true">
                                        </td>
                                        <td>
                                            <input type="number" name="criteria4" id="criteria4" class="form-control" value="{{$val->criteria4}}" readonly="true">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Comment:</td>
                                        <td colspan="3" > {{$val->comment}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" > Total: <span id="total">{{$val->score}}</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        
                            @endforeach                           
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
