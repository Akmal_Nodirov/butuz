@push('css')
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endpush

<div class="form-group {{ $errors->has('title_uz') ? 'has-error' : ''}}">
    <label for="title_uz" class="control-label">{{ 'Title Uz' }}</label>
    <input class="form-control" name="title_uz" type="text" id="title_uz" value="{{ isset($page->title_uz) ? $page->title_uz : ''}}" >
    {!! $errors->first('title_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_ru') ? 'has-error' : ''}}">
    <label for="title_ru" class="control-label">{{ 'Title Ru' }}</label>
    <input class="form-control" name="title_ru" type="text" id="title_ru" value="{{ isset($page->title_ru) ? $page->title_ru : ''}}" >
    {!! $errors->first('title_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_en') ? 'has-error' : ''}}">
    <label for="title_en" class="control-label">{{ 'Title En' }}</label>
    <input class="form-control" name="title_en" type="text" id="title_en" value="{{ isset($page->title_en) ? $page->title_en : ''}}" >
    {!! $errors->first('title_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('title_kr') ? 'has-error' : ''}}">
    <label for="title_kr" class="control-label">{{ 'Title Kr' }}</label>
    <input class="form-control" name="title_kr" type="text" id="title_kr" value="{{ isset($page->title_kr) ? $page->title_kr : ''}}" >
    {!! $errors->first('title_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_uz') ? 'has-error' : ''}}">
    <label for="content_uz" class="control-label">{{ 'Content Uz' }}</label>
    <textarea class="form-control" rows="5" name="content_uz" type="textarea" id="content_uz" >{{ isset($page->content_uz) ? $page->content_uz : ''}}</textarea>
    {!! $errors->first('content_uz', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_ru') ? 'has-error' : ''}}">
    <label for="content_ru" class="control-label">{{ 'Content Ru' }}</label>
    <textarea class="form-control" rows="5" name="content_ru" type="textarea" id="content_ru" >{{ isset($page->content_ru) ? $page->content_ru : ''}}</textarea>
    {!! $errors->first('content_ru', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_en') ? 'has-error' : ''}}">
    <label for="content_en" class="control-label">{{ 'Content En' }}</label>
    <textarea class="form-control" rows="5" name="content_en" type="textarea" id="content_en" >{{ isset($page->content_en) ? $page->content_en : ''}}</textarea>
    {!! $errors->first('content_en', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('content_kr') ? 'has-error' : ''}}">
    <label for="content_kr" class="control-label">{{ 'Content Kr' }}</label>
    <textarea class="form-control" rows="5" name="content_kr" type="textarea" id="content_kr" >{{ isset($page->content_kr) ? $page->content_kr : ''}}</textarea>
    {!! $errors->first('content_kr', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <select name="type" class="form-control" id="type" >
    @foreach (json_decode('{"0":"About","1":"Education"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($page->type) && $page->type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('published') ? 'has-error' : ''}}">
    <label for="published" class="control-label">{{ 'Published' }}</label>
    <select name="published" class="form-control" id="published" >
    @foreach (json_decode('{"0": "No", "1": "Yes"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($page->published) && $page->published == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>

@push('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('#content_uz').summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#content_ru").summernote({
               callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               } 
            });
            
            $("#content_en").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
            
            $("#content_kr").summernote({
                callbacks: {
                    onImageUpload: function(files) {
                        var el = $(this);
                        sendFile(files[0],el);
                    }
               }
            });
    });
    </script>
@endpush