@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Page {{ $page->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/page') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/page/' . $page->id . '/edit') }}" title="Edit Page"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/page' . '/' . $page->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Page" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                   {{--  <tr>
                                        <th>ID</th><td>{{ $page->id }}</td>
                                    </tr> --}}
                                    <tr><th> Title Uz </th><td> {{ $page->title_uz }} </td></tr>
                                    <tr><th> Title Ru </th><td> {{ $page->title_ru }} </td></tr>
                                    <tr><th> Title En </th><td> {{ $page->title_en }} </td></tr>
                                    <tr><th> Title Kr </th><td> {{ $page->title_kr }} </td></tr>
                                    <tr><th> Content Uz </th><td> {!! $page->content_uz !!} </td></tr>
                                    <tr><th> Content Ru </th><td> {!! $page->content_ru !!} </td></tr>
                                    <tr><th> Content En </th><td> {!! $page->content_en !!} </td></tr>
                                    <tr><th> Content Kr </th><td> {!! $page->content_kr !!} </td></tr>
                                    <tr><th> Type </th><td> {!! ($page->type == 0) ? '<span class="badge badge-success">About</span>' : '<span class="badge badge-danger">Education</span>' !!} </td></tr>
                                    <tr><th> Published </th><td> {!! ($page->published == 0) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
