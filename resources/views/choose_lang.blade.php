<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Bucheon University in Tashkent</title>
	<!-- FancyBox -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('images/apple-icon-114x114.png')}}">
    
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('images/apple-icon-72x72.png')}}">
    
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/apple-icon-57x57.png')}}">
    
    <!-- Library - Bootstrap v3.3.5 -->
    <link rel="stylesheet" type="text/css" href="{{asset('libraries/lib.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('libraries/Stroke-Gap-Icon/stroke-gap-icon.css')}}">
    
    <!-- Custom - Common CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/navigation-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('libraries/lightslider-master/lightslider.css')}}">
    
    <!-- Custom - Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/shortcode.css')}}">
    <!--[if lt IE 9]>       
        <script src="js/html5/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">


	<section class="mt_200">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mt_50">
					<img src="images/logo.png" class="img_intro" alt="">
					<h2 class="text-center">BUCHEON UNIVERSITY IN TASHKENT</h2>
					
				</div>
			</div>
			<div class="row mt_50">
				<div class="col-md-12">
					<div class="intro_block">

						<a href="{{url('/language/uz')}}">
							<span>Saytni O'zbek tilida ochish</span>
							<div class="block_10 padding_0 choose_l">
								<img width="100%" src="but/uzb.jpg">
							</div>
						</a>

						<a href="{{url('/language/kr')}}">
							<span>한국어 웹 사이트 열기</span>
							<div class="block_10 padding_0 choose_l">
								<img width="100%" src="but/kor.jpg">
							</div>
						</a>

						<a href="{{url('/language/ru')}}">
							<span>Открыть сайт на русском языке </span>
							<div class="block_10 padding_0 choose_l">
								<img width="100%" src="but/rus.jpg">
							</div>
						</a>

						<a href="{{url('/language/en')}}">
							<span>Open site in English</span>
							<div class="block_10 padding_0 choose_l">
								<img width="100%" src="but/gb.jpg">
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- JQuery v1.11.3 -->
    <script src="{{asset('js/jquery.min.js')}}"></script>

    <!-- Library - Js -->
    <script src="{{asset('libraries/lib.js')}}"></script><!-- Bootstrap JS File v3.3.5 -->
    <script src="{{asset('libraries/jquery.countdown.min.js')}}"></script>

    <script src="{{asset('libraries/lightslider-master/lightslider.js')}}"></script>
    <!-- Library - Google Map API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn3Z6i1AYolP3Y2SGis5qhbhRwmxxo1wU"></script>
    <script src="{{asset('js/functions.js')}}"></script>
</body>
</html>