<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="" lang="{{ str_replace('_', '-', app()->getLocale()) }}"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <!-- FancyBox -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <!-- Standard Favicon -->
    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />
    
    <!-- For iPhone 4 Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('images/apple-icon-114x114.png')}}">
    
    <!-- For iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('images/apple-icon-72x72.png')}}">
    
    <!-- For iPhone: -->
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/apple-icon-57x57.png')}}">
    
    <!-- Library - Bootstrap v3.3.5 -->
    <link rel="stylesheet" type="text/css" href="{{asset('libraries/lib.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('libraries/Stroke-Gap-Icon/stroke-gap-icon.css')}}">
    
    <!-- Custom - Common CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/navigation-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('libraries/lightslider-master/lightslider.css')}}">
    
    <!-- Custom - Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/shortcode.css')}}">
    <!--[if lt IE 9]>       
        <script src="js/html5/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    
    <style type="text/css">
        .pagination > li > a, .pagination > li > span {
            color: #FECA2B;
            border-color: #FECA2B;
        }
        .pagination > .disabled > a, .pagination > .disabled > a:focus, .pagination > .disabled > a:hover, .pagination > .disabled > span, .pagination > .disabled > span:focus, .pagination > .disabled > span:hover {
            color: #FECA2B;
            border-color: #FECA2B;
        }
        .pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover{
            background-color: #FECA2B;
            border-color: #FECA2B;
        }
    </style>

    @stack('css')

</head>

<body data-offset="200" data-spy="scroll" data-target=".ow-navigation">
    
    {{--
    <div id="site-loader" class="load-complete">
        <div class="loader">
            <div class="loader-inner ball-clip-rotate">
                <div>
                    
                </div>
                <img src="images/logo.png" alt="logo"/>
            </div>
        </div>
    </div> 
    --}}
    
    <!-- Header -->
    <header class="header-main container-fluid no-padding">
        <!-- Top Header -->
        <div class="top-header container-fluid no-padding">
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <!-- Social -->
                    <div class="col-md-4 col-sm-4 col-xs-6 social">
                        <ul>
                            <li><a  href="{{url('lang/uz')}}"><img width="100%" src="{{asset('but/uzb.jpg')}}"></a></li>
                            <li><a  href="{{url('lang/ru')}}"><img width="100%" src="{{asset('but/rus.jpg')}}"></a></li>  
                            <li><a  href="{{url('lang/kr')}}"><img width="100%" src="{{asset('but/kor.jpg')}}"></a></li>                   
                            <li><a  href="{{url('lang/en')}}"><img width="100%" src="{{asset('but/gb.jpg')}}"></a></li>
                            
                        </ul>
                    </div><!-- Social /- -->
                    
                    <!-- Register -->
                    <div class="col-md-4 col-sm-4 col-xs-6 register">
                        <!-- <a href="#" title="Register">Apply now</a> -->
                        <p><i class="fa fa-phone" aria-hidden="true"></i>{{$about->tel1}}<br> {{$about->tel2}}<br> {{$about->tel3}}</p>
                    </div><!-- Register /- -->

                    <!-- Logo Block -->
                    <div class="col-md-4 col-sm-4 col-xs-12 logo-block">
                        <a href="{{url('/')}}" title="Logo">
                            <img src="{{asset('images/logo.png')}}" alt="logo" width="46" height="41"/>
                            <h3>{{__('message.university_name')}}</h3></h3>
                        </a>
                    </div><!-- Logo Block /- -->
                </div>
            </div><!-- Container /- -->
        </div><!-- Top Header /- -->

        <!-- Menu Block -->
        <div class="menu-block container-fluid no-padding">
            <!-- Container -->
            <div class="container">
                <!-- User -->
            <!--    <a href="#" class="user" title="User">
                    <i class="fa fa-user"></i>
                </a>-->

                <!-- User /- -->
                
                
                {{--<div class="menu-search">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="{{__('message.enter_your_search_term')}}" type="text" value="" name="search" id="search" />
                            <button class="sb-search-submit"><i class="fa fa-search"></i></button>
                            <span class="sb-icon-search"></span>
                        </form>
                    </div>
                </div>--}}
                
                
                <div class="col-md-12 col-sm-12" style="padding:0 !important;">
                    <!-- Navigation -->
                    <nav class="navbar ow-navigation">
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a title="Logo" href="#" class="navbar-brand"><img src="images/logo.png" alt="logo" width="46" height="41"/><span>BUT</span></a>
                        </div>
                        <div class="navbar-collapse collapse" id="navbar">
                            <ul class="nav navbar-nav menubar">
                                <li class="{{ Request::is('/') ? 'active' : '' }}"><a title="{{__('message.menu_home')}}" href="{{url('/')}}">{{__('message.menu_home')}}</a></li>

                                <li class="nav-item dropdown {{ (Request::is('about*') | Request::is('administration')) ? 'active' : '' }}">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{__('message.menu_introduction')}} 
                                    </a>
                                    <div class="dropdown-menu border_a" aria-labelledby="navbarDropdown">
                                        @foreach($pages as $page)
                                            @if($page->type == 0)
                                                <a class="dropdown-item dropdown-divider" style="text-transform: uppercase" href="{{ url('/about/'.$page->id) }}">{{ $page['title_'.app()->getLocale()] }}</a>
                                            @endif    
                                        @endforeach
                                        <a class="dropdown-item" style="text-transform:uppercase"  href="{{url('/administration')}}">{{__('message.administration')}}</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown {{ (Request::is('education*') | Request::is('faculties')) ? 'active' : '' }}">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{__('message.menu_education')}} 
                                    </a>
                                    <div class="dropdown-menu border_a" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item dropdown-divider" style="text-transform:uppercase"  href="{{url('/faculties')}}">{{__('message.faculties')}}</a>
                                        @foreach($pages as $page)
                                            @if($page->type == 1)
                                                <a class="dropdown-item dropdown-divider" style="text-transform:uppercase" href="{{url('/education/'.$page->id)}}">{{ $page['title_'.app()->getLocale()] }}</a>
                                            @endif
                                        @endforeach
                                    </div>
                                </li>
                                <li class="{{ Request::is('announcements') ? 'active' : '' }}"><a title="{{__('message.menu_announcement')}}" href="{{url('/announcements')}}">{{__('message.menu_announcement')}}</a></li>
                                {{-- <li class="{{ Request::is('all_news') ? 'active' : '' }}"><a title="{{__('message.menu_news')}}" href="{{url('/all_news')}}">{{__('message.menu_news')}}</a></li> --}}
                                <li class="{{ Request::is('rights_and_duties') ? 'active' : '' }}"><a title="{{__('message.rights_and_duties')}}" href="{{url('/rights_and_duties')}}">{{__('message.rights_and_duties')}}</a></li>
                                <li class="{{ Request::is('events') ? 'active' : '' }}"><a title="{{__('message.menu_gallery')}}" href="{{url('/events')}}">{{__('message.menu_gallery')}}</a></li>
                                <li  class="{{ (Request::is('agreement') | Request::is('application')) ? 'active' : '' }}"><a title="{{__('message.menu_apply')}}" href="{{url('/agreement')}}" >{{__('message.menu_apply')}}</a></li>
                        
                                <li class="{{ Request::is('faq') ? 'active' : '' }}"><a title="{{__('message.menu_faq')}}" href="{{url('/faq')}}">{{__('message.menu_faq')}}</a></li>
                                <li class="{{ Request::is('contact') ? 'active' : '' }}"><a title="{{__('message.menu_contact_us')}}" href="{{ url('/contact') }}">{{__('message.menu_contact_us')}}</a></li>
                                <li class="{{ Request::is('login') ? 'active' : '' }}"><a class="crm_button" title="{{__('message.portal_system')}}" href="{{url('/to_login')}}">{!!__('message.menu_entrance')!!}</a></li>
                            </ul>
                            
                        </div>
                    </nav>
                    <!-- Navigation /- -->
                </div>
            </div><!-- Container /- -->
        </div><!-- Menu Block /- -->
    </header><!-- Header /- -->
   {{-- <div ><a class="fixed_link"  href="{{url('/agreement')}}">{{__('message.apply_fixed')}}</a></div> --}}
    
    
<div class="modal modal_radius" tabindex="-1" id="openModal" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header modal_bg">
          <button type="button" class="close modal_close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-close"></i></i></span>
          </button>
      </div>
      <div class="modal-body">
         <h1>
           {{-- {!!__('message.test_opublikovani')!!} --}}
         </h1>
         <h4>{!!__('message.modal_t1')!!}</h4>
         <p>{!!__('message.modal_tex01')!!}</p>
         <p>{!!__('message.modal_tex02')!!}</p>
         <p><b>{!!__('message.modal_tex03_bold')!!}</b></p>
         <p><b>{!!__('message.modal_tex04_bold')!!}</b></p>
         <ul>
             <li>{!!__('message.modal_tex_ul1')!!}</li>
             <li>{!!__('message.modal_tex_ul2')!!}</li>
             <li>{!!__('message.modal_tex_ul3')!!}</li>
             <li>{!!__('message.modal_tex_ul4')!!}</li>
         </ul>
         <p>{!!__('message.modal_foot_1')!!}</p>
         <p><b>{!!__('message.modal_foot_2')!!}</b></p>
      </div>
      <div class="modal-footer modal_bg">
        <button type="button" class="btn modal_close_2" data-dismiss="modal">{{__('message.modal_close')}}</button>
      </div>
   </div>
  </div>
</div>
    

    @yield('content')
    
    <!-- Footer Main -->
    <footer class="footer-main container-fluid no-padding">
        <!-- Container -->
        <div class="container">
            <!-- Footer About -->


            <div class="row">
                <!-- Customer Service Widget -->
                <aside class="col-md-3 col-sm-6 col-xs-6 widget widget_customer_services catchphrase">

                    <img src="/storage/logo/{{$about->logo}}" alt="logo" width="56" class="catchphrase_img">
                    <div class="catchphrase_text">{!! $about['slogan_'.app()->getLocale()] !!}</div>

                </aside><!-- Customer Service Widget /- -->

                <!-- Quick Links Widget -->
                <aside class="col-md-3 col-sm-6 col-xs-6 widget widget_quick_links">
                    <h3 class="block-title">{{__('message.follow_us_on')}}:</h3>
                    <div>
                        <a href="{{ $about->facebook }}"><i class="fab fa-facebook-square"></i></a>
                        <a href="{{ $about->telegram }}"><i class="fab fa-telegram"></i></a>
                        <a href="{{ $about->youtube }}"><i class="fab fa-youtube"></i></a>
                    </div>

                </aside><!-- Quick Links Widget /- -->

                <!-- ContactUs Widget -->
                <aside class="col-md-6 col-sm-6 col-xs-6 widget widget_contactus">
                    <h3 class="block-title">{{__('message.contact_us')}}</h3>
                    <div class="contactinfo-box">
                        <i class="fas fa-map-marker-alt"></i>
                        {!! $about['address_'.app()->getLocale()] !!}
                    </div>
                    <div class="contactinfo-box">
                        <i class="fa fa-phone"></i>
                        <p>
                            <a title="{{$about['tel1']}}" href="tel:{{$about['tel1']}}">{{$about['tel1']}}</a>
                            <a title="{{$about['tel2']}}" href="tel:{{$about['tel2']}}">{{$about['tel2']}}</a>
                        </p>
                    </div>
                    <div class="contactinfo-box">
                        <i class="fa fa-envelope"></i>
                        <p>
                            <a href="mailto:{{$about['email']}}" title="{{$about['email']}}">{{$about['email']}}</a>

                        </p>
                    </div>
                </aside><!-- ContactUs Widget /- -->

                <!-- NewsLetter Widget -->

            </div>
        </div><!-- Container /- --> 

        <!-- Container -->

    </footer><!-- Footer Main /- -->

    <!-- JQuery v1.11.3 -->
    <script src="{{asset('js/jquery.min.js')}}"></script>

    <!-- Library - Js -->
    <script src="{{asset('libraries/lib.js')}}"></script><!-- Bootstrap JS File v3.3.5 -->
    <script src="{{asset('libraries/jquery.countdown.min.js')}}"></script>

    <script src="{{asset('libraries/lightslider-master/lightslider.js')}}"></script>
    <!-- Library - Google Map API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCn3Z6i1AYolP3Y2SGis5qhbhRwmxxo1wU"></script>
    <script src="{{asset('js/functions.js')}}"></script>
    <script type="text/javascript">
    $(window).on('load',function(){
        $('#openModal').modal('show');
    });
</script>
    @stack('js')
</body>
</html>
