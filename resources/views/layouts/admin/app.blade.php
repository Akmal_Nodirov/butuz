<!DOCTYPE html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('css/vendor.css')}}">
    <!-- Theme initialization -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    @stack('css')
</head>
<body>
        <div class="main-wrapper">
            <div class="app" id="app">
                <header class="header">
                    <div class="header-block header-block-collapse d-lg-none d-xl-none">
                        <button class="collapse-btn" id="sidebar-collapse-btn">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    {{-- <div class="header-block header-block-search">
                        <form role="search">
                            <div class="input-container">
                                <i class="fa fa-search"></i>
                                <input type="search" placeholder="Search">
                                <div class="underline"></div>
                            </div>
                        </form>
                    </div> --}}
                    <div class="header-block header-block-nav">
                        <ul class="nav-profile">
                            <li class="profile dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    {{-- <div class="img" style="background-image: url('https://avatars3.githubusercontent.com/u/3959008?v=3&s=40')"> </div> --}}
                                    <span class="name"> {{ Auth::user()->phone }} </span>
                                </a>
                                <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
                                    {{-- <a class="dropdown-item" href="#">
                                        <i class="fa fa-user icon"></i> Profile </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fa fa-bell icon"></i> Notifications </a> --}}
                                    <a class="dropdown-item" href="{{url('admin/settings')}}">
                                        <i class="fa fa-gear icon"></i> Settings </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-power-off icon"></i> {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>
                </header>
                <aside class="sidebar">
                    <div class="sidebar-container">
                        <div class="sidebar-header">
                            <div class="brand">
                                <div class="logo">
                                    <span class="l l1"></span>
                                    <span class="l l2"></span>
                                    <span class="l l3"></span>
                                    <span class="l l4"></span>
                                    <span class="l l5"></span>
                                </div> {{ config('app.name') }} </div>
                        </div>
                        <nav class="menu">
                            <ul class="sidebar-menu metismenu" id="sidebar-menu">
                                <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                                    <a href="{{url('admin/dashboard')}}">
                                        <i class="fa fa-home"></i> Home </a>
                                </li>
                                <li class="{{ Request::is('admin/applicant') ? 'active' : '' }}">
                                    <a href="{{ url('admin/applicant') }}">
                                        <i class="fa fa-th-list"></i> Applicants </a>
                                </li>
                                <li class="{{ Request::is('admin/appresult') ? 'active' : '' }}">
                                    <a href="{{ url('admin/appresult') }}">
                                        <i class="fa fa-th-list"></i> App Results </a>
                                </li>
                                <li class="{{ Request::is('admin/interviewers') ? 'active' : '' }}">
                                    <a href="{{ url('admin/interviewers') }}">
                                        <i class="fa fa-th-list"></i> Interviewers </a>
                                </li>
                                <li class="{{ Request::is('admin/applicantsms') ? 'active' : '' }}">
                                    <a href="{{ url('admin/applicantsms') }}">
                                        <i class="fa fa-th-list"></i> Send SMS </a>
                                </li>
                                <li class="{{ Request::is('admin/about*') ? 'active' : '' }}">
                                    <a href="{{ url('admin/about/1/edit') }}">
                                        <i class="fa fa-dashboard"></i> About </a>
                                </li>
                                <li class="{{ Request::is('admin/page*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.page.index') }}">
                                        <i class="fa fa-file"></i> Pages </a>
                                </li>
                                <li class="{{ Request::is('admin/position*') ? 'active' : '' }}">
                                    <a href="{{ url('admin/position') }}">
                                        <i class="fa fa-list"></i> Position </a>
                                </li>
                                <li class="{{ Request::is('admin/administration*') ? 'active' : '' }}">
                                    <a href="{{ url('admin/administration') }}">
                                        <i class="fa fa-users"></i> Administration </a>
                                </li>
                                <li class="{{ Request::is('admin/slider*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.slider.index') }}">
                                        <i class="fa fa-camera"></i> Slider </a>
                                </li>
                                <li class="{{ Request::is('admin/announcement*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.announcement.index') }}">
                                        <i class="fa fa-globe"></i> Announcement </a>
                                </li>
                                <li class="{{ Request::is('admin/news*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.news.index') }}">
                                        <i class="fa fa-globe"></i> News </a>
                                </li>
                                <li class="{{ Request::is('admin/program*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.program.index') }}">
                                        <i class="fa fa-th-list"></i> Programs </a>
                                </li>
                                <li class="{{ Request::is('admin/event*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.event.index') }}">
                                        <i class="fa fa-th-list"></i> Events </a>
                                </li>
                                <li class="{{ Request::is('admin/image*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.image.index') }}">
                                        <i class="fa fa-image"></i> Images </a>
                                </li>
                                <li class="{{ Request::is('admin/message*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.message.index') }}">
                                        <?php
                                            $unread_messages = App\Message::where('seen',false)->count();
                                            $unanswered_messages = App\Message::where('answer', '=', null)->count();
                                        ?>
                                        <i class="fa fa-envelope"></i> Messages <br> <span style="color:red;font-weight:bold">[{{$unread_messages}} / {{$unanswered_messages}}] </span> </a>
                                </li>
                                <li class="{{ Request::is('admin/faq*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.faq.index') }}">
                                        <i class="fa fa-question"></i> FAQ </a>
                                </li>

                                <hr>
                                <li class="{{ Request::is('admin/subject*') ? 'active' : '' }}">
                                    <a href="{{ url('admin/subject') }}">
                                        <i class="fa fa-list-ol"></i> Subjects </a>
                                </li>
                                <li class="{{ Request::is('admin/faculty*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.faculty.index') }}">
                                        <i class="fa fa-th-list"></i> Faculties </a>
                                </li>
                                <li class="{{ Request::is('admin/group*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.group.index') }}">
                                        <i class="fa fa-list-alt"></i> Groups </a>
                                </li>
                                <li class="{{ Request::is('admin/professor*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.professor.index') }}">
                                        <i class="fa fa-user-circle k"></i> Professors </a>
                                </li>

                                <li class="{{ Request::is('admin/application*') ? 'active' : '' }}">
                                    <a href="{{ route('admin.application.index') }}">
                                        <i class="fa fa-graduation-cap"></i> Students </a>
                                </li>
                                <li class="{{ Request::is('admin/new_mark*') ? 'active' : '' }}">
                                    <a href="{{ url('admin/new_mark') }}">
                                        <i class="fa fa-list-ul"></i> Semesters </a>
                                </li>
                                <li class="{{ Request::is('admin/students_results*') ? 'active' : '' }}">
                                    <a href="{{ url('admin/students_results') }}">
                                        <i class="fa fa-list-ul"></i> Students results </a>
                                </li>
                                {{-- <li class="{{ Request::is('dean/student_results') ? 'active' : '' }}">
                                    <a href="{{ url('dean/student_results') }}">
                                        <i class="fa fa-list-ul"></i> Results </a>
                                </li> --}}
                                {{-- <li class="{{ Request::is('dean/user*') ? 'active' : '' }}">
                                    <a href="{{ route('dean.user.index') }}">
                                        <i class="fa fa-users"></i> Users </a>
                                </li> --}}
                            </ul>
                        </nav>
                    </div>

                </aside>
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                <div class="sidebar-mobile-menu-handle" id="sidebar-mobile-menu-handle"></div>
                <div class="mobile-menu-handle"></div>
                <article class="content dashboard-page">
                    <section class="section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        @yield('content')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>
                <footer class="footer">
                    <div class="footer-block buttons">
                       {{--  <iframe class="footer-github-btn" src="https://ghbtns.com/github-btn.html?user=modularcode&repo=modular-admin-html&type=star&count=true" frameborder="0" scrolling="0" width="140px" height="20px"></iframe> --}}
                    </div>
                    <div class="footer-block author">
                        <ul>
                            {{-- <li> created by
                                <a href="https://github.com/modularcode">ModularCode</a>
                            </li> --}}
                            <li>
                                <a href="/">{{ config('app.name', 'Laravel') }}</a> {{ date("Y") }}
                            </li>
                        </ul>
                    </div>
                </footer>
                <div class="modal fade" id="modal-media">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Media Library</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                            </div>
                            <div class="modal-body modal-tab-container">
                                <ul class="nav nav-tabs modal-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#gallery" data-toggle="tab" role="tab">Gallery</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#upload" data-toggle="tab" role="tab">Upload</a>
                                    </li>
                                </ul>
                                <div class="tab-content modal-tab-content">
                                    <div class="tab-pane fade" id="gallery" role="tabpanel">
                                        <div class="images-container">
                                            <div class="row"> </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade active in" id="upload" role="tabpanel">
                                        <div class="upload-container">
                                            <div id="dropzone">
                                                <form action="/" method="POST" enctype="multipart/form-data" class="dropzone needsclick dz-clickable" id="demo-upload">
                                                    <div class="dz-message-block">
                                                        <div class="dz-message needsclick"> Drop files here or click to upload. </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Insert Selected</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <div class="modal fade" id="confirm-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">
                                    <i class="fa fa-warning"></i> Alert</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure want to do this?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Yes</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
        {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="{{ asset('js/my.js') }}"></script>

        @stack('js')
    </body>
</html>
