@extends('layouts.app')
@section('title',__('message.menu_apply'))
@push('css')
    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
@endpush
@section('content')
	<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{__('message.menu_apply')}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{__('message.menu_apply')}}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- PageBanner /- -->
	<!-- Apply  -->

	<div class="container">
		<div class="row">
			<div class="col-lg-12 border">
				<h1 class="text-center">{{__('message.app_form_adm')}}</h1>
				<form action="{{url('/send_application')}}" name="test" method="post" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="radio_form">
						<h3 class="text-center">{{__('message.fac_name')}}</h3>
						{{-- <h5 class="text-center">Faculty of apply</h5> --}}
						{!! $errors->first('faculty_id', '<p class="p_val">:message</p>') !!}<br>
				        {!! $errors->first('exam_lang', '<p class="p_val">:message</p>') !!}
						<div class="radio_items">
							<div class="d-lg-flex justify-content-between">
								@foreach($faculties as $key => $faculty)
									<div class="item_1">
										<input type="radio" name="faculty_id"  id="faculty{{$faculty->id}}" value="{{$faculty->id}}">
										<span class="radio_text">
											<p>{{$faculty['name']}}</p>
										</span>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					
					<div class="radio_form">
						<h5 class="text-center" id="exam_lang">{{__('message.lan_group')}}</h5>
						<div class="radio_items">
							<div class="d-lg-flex justify-content-between">
								<div class="item_1" id="uzbek">
									<input type="radio" name="exam_lang" id="inlineRadio1" value="1">
									<span class="radio_text">
										<p>{{__('message.lan_uzbe')}}</p>
									</span>
								</div>
								<div class="item_2" id="russian">
									<input type="radio" name="exam_lang" id="inlineRadio2" value="2">
									<span class="radio_text">
										<p>{{__('message.lan_russ')}}</p>
									</span>
								</div>
								<div class="item_3" id="korean">
									<input type="radio" name="exam_lang" id="inlineRadio3" value="3">
									<span class="radio_text">
										<p>{{__('message.lan_kore')}}</p>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    
		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border text-center justify-content-center">
					<h1 class="text-center">{{__('message.pers_info_ap')}}</h1>
					<div class="block">

						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100">
							    
							    <h5>{{__('message.famil_y')}}<span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}">
								{!! $errors->first('lastname', '<p class="p_val">:message</p>') !!}
								<h5 class="red_text">{{__('message.err_va1')}}</h5>
								<h5 id="family"></h5>
							</label>
							<label for="" class="control-label wid100">
							    
							    <h5>{{__('message.first_name')}}<span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}">
								{!! $errors->first('firstname', '<p class="p_val">:message</p>') !!}
								<h5 class="red_text">{{__('message.err_va1')}}</h5>
								<h5 id="firstName"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.date_birth')}}<span style="color:red">*</span> </h5>
								<input type="text" readonly="true" class="form-control" name="date_of_birth" id="datepicker" value="{{ old('date_of_birth') }}">
								{!! $errors->first('date_of_birth', '<p class="p_val">:message</p>') !!}
								<h5 id="date"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.nationalit_y')}}<span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="nationality" value="{{ old('nationality') }}">
								{!! $errors->first('nationality', '<p class="p_val">:message</p>') !!}
								<h5 id="nationality"></h5>
							</label>
						</div>

						<div class="block">
							<div class="block_11 d-lg-flex justify-content-between">
								<label for="" class="control-label wid100"><h5> {{__('message.pass_serial_number')}}<span style="color:red">*</span> </h5>
									<input type="text" class="form-control" name="passport_number" value="{{ old('passport_number') }}">
									{!! $errors->first('passport_number', '<p class="p_val">:message</p>') !!}
									<h5 id="passport"></h5>
								</label>
								<label for="" class="control-label wid100"><h5> {{__('message.gende_r')}}<span style="color:red">*</span> </h5>
									<select type="text" class="form-control" name="gender">
										<option value="1" {{old('gender')=='1' ? 'checked' : ''}} >Мужчина</option>
										<option value="0" {{old('gender')=='0' ? 'checked' : ''}}>Женщина</option>
									</select>
									{!! $errors->first('gender', '<p class="p_val">:message</p>') !!}
									<h5 id="gender"></h5>
								</label>
								<label for="" class="control-label wid100"><h5>{{__('message.el_addr')}}<span style="color:red">*</span> </h5>
									<input type="text" class="form-control" name="email" value="{{ old('email') }}">
									{!! $errors->first('email', '<p class="p_val">:message</p>') !!}
									<h5 id="email"></h5>
								</label>
								<label for="" class="control-label wid100"><h5>{{__('message.tel_numb')}}<span style="color:red">*</span> </h5>
									<div class="form-group">
                                        <div class="input-group">
                                          <div class="input-group-addon">+998</div>
                                          <input type="text" class="form-control" id="phone" name="phone_number" placeholder="901234567"  value="{{ old('phone_number') }}">
                                        </div>
                                      </div>
									{!! $errors->first('phone_number', '<p class="p_val">:message</p>') !!}
									<h5 id="phone"></h5>
								</label>
							</div>
							<div class="block">
								<div class="block_11 d-lg-flex justify-content-between">
									<label for="" class="control-label"><h5>{{__('message.addr_es')}}<span style="color:red">*</span> </h5>
										<textarea class="form-control" name="address" id="" cols="50" rows="5"> {{ old('address') }}</textarea>
										{!! $errors->first('address', '<p class="p_val">:message</p>') !!}
									</label>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<section>
		    <div class="container m_auto border">
		    <div class="row">
		        <div class="col-md-12">
		            <h1 class="text-center">{{__('message.upl_req_doc')}}</h1>
		            <h4 class="pdf_upload_title">{{__('message.pdf_upload_title')}}</h4>
		            <i style="padding:5px 35px">{!!__('message.bl_appl_mai')!!}</i>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="block">
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="photo" class="control-label wid100 cursor_pointer"><span  class="butt_s w100" title="Подробнее">{{__('message.phot_o')}}<i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" name="photo" id="photo" style="display:none">
							</label>
						
							<label for="diploma_copy" class="control-label wid100 cursor_pointer"><span  class="butt_s w100" title="Подробнее">{{__('message.cop_of_dip')}} <i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" name="diploma_copy" id="diploma_copy" style="display:none">
							</label>
						
							<label for="passport_copy" class="control-label wid100 cursor_pointer"><span  class="butt_s w100" title="Подробнее">{{__('message.cop_of_pa')}}  <i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" name="passport_copy" id="passport_copy" style="display:none">
							</label>
					
							<label for="inn_copy" class="control-label wid100 cursor_pointer"><span  class="butt_s w100" title="Подробнее">{{__('message.cop_inn')}}<i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" id="inn_copy" name="inn_copy" style="display:none">
							</label>
						</div>
					</div>
		        </div>
		    </div>
		</div>
		</section>
		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border text-center justify-content-center">
					<h1 class="text-center">{{__('message.edu_back')}}</h1>
					<h3 class="text-center">{{__('message.hig_sch')}}  </h3>
					<div class="block">
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100"><h5>{{__('message.higs_col_ly')}} <span style="color:red">*</span> </h5>
								<div class="errorText"></div>
								<input id="highschool" type="text" name="highschool" class="form-control" value="{{ old('highschool') }}">
								{!! $errors->first('highschool', '<p class="p_val">:message</p>') !!}
								<h5 id="highschool"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.gradu_year')}}<span style="color:red">*</span>  </h5>
								<select class="form-control" name="grad_year" placeholder="{{__('message.gradu_year')}}">
								    <option></option>
								    @for($i=2019;$i>=1980;$i--)
								        <option value="{{$i}}" {{ (old('grad_year') == $i) ? 'selected' : '' }} >{{$i}}</option>
								    @endfor
								</select>
								{!! $errors->first('grad_year', '<p class="p_val">:message</p>') !!}
								<h5 id="graduation"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.major_ma')}} <span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="major" value="{{ old('major') }}">
								{!! $errors->first('major', '<p class="p_val">:message</p>') !!}
								<h5 id="major"></h5>
							</label>
						</div>

						<h3 class="text-center">{{__('message.univ_inf')}}</h3>
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100"><h5>{{__('message.univ_name')}}</h5>
								<input type="text" class="form-control" name="university_name" value="{{ old('university_name') }}">
								<h5 id="universityname"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.expir_date')}}</h5>
								<select class="form-control" name="university_graduation_year">
    								<option></option>
    								@for($i=2019;$i>=1980;$i--)
    							        <option value="{{$i}}" {{ (old('university_graduation_year') == $i) ? 'selected' : '' }} >{{$i}}</option>
    							    @endfor
							    </select>
								<h5 id="universiytgraduation"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.unv_maj')}}</h5>
								<input type="text" class="form-control" name="university_major" value="{{ old('university_major') }}">
								<h5 id="universitymajor"></h5>
							</label>
						</div>
						<!-- ============================================================= -->
						<h3 class="text-center"> {{__('message.lan_skl1')}}</h3>
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100"><h5> {{__('message.exa_min')}}</h5>
								<input type="text" class="form-control" name="examination" value="{{ old('examination') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.org_iss_ue')}}</h5>
								<input type="text" class="form-control" name="organization_of_issue" value="{{ old('organization_of_issue') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.gra_de')}}</h5>
								<input type="text" class="form-control" name="grade" value="{{ old('grade') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.sc_ore')}}</h5>
								<input type="text" class="form-control" name="score" value="{{ old('score') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.date_edu_end')}}</h5>
								<input type="text"  readonly="true" class="form-control" name="date" id="datepicker2" value="{{ old('date') }}">
							</label>
						</div>

						<h3 class="text-center">{{__('message.lan_skl')}}</h3>
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100"><h5>{{__('message.exa_min')}}</h5>
								<input type="text" class="form-control" name="examination2" value="{{ old('examination2') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.org_iss_ue')}}</h5>
								<input type="text" class="form-control" name="organization_of_issue2" value="{{ old('organization_of_issue2') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.gra_de')}}</h5>
								<input type="text" class="form-control" name="grade2" value="{{ old('grade2') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.sc_ore')}}</h5>
								<input type="text" class="form-control" name="score2" value="{{ old('score2') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.date_edu_end')}}</h5>
								<input type="text"  readonly="true" class="form-control"name="date2" id="datepicker3" value="{{ old('date2') }}">
							</label>
						</div>

						<h3 class="text-center">{{__('message.lan_skl')}}</h3>
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100"><h5>{{__('message.exa_min')}}</h5>
								<input type="text" class="form-control" name="examination3" value="{{ old('examination3') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.org_iss_ue')}}</h5>
								<input type="text" class="form-control" name="organization_of_issue3" value="{{ old('organization_of_issue') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.gra_de')}}</h5>
								<input type="text" class="form-control" name="grade3" value="{{ old('grade3') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.sc_ore')}}</h5>
								<input type="text" class="form-control" name="score3" value="{{ old('score3') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.date_edu_end')}}</h5>
								<input type="text" class="form-control"  readonly="true" name="date3" id="datepicker4" value="{{ old('date3') }}">
							</label>
						</div>			
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border text-center justify-content-center">
					<div class="block">
						<h3 class="text-center">{{__('message.par_gua_inf')}}</h3>
						<div class="block_11 d-lg-flex justify-content-between">
							<label for="" class="control-label wid100"><h5>{{__('message.nam_sur')}}</h5>
								<input type="text" class="form-control" name="parent_name" value="{{ old('parent_name') }}">
							</label>
							<label for="" class="control-label wid100"><h5> {{__('message.par_ents')}}</h5>
								<input type="text" class="form-control" name="relationship" value="{{ old('relationship') }}">
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.tel_numb_parent')}} <span style="color:red">*</span> </h5>
								<div class="form-group">
                                    <div class="input-group">
                                      <div class="input-group-addon">+998</div>
                                      <input type="text" class="form-control" id="" name="parent_phone" placeholder="901234567" value="{{ old('parent_phone') }}">
                                    </div>
                                  </div>
								{!! $errors->first('parent_phone', '<p class="p_val">:message</p>') !!}
								<h5 id="phone"></h5>
							</label>
							<label for="" class="control-label wid100"><h5>{{__('message.em_addrs')}}</h5>
								<input  type="text" class="form-control" name="parent_email" value="{{ old('parent_email') }}">
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border justify-content-center">
					<div class="block mb">
					    <p class="notice_text">{{__('message.notice_text')}}</p>
						<label for="" class="wid100" style="display: flex; justify-content: center;">
							<span class="applyFormButton ">
								<button class="text-center more_b" type="submit">{{__('message.send_app')}}</button>
							<!--	&nbsp;&nbsp;&nbsp;You can modify the application before submission.-->
							</span>

						</label>

					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Apply  -->

@endsection
@push('js')
	<script type="text/javascript">
		$('#uzbek,#russian,#korean,#exam_lang').hide();
		$('#faculty1, #faculty2, #faculty4').change(function(){
			if($(this).is(':checked')){
				$('#exam_lang,#uzbek,#russian').show();
				$('#korean').hide();
			}
		});

		$('#faculty3').change(function(){
			if($(this).is(':checked')){
				$('#exam_lang,#korean').show();
				$('#uzbek,#russian').hide();
				$("#inlineRadio3").prop("checked", true);
			}
		});
		</script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
          $( function() {
            $( "#datepicker,#datepicker2,#datepicker3,#datepicker4" ).datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, yearRange: "-80:+0" }).val();
          } );
          
          $('#photo, #diploma_copy, #passport_copy, #inn_copy').change(function(){
              $(this).prev().find('.bor_left').removeClass('fa-upload').addClass('fa-check').css('color','green');
          })
        </script>
@endpush

@if(Session::has('application_saved'))
	@push('js')
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<script type="text/javascript">
			Swal.fire(
			  '{{ Session::get('application_saved') }}',
			  '',
			  'success'
			)
		</script>
	@endpush
@endif	