@extends('layouts.app')
@section('title',__('message.archive'))
@section('content')
	<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{__('message.'.$type)}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{__('message.archive')}}-{{$year}}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- PageBanner /- -->


	<!-- NEWS Section -->


	<!-- Event List -->
	<div class="container-fluid eventlist upcoming-event latest-blog no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="section-header">
				<h3>{{__('message.archive')}}-{{$year}}</h3>
				<span>{{__('message.'.$type)}}</span>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-6 col-xs-6 content-area">
					@foreach($items as $key => $item)
						<?php 
							$item = (array) $item; 
							$created_at = new \datetime($item['created_at']);
						?>
						<article class="type-post">
							<div class="entry-cover">
								<?php $image =  ($type == 'news') ? 'news' : $type.'s' ?>
								<a href="{{url('/'.$type.'/'.$item['id'])}}"><img src="{{asset('storage/'.$image.'/'.$item['image'])}}" alt="{{$item['title_'.app()->getLocale()]}}" width="310" height="246"/></a>
							</div>
							<div class="entry-block">
								<div class="entry-title">
									<a href="{{url('/'.$type.'/'.$item['id'])}}" title="{{$item['title_'.app()->getLocale()]}}"><h3>{{$item['title_'.app()->getLocale()]}}</h3></a>
								</div>
								<div class="entry-meta">
									<div class="post-date">
										<p>{{$created_at->format('M')}}<span>{{$created_at->format('d')}}</span></p>
									</div>
									<div class="post-metablock">
										<div class="post-time">
											<span><i class="fa fa-clock"></i>{{$created_at->format('H:i')}}</span>
										</div>
										<div class="post-location">
											<span><i class="fa fa-calendar-alt"></i>{{$created_at->format('Y')}}-{{__('message.year')}}</span>
										</div>
									</div>
								</div>
								<div class="entry-content">
									{!! str_limit(strip_tags($item['content_'.app()->getLocale()]),20,'...') !!}
								</div>
								<a href="{{url('/'.$type.'/'.$item['id'])}}" class="learn-more" title="{{__('message.read_more')}}">{{__('message.read_more')}}</a>
							</div>
						</article>
					@endforeach	
					
					<!-- Ow Pagination -->
					{{-- <div class="ow-pagination">
						<nav>
							<ul class="pager">
								<li class="page-prv"><a href="#" title="Previous"><i class="fa fa-arrow-left" aria-hidden="true"></i>Previous Event</a></li>								
								<li class="page-next"><a href="#" title="Next">Next Event<i class="fa fa-arrow-right" aria-hidden="true"></i></a></li>
							</ul>
						</nav>
					</div> --}}
					<center>{{$items->links()}}</center>
					<!-- Ow Pagination /- -->
					
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6 widget-area">
					<aside class="widget widget_similar_event">
						<div class="widget-title">
							<span class="fa fa-file-archive"></span>
							<h3>{{__('message.archives')}}</h3>
						</div>						
						<ul>
							@foreach($archive as $key => $archive)
								<li><a title="{{__('message.archive')}}-{{$archive->year}}" href="{{url('archive/'.$type.'/'.$archive->year)}}"><span class="fa fa-file-archive"></span><h4>{{__('message.archive')}}-{{$archive->year}}</h4></a></li>
							@endforeach
						</ul>
					</aside>
					
				</div>
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Event List /- -->

	<!-- NEWS Section /- -->

@endsection