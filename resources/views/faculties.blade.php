@extends('layouts.app')
@section('title',__('message.faculties'))
@section('content')
	<div class="container-fluid page-banner banner_faculties about no-padding">
		<div class="section-padding"></div>
			<div class="container">
				<div class="banner-content-block">
				    {{--
					<div class="banner-content">
						<h3>{{__('message.what_are_we_professionals')}}</h3>
						<ol class="breadcrumb">
							<li><a href="{{ url('/') }}">Home</a></li>
							<li class="active">{{__('message.university_programs')}}</li>
						</ol>
					</div>
					--}}
				</div>
			</div>
		<div class="section-padding"></div>
	</div><!-- PageBanner /- -->


	<!-- PROGRAMS OF STUDY Section -->
	{{--<div class="section-header">
		<h2 style="font-size:34px;text-transform:uppercase;">{{__('message.programs_of_study')}}</h2>
	   <span>Our Schedule table</span>
	</div>--}}
	<div class="container-fluid no-padding introduction-section">
		<div class="introduction-carousel">
			@foreach($programs as $program)
				<div class="col-md-4 no-padding">
					<div class="introduction-block">
						<a href="{{url('/program/'.$program->id)}}"><center><img src="{{asset('storage/programs/'.$program['image'])}}">
						<h3 style="text-align: center" class="block-title">{{$program['title_'.app()->getLocale()]}}</h3></center>

						<!--<p>{!! str_limit(strip_tags($program['description_'.app()->getLocale()]),35,'...')!!}</p>-->
						<!--<a href="#" title="Learn more">Learn more</a>--></a>

					</div>
				</div>
			@endforeach
		</div>
	</div><!-- PROGRAMS OF STUDY Section /- -->

@endsection