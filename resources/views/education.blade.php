@extends('layouts.app')
@section('title',$education['title_'.app()->getLocale()])
@section('content')
<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{$education['title_'.app()->getLocale()]}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{ url('/') }}">Home</a></li>
					<li class="active">{{ $education['title_'.app()->getLocale()] }}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Page-->
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="content_block">
								<h3 class="h3-b50">{{ $education['title_'.app()->getLocale()] }}</h3>
								{!! $education['content_'.app()->getLocale()] !!}
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
@endsection	