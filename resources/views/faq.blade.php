@extends('layouts.app')
@section('title',__('message.menu_faq'))
@section('content')
	<!-- F.A.Q. -->
	<div class="container-fluid faq_ no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="section-header">
				<h3>{{__('message.menu_faq')}}</h3>
            	<span>{{__('message.frequently_asked_questions')}}</span>
			</div>

			<div class="panel-group" id="faqAccordion">
				@foreach($faq as $key => $faq)
	                <div class="panel panel-default ">
	                    <div class="panel-heading accordion-toggle question-toggle" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question{{$key}}" aria-expanded="true">
	                        <h4 class="panel-title">
	                            {{ strip_tags($faq['question_'.app()->getLocale()]) }}
	                        </h4>
	                    </div>
	                    <div id="question{{$key}}" class="panel-collapse collapse" style="height: 0px;">
	                        <div class="panel-body">
	                            <h5><span class="label label-primary">{{__('message.answer')}}</span></h5>

	                            {!! $faq['answer_'.app()->getLocale()] !!}
	                        </div>
	                    </div>
	                </div>
	            @endforeach  
			</div>

			<!--/panel-group-->
		</div>
		<div class="section-padding"></div>
	</div>
	<!-- F.A.Q. /- -->
@endsection