@extends('layouts.login')
{{-- {{Hash::make('yengiparolqoying')}} --}}
@section('content')
<form id="login-form" method="POST" action="{{ route('login') }}" novalidate="">
    @csrf


    <div class="form-group">
        <label for="phone">{{ __('Login') }}</label>
        <input id="phone" type="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} underlined" name="phone" value="{{ old('phone') }}" required autofocus>

        @if ($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="password">{{ __('Password') }}</label>
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} underlined" name="password" required>

        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="remember">
            <input class="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <span>
                {{ __('Remember Me') }}
            </span>
        </label>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">
            {{ __('Login') }}
        </button>

        @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
    </div>
</form>
@endsection
