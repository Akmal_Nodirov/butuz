@extends('layouts.student.app')

@section('content')
	
	<div class="container">
		<h3>Change password</h3>
		@if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
		<form method="post" action="{{url('student/update_password')}}">
		  {{csrf_field()}}
          {{ method_field('PUT') }}
		  {{-- <div class="form-group row">
		    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control-plaintext" id="phone" value="{{Auth::user()->phone}}">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="oldPassword" class="col-sm-2 col-form-label">Old password</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="oldPassword" placeholder="Password" name="old_password">
		    </div>
		  </div> --}}
		  <div class="form-group row">
		    <label for="newPassword" class="col-sm-2 col-form-label">New Password</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="newPassword" placeholder="Password" name="password">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="confirmPassword" class="col-sm-2 col-form-label">New password once again</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="confirmPassword" placeholder="Password" name="password_confirmation">
		    </div>
		  </div>
		    <div class="col-sm-10 offset-sm-2">
		  		<input type="submit" name="submit" value="Save" class="btn btn-info">
		  	</div>	
		</form>	
	</div>

@endsection