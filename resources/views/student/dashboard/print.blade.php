@extends('layouts.app')
@push('css')
    <style type="text/css">
        .bg_print{
            display: none;
        }
        .bg_print2{
            display: none;
        }
.min_mar_pr{
                    margin-top: -25px !important;
            }
        @media print{
            .min_mar_pr{
                    margin-top: -25px !important;
            }
            .col-md-8{
                width: 77.66666667%;
            }
            .fixed_link{
                display: none !important;
            }
            .print_but{
                display:none !important;
            }
            header, footer{
                display:none;
            }
            .print-width{
                width: 300px;
                margin: 5px;
            }
            .col-md-5{
                width: 41.66666667%;
            }
            .row{
                display: flex;
            }
            .bg_print{
                position: absolute;
                top: -15px !important;
                left: 240px;
                opacity:;
                z-index: -1;
                display: flex;
            }
            .bg_print2{
                position: absolute;
                top: 600px;
                width: 200px;
                left: 250px;
                opacity:;
                z-index: -1;
                display: flex;
            }

            .padding_none {
            padding: 0;
            }

            @page {

                margin-left: 5px;
                margin-right: 5px;
                margin-top: 35px;
                margin-bottom: 5px;
                -webkit-print-color-adjust: exact;
            }
        }

        /*-------------------- Anketa Block -----------------------*/

        .anketa_block{
            width: 100%;
            height: auto;
            background-color: #fff;
            margin: 0 auto;
        }
        .ank_in_block{
            display: block;

        }
        .i_tem{
            display: flex;
            /*justify-content: space-between;*/
            border-bottom: 1px solid #fdbc0f;
            margin: 5px;
        }
        .bold_ank{
            font-weight: bold;
            font-size: 12px;
            width: 120px !important;
            min-width: 120px;
            border-right: 1px solid #000000;
            margin: 0 !important;
            padding-right: 6px;

        }
        .float-right{
            float: right;
            padding: 5px;
			margin: 0 !important;
        }
        .float-left{
            float: left;
        }
        .font_10{
            font-size: 12px;
        }
        .margin_15_lr{
            margin: 10px 5px;
        }

        .margin_15{
            margin:15px;
        }
        .margin_4{
            margin: 4px;
        }
        .img_block_3x4{
            width: 3cm;
            height: 4cm;
            border: 1px solid #333333;
            margin: 0 auto;
        }
        .bt{
            border-bottom: 1px solid #333;
        }
        .first{
            background-image: url({{asset('but/welcomergba.png')}});
            background-repeat: no-repeat;
            background-position: center;
            -webkit-background-size: 200px;
            background-size: 200px;
            position: relative;
        }
        .second{
            background-image: url({{asset('but/welcome.png')}});
            background-repeat: no-repeat;
            background-position: center;
            -webkit-background-size: 100px;
            background-size: 166px;
        }
        .img_bucheon img{
            width: 30px;
        }
        .bb{
            border-bottom: 2px solid #333;
            border-style: dashed;
            border-left: none;
            border-right: none;
            border-top: none;
            padding-bottom: 10px;
        }
        .p5{
            padding: 10px;
        }30
        .m5{
            margin-top: 8px !important;
        }
        .p_t5{
            padding-top: 5px;
        }
        .w30p{
            width: 30px;
            float: left;
        }
        .bitta_bloc{
            width: 402px;
            margin: 10px auto;
        }
        .pb5{
            padding-bottom: 5px;
        }



        .padding_none {
            padding: 0;
        }

    </style>
@endpush
@section('content')
    <section style="background: #f1f1f1;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="anketa_block">
                    <div class="ank_in_block">
                        <div class="first margin_4 bb">
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="bitta_bloc p_t5">
                                      <img src="{{asset('but/welcome.png')}}" class="float-left w30p m5" alt="">
                                      <h4 class="float-left p5">&nbsp;Университет Пучон в городе Ташкенте</h4>
                                      <button class="more_b print_but" id="print_button"> {{__('message.print')}} <i style="font-size:18px;" class="fa fa-print"></i></button>
                                  </div>
                              </div>
                          </div>
                          
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="d-lf-flex print-width margin_4">
                                        <div class="img_block_3x4">

                                        </div>
                                            <p class="text-center p_t5">{{__('message.registr_number')}}</p>
                                        <p class="text-center pb5">{{ $application->apply_no }}</p>
                                        <img src="{{asset('but/welcomergba.png')}}" class="bg_print"  alt="">
                                        <img src="{{asset('but/welcomergba.png')}}" class="bg_print2"  alt="">
                                        <div class="i_tem font_10">
                                            <p class="bold_ank">{{__('message.famil_y')}}</p>
                                            <p class="float-right">{{ $application->lastname }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank"> {{__('message.first_name')}}</p>
                                            <p class="float-right">{{ $application->firstname }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank">{{__('message.date_birth')}}</p>
                                            <p class="float-right">{{ $application->date_of_birth }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank"> {{__('message.nationalit_y')}}</p>
                                            <p class="float-right">{{ $application->nationality }}</p>
                                        </div>
                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10"> {{__('message.pass_serial_number')}}</p>
                                            <p class="float-right"> {{ $application->passport }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10"> {{__('message.gende_r')}}</p>
                                            <p class="float-right"> {{ ($application->gender) ? 'M' : 'F' }}</p>
                                        </div>
                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.tel_numb')}}</p>
                                            <p class="float-right"> {{ $application->phone_number }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="d-lf-flex margin_4">

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.el_addr')}}</p>
                                            <p class="float-right"> {{ $application->email }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.lan_group')}}</p>
                                            <p class="float-right"> 
                                                @if($application->examination_language == 1)
                                                    Uzbek
                                                @elseif($application->examination_language == 2)
                                                    Russian
                                                @else
                                                    Korean
                                                @endif
                                            </p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.addr_es')}}</p>
                                            <p class="float-right">{{ $application->address }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.fac_name')}}</p>
                                            <p class="float-right">{{ $application->faculty->name }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.name_comp')}}</p>
                                            <p class="float-right">{{ $application->faculty->name }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.info_edu')}}</p>
                                            <p class="float-right">{{$application->high_school_name}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.expir_date')}}</p>
                                            <p class="float-right">{{$application->high_school_graduation_year}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.special_ity')}}</p>
                                            <p class="float-right"> {{$application->high_school_major}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.fam_nam_parent')}}</p>
                                            <p class="float-right"> {{$application->parent_name}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10"> {{__('message.par_ents')}}</p>
                                            <p class="float-right"> {{$application->relationship}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10"> {{__('message.tel_numb_parent')}}</p>
                                            <p class="float-right"> {{$application->parent_phone_number}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.em_addrs')}}</p>
                                            <p class="float-right"> {{$application->parent_email}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="second margin_4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="bitta_bloc m5">
                                        <img src="{{asset('but/welcome.png')}}" class="float-left w30p m5" alt="">
                                        <h4 class="float-left p5">&nbsp;Университет Пучон в городе Ташкенте</h4>
                                    </div>

                                </div>
                            </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-4 ">
                                    <div class="d-lf-flex margin_15_lr">

                                        <div class="i_tem font_10">
                                            <p class="bold_ank">{{__('message.famil_y')}}</p>
                                            <p class="float-right"> {{$application->lastname}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank">{{__('message.first_name')}}</p>
                                            <p class="float-right"> {{$application->firstname}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank">{{__('message.date_birth')}}</p>
                                            <p class="float-right"> {{$application->date_of_birth}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank"> {{__('message.nationalit_y')}}</p>
                                            <p class="float-right"> {{$application->nationality}}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.pass_serial_number')}}</p>
                                            <p class="float-right"> {{$application->passport}}</p>
                                        </div>

                                      
                                        

                                    </div>
                                </div>
                                <div class="col-md-4 padding_none">
                                    <div class="d-lf-flex margin_15_lr ">

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.gende_r')}}</p>
                                            <p class="float-right"> {{ ($application->gender) ? 'M' : 'F' }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.tel_numb')}}</p>
                                            <p class="float-right">{{ $application->phone_number }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.el_addr')}}</p>
                                            <p class="float-right"> {{ $application->email }}</p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.lan_group')}}</p>
                                            <p class="float-right"> 
                                                @if($application->examination_language == 1)
                                                    Uzbek
                                                @elseif($application->examination_language == 2)
                                                    Russian
                                                @else
                                                    Korean
                                                @endif
                                            </p>
                                        </div>

                                        <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.fac_name')}}</p>
                                            <p class="float-right">{{$application->faculty->name}}</p>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="img_block_3x4 min_mar_pr">

                                    </div>
                                    <p class="text-center p_t5">{{__('message.registr_number')}}</p>
                                    <p class="text-center pb5">{{$application->apply_no}}</p>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-8 clmdsak">
                                      <div class="i_tem font_10">
                                            <p class="bold_ank font_10">{{__('message.addr_es')}}</p>
                                            <p class="float-right">{{$application->address}}</p>
                                        </div>
                                        <hr style="margin-bottom: 0px; border-top: 2px solid #000;">
                                    <div class="i_tem font_10 log_pass_stil">
                                            <p class="bold_ank font_10">{!!__('message.vash_login')!!}</p>
                                            <p class="float-right">{{ $application->phone_number }}</p>
                                        </div>
                                        <div class="i_tem font_10 log_pass_stil">
                                            <p class="bold_ank font_10">{!!__('message.vash_parol')!!}</p>
                                            <p class="float-right znach_stil">{{$application->passport}}</p>
                                        </div>
                                       
                                    
                                    <div class=" font_10 ">
                                            <p class="">{!!__('message.login_url')!!}</p>
                                            
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('js')
    <script>
        window.print();
        $('#print_button').click(function(){
            window.print();
        })
    </script>
@endpush