@extends('layouts.student.app')
@push('css')
    <style type="text/css">
    .th_center{
        text-align:center;
    }
    
    .data_ {
    font-size: 20px;
    font-weight: bold;
    margin: 130px 30px 30px;
    }
    
    .only_print {
            width: 565px;
        margin: 120px  auto 10px;
        display: table;
    }
    
    .logo_bucheon {
    width: 100px;
    float: left;
    margin-right: 20px;
    vertical-align: middle;
    display: table-cell;
}

.logo_bucheon_t {
    z-index: 0;
    position: absolute;
    width: 60%;
    left:23%;
}

        @media print{
            .table thead tr th{
                width:30px;
                overflow:hidden;
                font-size:15px;
                padding: 5px 10px;
            }
            .table tbody tr td{
                width:30px;
                overflow:hidden;
                font-size:15px;
                padding: 5px 10px;
               
            }
            
            .subject_print {
                min-width: 400px;
            }

            .header .header-block-nav > ul > li > a {
                display: none;
            }

            form.form-inline {
                display: none;
            }

            .fa {
                display: none;
            }
            
        }

        .logo_bucheon{
                width:100px;
            }
        .table thead tr th,.table tbody tr td {
                border: 1px solid #000 !important;
        }
    </style>
@endpush
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr>
                    Student: {{$student->firstname.' '.$student->lastname}}<br>
                    Student. no: {{$student->student_id}}<br>
                    Course: {{$student->group->course}}<br>
                @if(isset($_GET['semester']))
                    Semester: 2018/2019 1st semester<br>
                @endif  
                    Faculty: {{$student->faculty->name}}<br>
                    Group: {{$student->group->name}}<br>
                    <hr>
               <img class="logo_bucheon_t" src="{{asset('temporary/but_transparent.png')}}">     
            <form class="form-inline" method="get">
              <div class="form-group mb-2">
                <label for="course">Course: </label> 
                <select type="text" class="form-control" id="course" name="course" placeholder="Choose course...">
                    <option>Choose course...</option>
                    @for($i=1;$i<=3;$i++)
                        <option value="{{$i}}" {{ (isset($_GET['course']) && $_GET['course'] == $i) ? 'selected' : '' }}>{{$i}}</option>
                    @endfor
                </select>
              </div>
              <div class="form-group mx-sm-3 mb-2">
                <label for="semester">Semester: </label> 
                <select type="text" class="form-control" id="semester" placeholder="Choose semester..." name="semester">
                    <option>Choose semester...</option>
                    @for($i=1;$i<=2;$i++)
                        <option value="{{$i}}" {{ (isset($_GET['semester']) && $_GET['semester'] == $i) ? 'selected' : '' }}>{{$i}}</option>
                    @endfor                    
                </select>    
              </div>
              <button type="submit" class="btn btn-primary mb-2">Show results</button>
            </form>


            @if((isset($_GET['course']) && is_numeric($_GET['course'])) && (isset($_GET['semester']) && is_numeric($_GET['semester'])))
                <hr>
                <div style="overflow:auto">    
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                {{-- <th>Group</th> --}}
                                <th>Subject</th>
                                {{-- <th>Professor</th>
                                <th>Late</th>
                                <th>Absent</th>
                                <th>Present</th>
                                <th>Homework</th>
                                <th>Middle exam</th>
                                <th>Final exam</th>
                                <th>Assignment</th> --}}
                                <th>Total</th>
                                <th>Mark</th>
                                {{-- <th>Rate</th>
                                <th>Credit * Rate</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <?php $totals = 0; $credits=0; ?>
                            @foreach($semester as $key => $semester)
                                <?php 
                                    $totals += ($semester->total*$semester->subject->credit); 
                                    $credits += $semester->subject->credit;
                                ?>
                                <tr>
                                    <td>{{++$key}}</td>
                                    {{-- <th>{{$semester->group->name.' ('.$semester->group->faculty->name.')'}}</th> --}}
                                    <td>{{$semester->subject->subject}}</td>
                                    {{-- <td>{{$semester->professor->firstname.' '.$semester->professor->lastname}}</td>
                                    <td>{{$semester->late_count}}</td>
                                    <td>{{$semester->absent_count}}</td>
                                    <td>{{$semester->present_count}}</td>
                                    <td>{{$semester->homework}}</td>
                                    <td>{{$semester->middle_exam}}</td>
                                    <td>{{$semester->final_exam}}</td>
                                    <td>{{$semester->bonus}}</td> --}}
                                    <td>{{$semester->total}}</td>
                                    <td>{{$semester->mark}}</td>
                                    {{-- <td>{{$semester->rate}}</td>
                                    <td>{{$semester->credit_x_rate}}</td> --}}
                                </tr>
                            @endforeach
                            <tr style="font-weight:bold">
                                <td></td>
                                {{-- <td></td> --}}
                                {{-- <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td> --}}
                                <td>Total</td>
                                <td>{{ $totals }}</td>
                                <td></td>
                                {{-- <td></td>
                                <td></td> --}}
                            </tr>
                            <tr style="font-weight:bold">
                                <td></td>
                                {{-- <td></td> --}}
                                {{-- <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td> --}}
                                <td>Average score</td>
                                <td>{{$average = $credits != 0 ? round($totals/$credits) : 0}}</td>
                                <td>
                                    @if($average>=95 && $average<=100)
                                        A+
                                    @elseif($average>=90 && $average<=94)
                                        A
                                    @elseif($average>=85 && $average<=89)
                                        B+
                                    @elseif($average>=80 && $average<=84)
                                        B
                                    @elseif($average>=75 && $average<=79)
                                        C+
                                    @elseif($average>=70 && $average<=74)
                                        C
                                    @elseif($average>=65 && $average<=69)
                                        D+
                                    @elseif($average>=60 && $average<=64)
                                        D
                                    @elseif($average>0 && $average<=59)
                                        F   
                                    @else
                                        
                                    @endif
                                </td>
                                {{-- <td>
                                    @if($average>=95 && $average<=100)
                                        4.5
                                    @elseif($average>=90 && $average<=94)
                                        4.0
                                    @elseif($average>=85 && $average<=89)
                                        3.5
                                    @elseif($average>=80 && $average<=84)
                                        3.0
                                    @elseif($average>=75 && $average<=79)
                                        2.5
                                    @elseif($average>=70 && $average<=74)
                                        2.0
                                    @elseif($average>=65 && $average<=69)
                                        1.5
                                    @elseif($average>=60 && $average<=64)
                                        1.0
                                    @elseif($average>0 && $average<=59)
                                        0.0 
                                    @else
                                        0.0
                                    @endif
                                </td>
                                <td></td> --}}
                            </tr>
                            <tr style="font-weight:bold">
                                    <td></td>
                                    {{-- <td></td> --}}
                                    {{-- <td></td> --}}
                                    {{-- <td></td>
                                    <td></td> --}}
                                    {{-- <td></td>
                                    <td></td>
                                    <td></td> --}}
                                    <td>Average rate</td>
                                    {{-- <td class="th_center">
                                        {{ $average = $credits != 0 ? round($totals/$credits) : 0 }}
                                    </td> --}}
                                    <td class="th_center">
                                        @if($average>=95 && $average<=100)
                                            4.5
                                        @elseif($average>=90 && $average<=94)
                                            4.0
                                        @elseif($average>=85 && $average<=89)
                                            3.5
                                        @elseif($average>=80 && $average<=84)
                                            3.0
                                        @elseif($average>=75 && $average<=79)
                                            2.5
                                        @elseif($average>=70 && $average<=74)
                                            2.0
                                        @elseif($average>=65 && $average<=69)
                                            1.5
                                        @elseif($average>=60 && $average<=64)
                                            1.0
                                        @elseif($average>0 && $average<=59)
                                            0.0 
                                        @else
                                            0.0
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
         <div style="clear:both"></div>
            <div class="only_print">
                <img class="logo_bucheon" src="{{asset('temporary/logo.png')}}">
                <h3 style="font-weight: bold; display: table-cell; vertical-align: middle;">Bucheon University in Tashkent</h3>
            </div>

        </div>
        <p class="data_">Date of issue:&nbsp;{{ date('d.m.Y') }}</p> 
    </div>
</div>
@endsection
