@extends('layouts.app')
@section('title',__('message.menu_apply'))
@push('css')
    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
@endpush
@section('content')
	<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
				<div class="banner-content">
					<h3>{{__('message.menu_apply')}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{__('message.menu_apply')}}</li>
					</ol>
				</div>
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- PageBanner /- -->
	<!-- Apply  -->

	<div class="container">
		<div class="row">
			<div class="col-lg-12 border">
				<h1 class="text-center">Application form for admission</h1>
				<form action="{{url('/update_application/'.$application->id)}}" name="test" method="post" enctype="multipart/form-data">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<div class="radio_form">
						<h3 class="text-center">Faculty</h3>
						<h5 class="text-center">Faculty of apply</h5>
						<h4 style="text-align:center; color:red">Choose name of faculty and language of study group one more time</h4>  
        				{!! $errors->first('faculty_id', '<p class="p_val">:message</p>') !!}<br>
        				{!! $errors->first('exam_lang', '<p class="p_val">:message</p>') !!}
						<div class="radio_items">
							<div class="d-lg-flex justify-content-between">
								@foreach($faculties as $key => $faculty)
									<div class="item_1">
										<input type="radio" name="faculty_id"  id="faculty{{$faculty->id}}" value="{{$faculty->id}}">
										<span class="radio_text">
											<p>{{$faculty['name']}}</p>
										</span>
									</div>
								@endforeach
							</div>
						</div>
					</div>
					
					<div class="radio_form">
						<h5 class="text-center" id="exam_lang">Язык обучения группы</h5>
						<div class="radio_items">
							<div class="d-lg-flex justify-content-between">
								<div class="item_1" id="uzbek">
									<input type="radio" name="exam_lang" id="inlineRadio1" value="1">
									<span class="radio_text">
										<p>Uzbek</p>
									</span>
								</div>
								<div class="item_2" id="russian">
									<input type="radio" name="exam_lang" id="inlineRadio2" value="2">
									<span class="radio_text">
										<p>Russian</p>
									</span>
								</div>
								<div class="item_3" id="korean">
									<input type="radio" name="exam_lang" id="inlineRadio3" value="3">
									<span class="radio_text">
										<p>Korean</p>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border text-center justify-content-center">
					<h1 class="text-center">Personal Information</h1>
					<div class="block">

						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>Family name <span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="lastname" value="{{$application->lastname}}">
								{!! $errors->first('lastname', '<p class="p_val">:message</p>') !!}
								<h5 id="family"></h5>
							</label>
							<label for="" class="control-label"><h5>First Name <span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="firstname" value="{{$application->firstname}}">
								{!! $errors->first('firstname', '<p class="p_val">:message</p>') !!}
								<h5 id="firstName"></h5>
							</label>
							<label for="" class="control-label"><h5>Date of birth(yyyy-mm-dd) <span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="date_of_birth" id="datepicker" value="{{$application->date_of_birth}}">
								{!! $errors->first('date_of_birth', '<p class="p_val">:message</p>') !!}
								<h5 id="date"></h5>
							</label>
							<label for="" class="control-label"><h5>Nationality <span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="nationality" value="{{$application->nationality}}">
								{!! $errors->first('nationality', '<p class="p_val">:message</p>') !!}
								<h5 id="nationality"></h5>
							</label>
						</div>

						<div class="block">
							<div class="block_1 d-lg-flex justify-content-between">
								<label for="" class="control-label"><h5>Passport Number <span style="color:red">*</span> </h5>
									<input type="text" class="form-control" name="passport_number" value="{{$application->passport}}">
									{!! $errors->first('passport_number', '<p class="p_val">:message</p>') !!}
									<h5 id="passport"></h5>
								</label>
								<label for="" class="control-label"><h5>Gender <span style="color:red">*</span> </h5>
									<select type="text" class="form-control" name="gender">
										<option value="1" {{$application->gender == '1' ? 'checked' : ''}}>Мужчина</option>
										<option value="0" {{$application->gender == '0' ? 'checked' : ''}}>Женщина</option>
									</select>
									{!! $errors->first('gender', '<p class="p_val">:message</p>') !!}
									<h5 id="gender"></h5>
								</label>
								<label for="" class="control-label"><h5>E-mail <span style="color:red">*</span> </h5>
									<input type="text" class="form-control" name="email" value="{{$application->email}}">
									{!! $errors->first('email', '<p class="p_val">:message</p>') !!}
									<h5 id="email"></h5>
								</label>
								<label for="" class="control-label"><h5>Phone Number <span style="color:red">*</span> </h5>
									<div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="phone" name="phone_number" placeholder="901234567" value="{{$application->phone_number}}">
                                        </div>
                                      </div>
									{!! $errors->first('phone_number', '<p class="p_val">:message</p>') !!}
									<h5 id="phone"></h5>
								</label>
							</div>
							<div class="block">
								<div class="block_1 d-lg-flex justify-content-between">
									<label for="" class="control-label"><h5>Address <span style="color:red">*</span> </h5>
										<textarea class="form-control" name="address" id="" cols="50" rows="5">{{$application->address}}</textarea>
										{!! $errors->first('address', '<p class="p_val">:message</p>') !!}
									</label>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<section>
		    <div class="container m_auto border">
		    <div class="row">
		        <div class="col-md-12">
		            <h1 class="text-center">Upload the required documents in scanned form.</h1>
		        </div>
		    </div>
		    <div class="row">
		        <div class="col-md-12">
		            <div class="block">
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="photo" class="control-label"><span  class="butt_s w100" title="Подробнее">Photo  <i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" name="photo" id="photo" style="display:none">
							</label>
						
							<label for="diploma_copy" class="control-label"><span  class="butt_s w100" title="Подробнее"> Copy of diploma <i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" name="diploma_copy" id="diploma_copy" style="display:none">
							</label>
						
							<label for="passport_copy" class="control-label"><span  class="butt_s w100" title="Подробнее"> Copy of passport <i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" name="passport_copy" id="passport_copy" style="display:none">
							</label>
					
							<label for="inn_copy" class="control-label"><span  class="butt_s w100" title="Подробнее">INN copy <i class="fa fa-upload bor_left"></i></span>
								<input class="form-control" cols="50" rows="5" type="file" id="inn_copy" name="inn_copy" style="display:none">
							</label>
						</div>
					</div>
		        </div>
		    </div>
		</div>
		</section>
		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border text-center justify-content-center">
					<h1 class="text-center">Educational Background</h1>
					<h5 class="text-center">High school information </h5>
					<div class="block">
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>High school (college, lyceum etc.) <span style="color:red">*</span> </h5>
								<div class="errorText"></div>
								<input id="highschool" type="text" name="highschool" class="form-control" value="{{$application->high_school_name}}">
								{!! $errors->first('highschool', '<p class="p_val">:message</p>') !!}
								<h5 id="highschool"></h5>
							</label>
							<label for="" class="control-label"><h5>Graduation year <span style="color:red">*</span>  </h5>
								<select class="form-control" name="grad_year">
								    <option></option>
								    @for($i=2019;$i>=1980;$i--)
								        <option value="{{$i}}" {{(isset($application->high_school_graduation_year) && $application->high_school_graduation_year == $i) ? 'selected' : ''}}>{{$i}}</option>
								    @endfor
								</select>
								{!! $errors->first('grad_year', '<p class="p_val">:message</p>') !!}
								<h5 id="graduation"></h5>
							</label>
							<label for="" class="control-label"><h5>Major <span style="color:red">*</span> </h5>
								<input type="text" class="form-control" name="major"  value="{{$application->high_school_major}}">
								{!! $errors->first('major', '<p class="p_val">:message</p>') !!}
								<h5 id="major"></h5>
							</label>
						</div>

						<h3 class="text-center">University information</h3>
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>University Name</h5>
								<input type="text" class="form-control" name="university_name" value="{{$application->university_name}}">
								<h5 id="universityname"></h5>
							</label>
			
							<label for="" class="control-label"><h5>University Graduation Year</h5>
								<select class="form-control" name="university_graduation_year">
								    <option></option>
								    @for($i=2019;$i>=1980;$i--)
								        <option value="{{$i}}" {{(isset($application->university_graduation_year) && $application->university_graduation_year == $i) ? 'selected' : ''}}>{{$i}}</option>
								    @endfor
								</select>
								<h5 id="universiytgraduation"></h5>
							</label>
							<label for="" class="control-label"><h5>University Major</h5>
								<input type="text" class="form-control" name="university_major" value="{{$application->university_major}}">
								<h5 id="universitymajor"></h5>
							</label>
						</div>
						<!-- ============================================================= -->
						<h3 class="text-center">Language skills 1</h3>
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>Examination</h5>
								<input type="text" class="form-control" name="examination" value="{{$application->examination1}}">
							</label>
							<label for="" class="control-label"><h5>Organization of issue</h5>
								<input type="text" class="form-control" name="organization_of_issue" value="{{$application->organization_of_issue1}}">
							</label>
							<label for="" class="control-label"><h5>Grade</h5>
								<input type="text" class="form-control" name="grade" value="{{$application->grade1}}">
							</label>
							<label for="" class="control-label"><h5>Score</h5>
								<input type="text" class="form-control" name="score" value="{{$application->score1}}">
							</label>
							<label for="" class="control-label"><h5>Date (yyyy-mm-dd)</h5>
								<input type="text" class="form-control" name="date" id="datepicker2" value="{{$application->date1}}">
							</label>
						</div>

						<h3 class="text-center">Language skills 2</h3>
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>Examination</h5>
								<input type="text" class="form-control" name="examination2" value="{{$application->examination2}}">
							</label>
							<label for="" class="control-label"><h5>Organization of issue</h5>
								<input type="text" class="form-control" name="organization_of_issue2" value="{{$application->organization_of_issue2}}">
							</label>
							<label for="" class="control-label"><h5>Grade</h5>
								<input type="text" class="form-control" name="grade2" value="{{$application->grade2}}">
							</label>
							<label for="" class="control-label"><h5>Score</h5>
								<input type="text" class="form-control" name="score2" value="{{$application->score2}}">
							</label>
							<label for="" class="control-label"><h5>Date (yyyy-mm-dd)</h5>
								<input type="text" class="form-control"name="date2" id="datepicker3" value="{{$application->date2}}">
							</label>
						</div>

						<h3 class="text-center">Language skills 3</h3>
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>Examination</h5>
								<input type="text" class="form-control" name="examination3" value="{{$application->examination3}}">
							</label>
							<label for="" class="control-label"><h5>Organization of issue</h5>
								<input type="text" class="form-control" name="organization_of_issue3" value="{{$application->organization_of_issue3}}">
							</label>
							<label for="" class="control-label"><h5>Grade</h5>
								<input type="text" class="form-control" name="grade3" value="{{$application->grade3}}">
							</label>
							<label for="" class="control-label"><h5>Score</h5>
								<input type="text" class="form-control" name="score3" value="{{$application->score3}}">
							</label>
							<label for="" class="control-label"><h5>Date (yyyy-mm-dd)</h5>
								<input type="text" class="form-control"name="date3" id="datepicker4" value="{{$application->date3}}">
							</label>
						</div>			
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border text-center justify-content-center">
					<div class="block">
						<h3 class="text-center">Parent/Guardian information</h3>
						<div class="block_1 d-lg-flex justify-content-between">
							<label for="" class="control-label"><h5>Name/Surname</h5>
								<input type="text" class="form-control" name="parent_name" value="{{$application->parent_name}}">
							</label>
							<label for="" class="control-label"><h5>Relationship</h5>
								<input type="text" class="form-control" name="relationship" value="{{$application->relationship}}">
							</label>
							<label for="" class="control-label"><h5>Phone Number <span style="color:red">*</span> </h5>
								<div class="form-group">
                                    <div class="input-group">
                                      <input type="text" class="form-control" id="" name="parent_phone" placeholder="901234567" value="{{$application->parent_phone_number}}">
                                    </div>
                                  </div>
								{!! $errors->first('parent_phone', '<p class="p_val">:message</p>') !!}
								<h5 id="phone"></h5>
							</label>
							<label for="" class="control-label"><h5>E-mail</h5>
								<input  type="text" class="form-control" name="parent_email" value="{{$application->parent_email}}">
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row mb">
				<div class="col-lg-12 border justify-content-center">
					<div class="block mb">
						<h3 class="text-center">Personal information collection/use</h3>

						<label for="">
							<span class="applyFormButton ">
								<button class="text-center more_b" type="submit">Save</button>
								&nbsp;&nbsp;&nbsp;You can modify the application before submission.
							</span>

						</label>

					</div>
				</div>
			</div>
		</div>
	</form>
	<!-- Apply  -->

@endsection
@push('js')
	<script type="text/javascript">
		$('#uzbek,#russian,#korean,#exam_lang').hide();
		$('#faculty1, #faculty2, #faculty4').change(function(){
			if($(this).is(':checked')){
				$('#exam_lang,#uzbek,#russian').show();
				$('#korean').hide();
			}
		});

		$('#faculty3').change(function(){
			if($(this).is(':checked')){
				$('#exam_lang,#korean').show();
				$('#uzbek,#russian').hide();
				$("#inlineRadio3").prop("checked", true);
			}
		});
		</script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
          $( function() {
            $( "#datepicker,#datepicker2,#datepicker3,#datepicker4" ).datepicker({ dateFormat: 'yy-mm-dd', changeMonth: true, changeYear: true, yearRange: "-80:+0" }).val();
          } );
          
          $('#photo, #diploma_copy, #passport_copy, #inn_copy').change(function(){
              $(this).prev().find('.bor_left').removeClass('fa-upload').addClass('fa-check').css('color','green');
          })
        </script>
@endpush

@if(Session::has('application_saved'))
	@push('js')
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<script type="text/javascript">
			Swal.fire(
			  '{{ Session::get('application_saved') }}',
			  '',
			  'success'
			)
		</script>
	@endpush
@endif	