@extends('layouts.student.app')

@push('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 align="center">Hello, you are logged in as  <span style="color:red"> {{ $student->firstname.' '.$student->lastname }}</span></h4>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                               {{--  <tr>
                                    <th>ID</th><td>{{ $student->id }}</td>
                                </tr> --}}
                                <tr>
                                    <th> Photo </th>
                                    <td> 
                                        <img src="{!! $student->photo ? url('storage/photos/'.$student->photo) : url('storage/photos/no-photo.png') !!}" class="img-fluid img-thumbnail" style="height: 200px; width:200px; object-fit: cover;"> 
                                    </td>
                                </tr>
                                <tr>
                                    <th> Firstname </th>
                                    <td> {{ $student->firstname }} </td>
                                </tr>
                                <tr>
                                    <th> Lastname </th>
                                    <td> {{ $student->lastname }} </td>
                                </tr>
                                <tr>
                                    <th> Apply no. </th>
                                    <td> {{ $student->apply_no }} </td>
                                </tr>
                                <tr>
                                    <th> Student ID </th>
                                    <td> {{ $student->student_id }} </td>
                                </tr>
                                <tr>
                                    <th> Gender </th>
                                    <td> {{ $student->gender ? 'Male' : 'Female' }} </td>
                                </tr>
                                <tr>
                                    <th> Date of birth </th>
                                    <td> {{ $student->date_of_birth }} </td>
                                </tr>
                                <tr>
                                    <th> Status </th>
                                    <td> {{ $student->status ? 'Student' : '' }} </td>
                                </tr>
                                <tr>
                                    <th> Faculty </th>
                                    <td> {{ isset($student->faculty->name) ? $student->faculty->name : '' }} </td>
                                </tr>
                                <tr>
                                    <th> Course </th>
                                    <td> {{ isset($student->course->name) ? $student->group->course : '' }} </td>
                                </tr>
                                <tr>
                                    <th> Group </th>
                                    <td> {{ isset($student->group->name) ? $student->group->name : '' }} </td>
                                </tr>
                                <tr>
                                    <th> Examination language </th>
                                    <td> {{ $student->examination_language }} </td>
                                </tr>
                                <tr>
                                    <th> Application status </th>
                                    <td> {{ $student->changed ? 'Changed' : 'Not changed' }} </td>
                                </tr>
                                <tr>
                                    <th> Nationality </th>
                                    <td> {{ $student->nationality }} </td>
                                </tr>
                                <tr>
                                    <th> Passport </th>
                                    <td> {{ $student->passport }} </td>
                                </tr>
                                <tr>
                                    <th> Phone number </th>
                                    <td> {{ $student->phone_number }} </td>
                                </tr>
                                <tr>
                                    <th> Email </th>
                                    <td> {{ $student->email }} </td>
                                </tr>
                                <tr>
                                    <th> Address </th>
                                    <td> {{ $student->address }} </td>
                                </tr>
                                <tr>
                                    <th> Work place </th>
                                    <td> {{ $student->work_place }} </td>
                                </tr>
                                <tr>
                                    <th> Work phone number </th>
                                    <td> {{ $student->work_phone_number }} </td>
                                </tr>
                                <tr>
                                    <th> High school name </th>
                                    <td> {{ $student->high_school_name }} </td>
                                </tr>
                                <tr>
                                    <th> High school graduation year </th>
                                    <td> {{ $student->high_school_graduation_year }} </td>
                                </tr>
                                <tr>
                                    <th> High school major </th>
                                    <td> {{ $student->high_school_major }} </td>
                                </tr>
                                <tr>
                                    <th> University name </th>
                                    <td> {{ $student->university_name }} </td>
                                </tr>
                                <tr>
                                    <th> University graduation year </th>
                                    <td> {{ $student->university_graduation_year }} </td>
                                </tr>
                                <tr>
                                    <th> University major </th>
                                    <td> {{ $student->university_major }} </td>
                                </tr>
                                <tr>
                                    <th> Parent name </th>
                                    <td> {{ $student->parent_name }} </td>
                                </tr>
                                <tr>
                                    <th> Parent phone number </th>
                                    <td> {{ $student->parent_phone_number }} </td>
                                </tr>
                                <tr>
                                    <th> Parent email </th>
                                    <td> {{ $student->parent_email }} </td>
                                </tr>
                                <tr>
                                    <th> Examination 1 </th>
                                    <td> {{ $student->examination1 }} </td>
                                </tr>
                                <tr>
                                    <th> Organization of issue 1</th>
                                    <td> {{ $student->organization_of_issue1 }} </td>
                                </tr>
                                <tr>
                                    <th> Grade 1</th>
                                    <td> {{ $student->grade1 }} </td>
                                </tr>
                                <tr>
                                    <th> Score 1</th>
                                    <td> {{ $student->score1 }} </td>
                                </tr>
                                <tr>
                                    <th> Date 1</th>
                                    <td> {{ $student->date1 }} </td>
                                </tr>
                                <tr>
                                    <th> Examination 2 </th>
                                    <td> {{ $student->examination2 }} </td>
                                </tr>
                                <tr>
                                    <th> Organization of issue 2</th>
                                    <td> {{ $student->organization_of_issue2 }} </td>
                                </tr>
                                <tr>
                                    <th> Grade 2</th>
                                    <td> {{ $student->grade2 }} </td>
                                </tr>
                                <tr>
                                    <th> Score 2</th>
                                    <td> {{ $student->score2 }} </td>
                                </tr>
                                <tr>
                                    <th> Date 2</th>
                                    <td> {{ $student->date2 }} </td>
                                </tr>
                                <tr>
                                    <th> Examination 3 </th>
                                    <td> {{ $student->examination3 }} </td>
                                </tr>
                                <tr>
                                    <th> Organization of issue 3</th>
                                    <td> {{ $student->organization_of_issue3 }} </td>
                                </tr>
                                <tr>
                                    <th> Grade 3</th>
                                    <td> {{ $student->grade3 }} </td>
                                </tr>
                                <tr>
                                    <th> Score 3</th>
                                    <td> {{ $student->score3 }} </td>
                                </tr>
                                <tr>
                                    <th> Date 3</th>
                                    <td> {{ $student->date3 }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@if(Session::has('application_saved'))
	@push('js')
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<script type="text/javascript">
			Swal.fire(
			  '{{__('message.ariza_yuborildi')}}',
			  '',
			  'success'
			)
		</script>
	@endpush
@endif

@if(Session::has('application_updated'))
	@push('js')
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<script type="text/javascript">
			Swal.fire(
			  '{{__('message.ariza_ozgartirildi')}}',
			  '',
			  'success'
			)
		</script>
	@endpush
@endif
