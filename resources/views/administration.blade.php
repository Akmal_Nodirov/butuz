@extends('layouts.app')
@section('title',__('message.about_us'))
@section('content')
<div class="container-fluid page-banner banner_administration about no-padding">

	<div class="container">
		<div class="banner-content-block">
		    {{--
			<div class="banner-content">
				<h3>{{__('message.administration')}}</h3>
				<ol class="breadcrumb">
					<li><a href="{{ url('/') }}">{{__('message.menu_home')}}</a></li>
					<li class="active">{{__('message.administration')}}</li>
				</ol>
			</div>
			--}}
		</div>
	</div>

</div><!-- Page-->

<section >
	<div class="container">
		<div class="row">
			<div class="col-md-9 adm_tab">
				
				<div class="tab">
					@foreach($positions as $key => $position)
						<button class="tablinks" onclick="openCity(event, '{{'position'.$position->id}}')" {{ ($key == 0) ? "id=defaultOpen" : ""}}>{{$position['position_'.app()->getLocale()]}}</button>
					@endforeach
				</div>
				@foreach($positions as $key => $position)
					<div id="{{'position'.$position->id}}" class="tabcontent">
						@foreach($position->administrations as $administrator)
							<div class="row">
								<div class="col-md-12">
									<div class="per__blog">
										<div class="per_img">
											<img src="{{$administrator->image}}" alt="{{$administrator['fullname_'.app()->getLocale()]}}">
										</div>
										<div class="per_title">
											<h3>{{$position['position_'.app()->getLocale()]}}</h3>
										</div>
										<div class="per_text">
											<h5>{{$administrator['fullname_'.app()->getLocale()]}}</h5>
											<p>{{__('message.telephone_number')}}:</p>
											<p>{{$administrator['phone']}}</p>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				@endforeach
				
			</div>
			<div class="col-md-3 right_bar">
				<ul style="padding-left: 0 !important" class="border">
					<li class="bold">{{__('message.menu_introduction')}}</li>
					@foreach($about_pages as $about)
						<li><a href="{{url('/about/'.$about->id)}}">{{$about['title_'.app()->getLocale()]}} </a></li>
					@endforeach
					<li class="active_sidebar"><a href="{{url('/administration')}}">{{__('message.administration')}}</a>
				</ul>
			</div>
		</div>
	</div>
</section>
@endsection
@push('js')
	<script type="text/javascript">
		function openCity(evt, cityName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}
		
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
	</script>
@endpush()