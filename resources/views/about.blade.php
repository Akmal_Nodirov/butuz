@extends('layouts.app')
@section('title',__('message.about_us'))
@section('content')
	<div class="container-fluid page-banner banner_about about no-padding">
		<div class="container">
			<div class="banner-content-block">
				{{--
				<div class="banner-content">
					<h3>{{$about['title_'.app()->getLocale()]}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{$about['title_'.app()->getLocale()]}}</li>
					</ol>
					--}}
				</div>
				
			</div>
		</div>
	</div><!-- Page-->
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-12">
							<div class="content_block">
								{{--
								<h3 class="h3-b50">{{$about['title_'.app()->getLocale()]}}</h3>
								<!-- <img src="images/about-banner.jpg" alt="" class="content-block-img-left"> -->
								<h2 class="text-center h3-t50">{{$about['title_'.app()->getLocale()]}}</h2>
								--}}
								<br>
								{!! $about['content_'.app()->getLocale()] !!}
								
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 right_bar">
					<ul style="padding-left: 0 !important" class="border">
						<li class="bold">{{__('message.menu_introduction')}}</li>
						@foreach($about_pages as $about)
							<li class="{{($about->id == $id) ? 'active_sidebar' : ''}}"><a href="{{url('/about/'.$about->id)}}">{{$about['title_'.app()->getLocale()]}} </a></li>
						@endforeach
						<li><a href="{{url('/administration')}}">{{__('message.administration')}}</a></li>
					</ul>
				</div>
			</div>
		</div>
	</section>
@endsection