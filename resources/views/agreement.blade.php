@extends('layouts.app')
@section('title',__('message.menu_apply'))
@section('content')
	<section>
		<div class="container m_t200">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center agree_title">{{__('message.first_stud')}}</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 mt_50">
					<div class="shadow_facult">
						<div class="facult_title">
							<h4 class="m_0 fsw">{{__('message.facult_stea')}}</h4>
						</div>
						<div class="facult_main">
							<div class="fac_number">
								1
							</div>
							<div class="fac_text">
							    {{__('message.fac_prescl')}}
							</div>
							<div class="fac_foo">
								<span>120</span>{{__('message.people')}}
							</div>
						</div>

						<div class="facult_main">
							<div class="fac_number">
								2
							</div>
							<div class="fac_text">
							    {{__('message.fac_ev')}}
								
							</div>
							<div class="fac_foo">
								<span>75</span> {{__('message.people')}}
							</div>
						</div>

						<div class="facult_main">
							<div class="fac_number">
								3
							</div>
							<div class="fac_text">
							    {{__('message.fac_arch')}}
							</div>
							<div class="fac_foo">
								<span>45</span> {{__('message.people')}}
							</div>
						</div>

						<div class="facult_main border_rad">
							<div class="fac_number">
								4
							</div>
							<div class="fac_text ">
							    {{__('message.fac_kore')}}
							</div>
							<div class="fac_foo">
								<span>45</span> {{__('message.people')}}
							</div>
						</div>
					</div>
					
				</div>

				<div class="col-md-6 mt_50">
					<div class="shadow_facult">
						<div class="facult_title">
							<h4 class="m_0 fsw">{{__('message.req_doc')}}</h4>
						</div>
						<div class="facult_main">
							<div class="fac_number">
								1
							</div>
							<div class="fac_text">
							    {{__('message.onl_ap')}}
							</div>
						</div>

						<div class="facult_main">
							<div class="fac_number">
								2
							</div>
							<div class="fac_text">
							    {{__('message.cop_ats')}}
							</div>
						</div>

						<div class="facult_main">
							<div class="fac_number">
								3
							</div>
							<div class="fac_text">
							    {{__('message.cop_pas')}}
							</div>
						</div>

						<!--<div class="facult_main">
							<div class="fac_number">
								4
							</div>
							<div class="fac_text">
								Копия мед.справки 086
							</div>
						</div>-->

						<div class="facult_main">
							<div class="fac_number">
								4
							</div>
							<div class="fac_text">
							    {{__('message.cop_inn')}}
							</div>
						</div>

						<div class="facult_main border_rad">
							<div class="fac_number">
								5
							</div>
							<div class="fac_text">
							    {{__('message.phot_3x4')}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
<section >
    <div class="container mt_50">
        <div class="row">
            <div class="col-md-12">
                <div class="table_block">
                    <table class="table text-left border_radius shadow_facult" align="center">
                        <thead class="facult_title td_border">
                        <tr>
                            <th style="border-radius: 15px 0 0 0; border-right: 2px solid #ffffff" class="text-center fsw wid50" colspan="2" scope="col" style="width: 50%;">{{__('message.offline_type')}}</th>
                            <th style="border-radius: 0 15px 0 0;"  class="text-center fsw wid50" colspan="2" scope="col" style="width: 50%;">{{__('message.online_type')}}</th>
                        </tr>
                        </thead>
                        <tbody class="td_border">
                        <tr class="active_table">
                            <td class="text-center fsw" colspan="4">{{__('message.srok_pod')}}</td>
                        </tr>
                        <tr>
                            <td>{{__('message.time_prem')}}</td>
                            <td>{{__('message.pon_pya')}}</td>
                            <td>{{__('message.tim_prem2')}}</td>
                            <td>{{__('message.fullt')}}</td>
                        </tr>

                        <tr>
                            <td>{{__('message.lea_day')}}</td>
                            <td>{{__('message.sunday_ye')}}</td>
                            <td>{{__('message.vyxod_dni')}}</td>
                            <td>{{__('message.bez_vyx')}}</td>
                        </tr>

                        <tr>
                            <td>{{__('message.spos_poda')}}</td>
                            <td>{!!__('message.zap_onl')!!}</td>
                            <td>{!!__('message.spos_podac')!!}</td>
                            <td>{!!__('message.zap_on_zaya')!!}</td>
                        </tr>

                        <tr>
                            <td>{!!__('message.ad_re_s')!!}</td>
                            <td>{!!__('message.uch_cen_hag')!!}
                                </td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="table_main shadow_facult">
						<table class="table table-striped mt_50">
							<thead class="td_border">
								<tr>
									<th class="p0" colspan="4"><h1 class="text-center facult_title m0 p20">{!!__('message.pocedura_title')!!}</h1></th>
								</tr>
								<tr class="active_table">
									<th scope="col">{!!__('message.pro_t_s')!!}</th>
									<th scope="col">{!!__('message.sr_ok')!!}</th>
									<th scope="col">{!!__('message.sod_erj')!!}</th>
									<th scope="col">{!!__('message.primech')!!}</th>
								</tr>
							</thead>
							<tbody class="td_border">
								<tr>
									<td  scope="row">{!!__('message.onl_zaya_v')!!}</td>
									<td scope="col">{{__('message.date_tim_e')}}</td>
									<td scope="col">{{__('message.on_re_g_sayt')}}</td>
									<td scope="col"></td>
								</tr>

								<tr class="active">
									<td scope="row">{{__('message.off_on_pod')}}</td>
									<td>{{__('message.date_time_e2')}}</td>
									<td>{{__('message.sdac_docc')}}</td>
									<td>{!!__('message.reg_vzns')!!}</td>
								</tr>

								<tr>
									<td scope="row">{{__('message.test_irov')}}</td>
									<td>{{__('message.date_tim_e3')}}</td>
									<td>{!!__('message.ponap_dosh')!!}</td>
									<td>
										<p>{{__('message.usl_osv')}}</p>
										<p>{{__('message.pona_pra')}}</p>
										<p>{{__('message.pona_kor')}}</p>
									</td>
								</tr>

								<tr class="active">
									<td scope="row">{{__('message.sobes_nie')}}</td>
									<td>{{__('message.date_tim_e4')}}</td>
									<td>{{__('message.sobes_pered')}}</td>
									<td></td>
								</tr>

								<tr>
									<td scope="row">{{__('message.rez_obya')}}</td>
									<td>3 мая</td>
									<td>{{__('message.rez_univer')}}</td>
									<td></td>
								</tr>

								<tr class="active">
									<td scope="row">{{__('message.predopl_kontr')}}</td>
									<td>{{__('message.date_tim_e5')}}</td>
									<td></td>
									<td>{{__('message.pred_op_l')}}</td>
								</tr>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-4 mt_50">
					<div class="table_footer">
						<div class="tfooter">
							<h3 class="fsw">{{__('message.fac_doshk')}}</h3>
						</div>
						<div class="tmain">
    						    <p>{!!__('message.stoim_kontr1')!!}</p>
							<p>{{__('message.but_3_goda')}}</p>
							<p>{!!__('message.but_1_goda')!!}</p>
							
						</div>
					</div>
				</div>
				<div class="col-md-4 mt_50">
					<div class="table_footer">
						<div class="tfooter">
							<h3 class="fsw">{{__('message.title_koreys_2_2')}}</h3>
						</div>
						<div class="tmain">
						    <p >{!!__('message.stoim_kontr2')!!}</p>
							<p>{{__('message.two_year')}}</p>
							<p class="active" style="width:50%; float:left; border-right: 1px solid #feca2b;">{{__('message.two_year_but')}}</p>
							<p style="width:50%; float:left;">{{__('message.two_year_but2')}}</p>
							<div class="clear_"></div>
							<p>{{__('message.pers_pekt_iv')}}</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 mt_50">
					<div class="table_footer">
						<div class="tfooter">
							<h3 class="fsw">{{__('message.arch_fac_3')}}</h3>
						</div>
						<div class="tmain">
						    <p >{!!__('message.stoim_kontr2')!!}</p>
						<p>{{__('message.fak_arch_info')}}</p>
							<p style="width:50%; float:left; border-right: 1px solid #feca2b;">{{__('message.one_year_but')}} </p>
							<p style="width:50%; float:left;">{{__('message.but_one_year_dip')}}</p>
							<div class="clear_"></div>
							<p>{{__('message.pers_pekt_trud')}}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="section-padding"></div>

	<section>
		<div class="container mb_20">
			<div class="row">
				<div class="colr-md-12 ">
					<div class="text-center m_auto">
						<form action="/i_agree" method="post">
							{{csrf_field()}}
							<div class="border">
								{{-- <h4 class="text-center p20">Если вы ознакомились со всеми требованиями тогда ниже дайте согласие на предоставления требуемых документов для заполнение заявления</h4> --}}
								<input type="checkbox" id="check" name="check" />
								<label for="check">&nbsp;&nbsp;{{__('message.tocontinue')}}</label>
									<div class="butt">
								<button type="submit" class="more_b  mb_20" title="Continue">{{__('message.count_nu')}}</button>
							</div>
							</div>
						
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection