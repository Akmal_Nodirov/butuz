
@extends('layouts.dean.app')
@push('css')
    <style type="text/css">
   
   .form-inline {
       height: 38px;
    margin: 30px;
    padding-top: 55px;
   }
   
    .th_center{
        text-align:center;
    }
    

    
    .data_ {
    font-size: 20px;
    font-weight: bold;
    margin: 130px 30px 30px;
    }
    
    .only_print {
            width: 565px;
        margin: 120px  auto 10px;
        display: table;
    }
    
    .logo_bucheon {
    width: 100px;
    float: left;
    margin-right: 20px;
    vertical-align: middle;
    display: table-cell;
}

.logo_bucheon_t {
    z-index: 0;
    position: absolute;
    width: 60%;
    left:23%;
}

  .logo_bucheon{
	        	width:100px;
	        }
        .table thead tr th,.table tbody tr td {
                border: 1px solid #000 !important;
        }

 
        @media print{
            
           .margin_minus {
               height: 1px;
               margin-top: -24px;
           }
           
            
           .form-inline {
                display: none;
                margin: 0;
                padding: 0;
               }
            
            .break_ {
                
                /*position: relative;
                
                width: 620px;
                margin: 0 auto;*/
                padding: 150px;
                padding-right: 50px;
                padding-left: 40px;
                height: 1214px;
            }
            
        /*   .break_div {
               page-break-before: always;
            }*/
            
            .table thead tr th{
                width:30px;
                overflow:hidden;
                font-size:15px;
                padding: 5px 10px;
            }
            .table tbody tr td{
                width:30px;
                overflow:hidden;
                font-size:15px;
                padding: 5px 10px;
            }
            
            .subject_print {
                min-width: 400px;
            }
            

            .header {
                display: none;
                padding: 0;
                margin: 0;
            }

            .card-block form {
                display: none;
            }

            .fa {
                display: none;
            }
            
/*            body { 
    overflow: visible !important; 
    overflow-x: visible !important; 
    overflow-y: visible !important; 
}
*/
/*this is because I use angular and have this particular layout*/
/*body > .app { 
    height: auto; 
    display: block;
    flex: none;
    float: none;

}
.dashboard-page {
    float: none;
}*/
            
        }

      
        
  .app .content {
    padding: 0px;
    min-height: 100vh;
    
      
  }
    
    @media (min-width: 1200px) {
      .app .content {
        padding: 0px; }
        
        
    }
    @media (min-width: 992px) and (max-width: 1199.98px) {
      .app .content {
        padding: 0px; }
        
    }
    @media (min-width: 768px) and (max-width: 991.98px) {
      .app .content {
        padding: 0px; }
        
        
        }
    @media (max-width: 767.98px) {
      .app .content {
        padding: 0px; }
        
        .header {
                display: none;
                padding: 0;
                margin: 0;
            }
            
            .form-inline {
                display: none;
                margin: 0;
                padding: 0;
               }
    }
  @media (max-width: 991.98px) {
    .app {
      padding: 0px; }
      
      .header {
                display: none;
                padding: 0;
                margin: 0;
            }
            
            .form-inline {
                display: none;
                margin: 0;
                padding: 0;
               }
  }
      
    </style>
@endpush
@section('content')
	<form class="form-inline" method="get">
      <div class="form-group mb-2">
        <label for="course" class="sr-only">Course: </label> 
        <select type="text" class="form-control" id="course" name="course" placeholder="Choose course...">
            <option>Choose course...</option>
            @for($i=1;$i<=3;$i++)
                <option value="{{$i}}" {{ (isset($_GET['course']) && $_GET['course'] == $i) ? 'selected' : '' }}>{{$i}}</option>
            @endfor
        </select>
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="semester" class="sr-only">Semester: </label> 
        <select type="text" class="form-control" id="semester" placeholder="Choose semester..." name="semester">
            <option>Choose semester...</option>
            @for($i=1;$i<=2;$i++)
                <option value="{{$i}}" {{ (isset($_GET['semester']) && $_GET['semester'] == $i) ? 'selected' : '' }}>{{$i}}</option>
            @endfor                    
        </select>    
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="group" class="sr-only">Group: </label> 
        <select type="text" class="form-control" id="group" placeholder="Choose group..." name="group">
            <option>Choose group...</option>
            @foreach($groups as $key => $group)
                <option value="{{$group->id}}" {{ (isset($_GET['group']) && $_GET['group'] == $group->id) ? 'selected' : '' }}>{{$group->course.' '.$group->name.' '.$group->faculty->name}}</option>
            @endforeach      
        </select>    
      </div>
      <button type="submit" class="btn btn-primary mb-2">Show results</button>
    </form>
    <div class="margin_minus"></div>
    @if(isset($_GET['course']) && isset($_GET['semester']) && isset($_GET['group']))
		@foreach($students as $student)
<!--	<div class="" style="margin-bottom:20%;"></div>-->
			<div class="container break_">
				<div class="row">
					<div class="col-sm-12">
					    <hr>
					        Student: {{$student->firstname.' '.$student->lastname}}<br>
					        Student. no: {{$student->student_id}}<br>
					        Course: {{$student->group->course}}<br>
						@if(isset($_GET['semester']))
							Semester: 2018/2019 1st semester<br>
						@endif	
							Faculty: {{$student->faculty->name}}<br>
							Group: {{$student->group->name}}<br>
							<hr>
		                <img class="logo_bucheon_t" src="{{asset('temporary/but_transparent.png')}}">
						<h3 style="margin: 30px 0">Record of Learning and Achievement</h3>
						

			            @if((isset($_GET['course']) && is_numeric($_GET['course'])) && (isset($_GET['semester']) && is_numeric($_GET['semester'])))
			                <div style="overflow:auto">    
			                    <table class="table table-bordered table-condensed table-hover">
			                        <thead>
			                            <tr>
			                                <!--<th class="th_center">#</th>-->
			                                {{-- <th>Group</th> --}}
			                                <th class="th_center">Subject</th>
			                                {{-- <th>Professor</th> --}}
			                                {{-- <th>Late</th>
			                                <th>Absent</th> --}}
			                                {{-- <th>Attendance</th>
			                                <th>Homework</th>
			                                <th>Assignment</th>
			                                <th>Middle Term</th>
			                                <th>Final Term</th> --}}
			                                <th class="th_center">Total</th>
			                                <th class="th_center">Mark</th>
			                                <th>Credit</th>
			                                {{-- <th>Rate</th>
			                                <th>Credit * Rate</th> --}}
			                            </tr>
			                        </thead>
			                        <tbody class="t_body">
			                            <?php $totals = 0; $credits=0; ?>
			                            @foreach($semester as $key => $sem)
			                           		@if($sem->application_id == $student->id) 
				                                <?php 
				                                    $totals += ($sem->total*$sem->subject->credit); 
				                                    $credits += $sem->subject->credit;
				                                ?>
				                                <tr>
				                                    <!--<td class="th_center">{{++$key}}</td>-->
				                                    {{-- <th>{{$semester->group->name.' ('.$semester->group->faculty->name.')'}}</th> --}}
				                                    <td class="subject_print">{{$sem->subject->subject}}</td>
				                                    {{-- <td>{{$semester->professor->firstname.' '.$semester->professor->lastname}}</td> --}}
				                                    {{--<td>{{$semester->late_count}}</td>
				                                    <td >{{$semester->absent_count}}</td> --}}
				                                    {{-- <td>{{$semester->present_count}}</td>
				                                    <td>{{$semester->homework}}</td>
				                                    <td>{{$semester->bonus}}</td>
				                                    <td>{{$semester->middle_exam}}</td>
				                                    <td>{{$semester->final_exam}}</td> --}}
				                                    <td class="th_center">{{round($sem->total)}}</td>
				                                    <td class="th_center">{{$sem->mark}}</td>
				                               		<td class="th_center">{{$sem->subject->credit}}</td>
				                                    {{-- <td>{{$semester->rate}}</td>
				                                    <td>{{$semester->credit_x_rate}}</td> --}}
				                                </tr>
				                            @endif    
			                            @endforeach
			                            {{-- <tr style="font-weight:bold"> --}}
			                                {{-- <td></td> --}}
			                                {{-- <td></td> --}}
			                                {{-- <td></td> --}}
			                                {{-- <td></td>
			                                <td></td> --}}
			                                {{-- <td></td>
			                                <td></td>
			                                <td></td>
			                                <td></td>
			                                <td></td> --}}
			                                {{-- <td>Total</td> --}}
			                                {{-- <td class="th_center">{{ round($totals) }}</td> --}}
			                                {{-- <td></td> --}}
			                                {{-- <td></td>
			                                <td></td> --}}
			                            {{-- </tr> --}}
			                            <tr style="font-weight:bold">
			                                <!--<td></td>-->
			                                {{-- <td></td> --}}
			                                {{-- <td></td> --}}
			                                {{-- <td></td>
			                                <td></td> --}}
			                                {{-- <td></td>
			                                <td></td>
			                                <td></td>
			                                <td></td> --}}
			                                <td>Average Score</td>
			                                <td class="th_center">
			                                	{{ $average = $credits != 0 ? round($totals/$credits) : 0 }}
			                                </td>
			                                <td class="th_center">
			                                	@if($average>=95 && $average<=100)
			                                		A+
			                                	@elseif($average>=90 && $average<=94)
			                                		A
			                                	@elseif($average>=85 && $average<=89)
			                                		B+
			                                	@elseif($average>=80 && $average<=84)
			                                		B
			                                	@elseif($average>=75 && $average<=79)
			                                		C+
			                                	@elseif($average>=70 && $average<=74)
			                                		C
			                                	@elseif($average>=65 && $average<=69)
			                                		D+
			                                	@elseif($average>=60 && $average<=64)
			                                		D
			                                	@elseif($average>0 && $average<=59)
			                                		F	
			                                	@else
			                                		
			                                	@endif
			                                </td>
			                                {{-- <td>
			                                	@if($average>=95 && $average<=100)
			                                		4.5
			                                	@elseif($average>=90 && $average<=94)
			                                		4.0
			                                	@elseif($average>=85 && $average<=89)
			                                		3.5
			                                	@elseif($average>=80 && $average<=84)
			                                		3.0
			                                	@elseif($average>=75 && $average<=79)
			                                		2.5
			                                	@elseif($average>=70 && $average<=74)
			                                		2.0
			                                	@elseif($average>=65 && $average<=69)
			                                		1.5
			                                	@elseif($average>=60 && $average<=64)
			                                		1.0
			                                	@elseif($average>0 && $average<=59)
			                                		0.0	
			                                	@else
			                                		0.0
			                                	@endif
			                                </td> --}}
			                                {{-- <td></td> --}}
			                                <td></td>
			                            </tr>
			                            <tr style="font-weight:bold">
			                                <!--<td></td>-->
			                                {{-- <td></td> --}}
			                                {{-- <td></td> --}}
			                                {{-- <td></td>
			                                <td></td> --}}
			                                {{-- <td></td>
			                                <td></td>
			                                <td></td> --}}
			                                <td>Average Grade</td>
			                                {{-- <td class="th_center">
			                                	{{ $average = $credits != 0 ? round($totals/$credits) : 0 }}
			                                </td> --}}
			                                <td class="th_center">
			                                	@if($average>=95 && $average<=100)
			                                		4.5
			                                	@elseif($average>=90 && $average<=94)
			                                		4.0
			                                	@elseif($average>=85 && $average<=89)
			                                		3.5
			                                	@elseif($average>=80 && $average<=84)
			                                		3.0
			                                	@elseif($average>=75 && $average<=79)
			                                		2.5
			                                	@elseif($average>=70 && $average<=74)
			                                		2.0
			                                	@elseif($average>=65 && $average<=69)
			                                		1.5
			                                	@elseif($average>=60 && $average<=64)
			                                		1.0
			                                	@elseif($average>0 && $average<=59)
			                                		0.0	
			                                	@else
			                                		0.0
			                                	@endif
			                                </td>
			                                <td></td>
			                                <td></td>
			                                <tr style="font-weight:bold">
				                                <!--<td></td>-->
				                                {{-- <td></td> --}}
				                                {{-- <td></td> --}}
				                                {{-- <td></td>
				                                <td></td> --}}
				                                {{-- <td></td>
				                                <td></td>
				                                <td></td> --}}
				                                <td>Total Credit</td>
				                                <td class="th_center">
				                                	
				                                </td>
				                                <td></td>
				                                <td>{{$credits}}</td>
				                            </tr>
			                            </tr>
			                        </tbody>
			                    </table>
			                </div>
			            @endif
					</div>

		            <div style="clear:both"></div>
					<div class="only_print">
			            <img class="logo_bucheon" src="{{asset('temporary/logo.png')}}">
						<h3 style="font-weight: bold; display: table-cell; vertical-align: middle;">Bucheon University in Tashkent</h3>
					</div>

				</div>
				<p class="data_">Date of issue:&nbsp;{{ date('d.m.Y') }}</p> 
			</div>
			
<div class="" style="margin-bottom:20%;"></div>
    
		@endforeach	
	@endif	
@endsection
<div class="break_div"></div>