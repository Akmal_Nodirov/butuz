<div class="form-group {{ $errors->has('course') ? 'has-error' : ''}}">
    <label for="course" class="control-label">{{ 'Course' }}</label>
    <input class="form-control" name="course" type="text" id="course" value="{{ isset($group->course) ? $group->course : ''}}" >
    {!! $errors->first('course', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Group' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($group->name) ? $group->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('faculty_id') ? 'has-error' : ''}}">
    <label for="faculty_id" class="control-label">{{ 'Faculty' }}</label>
    {{-- <input class="form-control" name="faculty_id" type="text" id="faculty_id" value="{{ isset($group->faculty_id) ? $group->faculty_id : ''}}" > --}}
    <select id="faculty_id" class="form-control" name="faculty_id">
        <option>Choose faculty...</option>
        @foreach(\App\Faculty::all() as $faculty)
            <option value="{{ $faculty->id }}" {{ (isset($group->faculty_id) && $group->faculty_id == $faculty->id) ? 'selected' : ''}} >{{ $faculty->name }}</option>
        @endforeach
    </select>
    {!! $errors->first('faculty_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
