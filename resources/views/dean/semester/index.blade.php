@extends('layouts.dean.app')

@section('content')
    <div class="container">
        <div class="row">

            {{-- @include('admin.sidebar') --}}
            
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Semester</div>
                    <div class="card-body">
                        {{-- <a href="{{ url('/professor/semester/create') }}" class="btn btn-success btn-sm" title="Add New Semester">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a> --}}

                        <a href="{{ url('/professor/new_mark') }}" class="btn btn-success btn-sm" title="Add New Semester">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <form method="GET" action="{{ url('/professor/semester') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Course</th>
                                        <th>Semester</th>
                                        <th>Student</th>
                                        <th>Professor</th>
                                        <th>Subject</th>
                                        <th>Late count</th>
                                        <th>Absent count</th>
                                        <th>Present count</th>
                                        <th>Homework</th>
                                        <th>Middle exam</th>
                                        <th>Final exam</th>
                                        <th>Total</th>
                                        <th>Mark</th>
                                        {{-- <th>Actions</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $iteration = isset($_GET['page']) ? ($_GET['page']-1) * 25 : 0;
                                ?>
                                @foreach($semester as $item)
                                    <tr>
                                        <td>{{ ++$iteration }}</td>
                                        <td>{{ $item->course }}</td>
                                        <td>{{ $item->semester }}</td>
                                        <td>{{ $item->application->firstname.' '.$item->application->lastname }}</td>
                                        <td>{{ $item->subject->subject }}</td>
                                        <td>{{ $item->professor->firstname.' '.$item->professor->lastname }}</td>
                                        <td>{{ $item->late_count }}</td>
                                        <td>{{ $item->absent_count }}</td>
                                        <td>{{ $item->present_count }}</td>
                                        <td>{{ $item->homework }}</td>
                                        <td>{{ $item->middle_exam }}</td>
                                        <td>{{ $item->final_exam }}</td>
                                        <td>{{ $item->total }}</td>
                                        <td>{{ $item->mark }}</td>

                                        {{-- <td>
                                            <a href="{{ url('/professor/semester/' . $item->id) }}" title="View Semester"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/professor/semester/' . $item->id . '/edit') }}" title="Edit Semester"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/professor/semester' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Semester" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td> --}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $semester->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
