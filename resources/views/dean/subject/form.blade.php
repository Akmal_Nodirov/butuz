<div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
    <label for="subject" class="control-label">{{ 'Subject' }}</label>
    <input class="form-control" name="subject" type="text" id="subject" value="{{ isset($subject->subject) ? $subject->subject : ''}}" >
    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    {{-- <input class="form-control" name="type" type="text" id="type" value="{{ isset($subject->type) ? $subject->type : ''}}" > --}}
    <select class="form-control" name="type" id="type">
    	<option value="0" {{ (isset($subject->type) && $subject->type == '0')? 'selected' : ''}} >Elective</option>
    	<option value="1" {{ (isset($subject->type) && $subject->type == '1') ? 'selected' : ''}} >Major</option>
    </select>
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('homework') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Homework' }}</label>
    {{-- <input class="form-control" name="type" type="text" id="type" value="{{ isset($subject->type) ? $subject->type : ''}}" > --}}
    <select class="form-control" name="homework" id="type">
        <option value="0" {{ (isset($subject->homework) && $subject->homework == '0')? 'selected' : ''}} >No</option>
        <option value="1" {{ (isset($subject->homework) && $subject->homework == '1') ? 'selected' : ''}} >Yes</option>
    </select>
    {!! $errors->first('homework', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('bonus') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Bonus' }}</label>
    {{-- <input class="form-control" name="type" type="text" id="type" value="{{ isset($subject->type) ? $subject->type : ''}}" > --}}
    <select class="form-control" name="bonus" id="type">
        <option value="0" {{ (isset($subject->bonus) && $subject->bonus == '0')? 'selected' : ''}} >No</option>
        <option value="1" {{ (isset($subject->bonus) && $subject->bonus == '1') ? 'selected' : ''}} >Yes</option>
    </select>
    {!! $errors->first('homework', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    <label for="code" class="control-label">{{ 'Code' }}</label>
    <input class="form-control" name="code" type="text" id="code" value="{{ isset($subject->code) ? $subject->code : ''}}" >
    {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('credit') ? 'has-error' : ''}}">
    <label for="credit" class="control-label">{{ 'Credit' }}</label>
    <input class="form-control" name="credit" type="text" id="credit" value="{{ isset($subject->credit) ? $subject->credit : ''}}" >
    {!! $errors->first('credit', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group form-float">
	<label class="form-label">Professors</label>
	<select name="professors[]" id="professor" class="form-control selectpicker" data-live-search="true" multiple>
		@foreach(\App\Professor::all() as $professor)
			<option value="{{ $professor->id }}" 
				@if(!Request::is('dean/subject/create'))
					@foreach($subject->professors as $subProf)
						{{ $professor->id == $subProf->id ? 'selected' : '' }}
					@endforeach
				@endif
			> 
				{{ $professor->firstname }} {{ $professor->lastname }} 
			</option>
		@endforeach
	</select>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
@push('js')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('.selectpicker').select2();
		});
	</script>
@endpush