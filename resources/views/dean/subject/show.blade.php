@extends('layouts.dean.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Subject {{ $subject->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/dean/subject') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/dean/subject/' . $subject->id . '/edit') }}" title="Edit Subject"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('dean/subject' . '/' . $subject->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Subject" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                   {{--  <tr>
                                        <th>ID</th><td>{{ $subject->id }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th> Subject </th>
                                        <td> {{ $subject->subject }} </td>
                                    </tr>
                                    <tr>
                                        <th> Type </th>
                                        <td> {{ $subject->type == '0' ? 'Additional' : 'Special' }} </td>
                                    </tr>
                                    <tr>
                                        <th> Homework </th>
                                        <td> {!! ($subject->homework == '0' || $subject->homework == null) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Bonus </th>
                                        <td> {!! ($subject->bonus == '0' || $subject->bonus == null) ? '<span class="badge badge-danger">No</span>' : '<span class="badge badge-success">Yes</span>' !!} </td>
                                    </tr>
                                    <tr>
                                        <th> Code </th>
                                        <td> {{ $subject->code }} </td>
                                    </tr>
                                    <tr>
                                        <th> Credit </th>
                                        <td> {{ $subject->credit }} </td>
                                    </tr>
                                    <tr>
                                        <th> Professors </th>
                                        <td> 
                                            <ul>
                                                @foreach($subject->professors as $professor)
                                                    <li>
                                                        <a href="{{url('dean/professor/'.$professor->id)}}">
                                                            {{ $professor->firstname.' '.$professor->lastname }}
                                                        </a>
                                                    </li>
                                                @endforeach    
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
