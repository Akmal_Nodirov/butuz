@extends('layouts.app')
@section('title',$program['title_'.app()->getLocale()])
@section('content')
	<div class="container-fluid page-banner about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{$program['title_'.app()->getLocale()]}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{ url('/') }}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{$program['title_'.app()->getLocale()]}}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Page-->
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="content_block">
								<h3 class="h3-b50">{{ $program['title_'.app()->getLocale()] }}</h3>
								<img src="{{ asset('storage/programs/'.$program['image']) }}" alt="" class="content-block-img-left">
								<h2 class="text-center h3-t50">{{ $program['title_'.app()->getLocale()] }}</h2>
								
								{!! $program['description_'.app()->getLocale()] !!}
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection