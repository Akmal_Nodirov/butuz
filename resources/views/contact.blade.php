@extends('layouts.app')
@section('title',__('message.menu_contact_us'))
@push('css')
	<style type="text/css">
		.help-block{
			color:red;
		}
	</style>
@endpush
@section('content')
	<div class="container-fluid page-banner banner_contact about no-padding">
	
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{__('message.menu_contact_us')}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">{{__('message.menu_home')}}</a></li>
						<li class="active">{{__('message.menu_contact_us')}}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
	
	</div><!-- PageBanner /- -->
	<!-- socialmedia -->
	<div class="container-fulid no-padding contactus">
		<div class="section-padding"></div>
		<div class="container">
			<div class="row">
				<div class="contactus-info-block">

					<div class="col-lg-3 text-center">
						<div class="contact_box">
							<span class="fa fa-home" style="color: #000"></span>
							<div class="info_box">
								<h3>{{__('message.our_location')}}</h3>
								<div class="a-items lin_hei">
									{!!$about['address_'.app()->getLocale()]!!}
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 text-center">
						<div class="contact_box">
							<span class="fa fa-phone" style="color: #000"></span>
							<div class="info_box">
								<h3>{{__('message.call_us_on')}}</h3>
								<div class="a-items fl_direc">
									<a href="tel:{{$about->tel1}}" title="{{$about->tel1}}">{{$about->tel1}}</a><br>
									<a href="tel:{{$about->tel2}}" title="{{$about->tel2}}">{{$about->tel2}}</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 text-center">
						<div class="contact_box">
							<span class="fa fa-envelope" style="color: #000"></span>
							<div class="info_box">
								<h3>{{__('message.send_a_message')}}</h3>
								<div class="a-items"><a href="mailto:{{$about->email}}"></a>
									<p class="center-me">{{$about->email}}</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 text-center">
						<div class="contact_box">
							<span class="fa fa-globe" style="color: #000"></span>
							<div class="info_box">
								<h3>{{__('message.social_media')}}</h3>
								<div class="a-items">
									<a href="{{$about->facebook}}" title="Facebook"><i class="fab fa-facebook marg_5"></i></a>
									<a href="{{$about->youtube}}" title="Youtube"><i class="fab fa-youtube marg_5"></i></a>
									<a href="{{$about->telegram}}" title="Telegram"><i class="fab fa-telegram marg_5"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>		
		<!-- socialmedia -->
		<!-- form -->
		<div class="container">
			<div class="row contact-form-section">
				<div class="col-md-12 col-sm-12">
					<div class="section-header pt">
						<h3>{{__('message.leave_a_message')}}</h3>
						<span>{{__('message.feel_free_to_contact_us')}}</span>
					</div>
					<form id="contact-form" class="contactus-form" method="post" action="{{url('/send_message')}}">	
						{{csrf_field()}}					
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<input type="text" name="name" class="form-control" id="input_name" placeholder="{{__('message.your_name')}}*" required=""/>
								{!! $errors->first('name', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<input type="text" name="phone" class="form-control" id="input_phone" placeholder="{{__('message.phone')}}: 998901234567"/>
								{!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<input type="email" name="email" class="form-control" id="input_email" placeholder="{{__('message.your_email')}}" required=""/>
								{!! $errors->first('email', '<p class="help-block">:message</p>') !!}
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
								<input type="text" name="subject" class="form-control" id="input_subject" placeholder="{{__('message.subject')}}"/>
								{!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
							</div>
						</div>						
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<textarea rows="10" name="message" class="form-control" id="textarea_message" placeholder="{{__('message.message')}}"></textarea>
								{!! $errors->first('message', '<p class="help-block">:message</p>') !!}
							</div>
						</div>	
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="submit" value="{{__('message.send_message')}}" title="Send" name="post">
							</div>
						</div>
						<div id="alert-msg" class="alert-msg"></div>
					</form>	
					<hr>
				</div>	
			</div>
		</div>
		<div class="section-padding"></div>
	</div> <!-- form -->
	<!-- map -->
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 section-header">
					<h3>{{__('message.our_location')}}</h3>
					{!!$about->location!!}
				</div>
			</div>
		</div>
	</section>
	<!-- map -->
@endsection
@if(Session::has('message_sent'))
	@push('js')
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
		<script type="text/javascript">
			Swal.fire(
			  '{{ Session::get('message_sent') }}',
			  'Thank you for contacting us',
			  'success'
			)
		</script>
	@endpush
@endif	