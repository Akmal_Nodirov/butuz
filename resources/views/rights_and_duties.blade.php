@extends('layouts.app')
@section('title',__('message.rights_and_duties'))
@section('content')
<div class="container-fluid page-banner banner_rules about no-padding">
		<div class="section-padding"></div>
		<div class="container">
			<div class="banner-content-block">
			    {{--
				<div class="banner-content">
					<h3>{{__('message.rights_and_duties')}}</h3>
					<ol class="breadcrumb">
						<li><a href="{{ url('/') }}">{{__('message.menu_home')}}</a></li>
					<li class="active">{{__('message.rights_and_duties')}}</li>
					</ol>
				</div>
				--}}
			</div>
		</div>
		<div class="section-padding"></div>
	</div><!-- Page-->
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12">
							<div class="content_block">
								<h3 class="h3-b50">{{__('message.rights_and_duties')}}</h3>
								{!! $about['rights_'.app()->getLocale()] !!}
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</section>
@endsection	