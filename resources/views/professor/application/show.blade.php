@extends('layouts.professor.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Application {{ $application->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/professor/application') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/professor/application/' . $application->id . '/edit') }}" title="Edit Application"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('dean/application' . '/' . $application->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Application" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                   {{--  <tr>
                                        <th>ID</th><td>{{ $application->id }}</td>
                                    </tr> --}}
                                    <tr><th> Firstname </th><td> {{ $application->firstname }} </td></tr><tr><th> Lastname </th><td> {{ $application->lastname }} </td></tr><tr><th> Apply No </th><td> {{ $application->apply_no }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
