<div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
    <label for="subject" class="control-label">{{ 'Subject' }}</label>
    <input class="form-control" name="subject" type="text" id="subject" value="{{ isset($subject->subject) ? $subject->subject : ''}}" >
    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    {{-- <input class="form-control" name="type" type="text" id="type" value="{{ isset($subject->type) ? $subject->type : ''}}" > --}}
    <select class="form-control" name="type" id="type">
    	<option value="0" {{ (isset($subject->type) && $subject->type == '0')? 'selected' : ''}} >Additional</option>
    	<option value="1" {{ (isset($subject->type) && $subject->type == '1') ? 'selected' : ''}} >Special</option>
    </select>
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group form-float">
	<label class="form-label">Professors</label>
	<select name="professors[]" id="professor" class="form-control selectpicker" data-live-search="true" multiple>
		@foreach(\App\Professor::all() as $professor)
			<option value="{{ $professor->id }}" 
				@if(!Request::is('professor/subject/create'))
					@foreach($subject->professors as $subProf)
						{{ $professor->id == $subProf->id ? 'selected' : '' }}
					@endforeach
				@endif
			> 
				{{ $professor->firstname }} {{ $professor->lastname }} 
			</option>
		@endforeach
	</select>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
@push('js')
	<script type="text/javascript">
		$(document).ready(function() {
		    $('.selectpicker').select2();
		});
	</script>
@endpush