@extends('layouts.professor.app')

@push('css')
	<style type="text/css">
		.mark_input{
			width:50px;
			margin:0;
			border:none;
			width:90%;
			margin:5px 5% 5px 5%;
		}
		#mark_table td{
			padding:0;
		}
		#mark_table td:first-child{
			padding-left:7px;
		}
		#name{
			width:350px;
		}
		/*.print{
			display:none;
		}*/
		@media print{
			.rate, .rate_credit, .no-print, header.header{
				display:none;
			}	
			#mark_table thead th, #mark_table tbody td{
				font-size:10px!important;
			}
			.content.dashboard-page{
				padding-top:0;
			}
			
		}
		
		/*#mark_table td:nth-child(n):hover{
			background:green;
		}*/
	</style>
@endpush

<?php
	$course = $_GET['course'] ?? null;
	$semester = $_GET['semester'] ?? null;
	$group_id = $_GET['group'] ?? null;
	$subject_id = $_GET['subject'] ?? null;
?>

@section('content')
	<form class="form-inline" method="GET" action="">
	  <div class="form-group mb-2">
	    <label for="course" class="sr-only">Course</label>
	  </div>
	  <div class="form-group mx-sm-3 mb-2">
	    <label for="course">Course</label>
	    <select class="form-control" id="course" name="course">
	    	<option>Choose course...</option>
	    	@for($i=1;$i<=3;$i++)
	    		<option value="{{$i}}" {{(isset($_GET['course']) && $_GET['course']==$i) ? 'selected' : ''}}>{{$i}}</option>
	    	@endfor
	    </select>
	  </div>
	  <div class="form-group mx-sm-3 mb-2">
	    <label for="semester">Semester</label>
	    <select class="form-control" id="semester" name="semester">
	    	<option>Choose semester...</option>
	    	@for($i=1;$i<=2;$i++)
	    		<option value="{{$i}}" {{(isset($_GET['semester']) && $_GET['semester']==$i) ? 'selected' : ''}}>{{$i}}</option>
	    	@endfor
	    </select>
	  </div>
	  <div class="form-group mx-sm-3 mb-2">
	    <label for="group">Group</label>
	    <select class="form-control" id="group" name="group">
	    	<option>Choose group...</option>
	    	@foreach($professor->groups as $group)
	    		<option value="{{$group->id}}" {{(isset($_GET['group']) && $_GET['group']==$group->id) ? 'selected' : ''}}>{{$group->course.' '.$group->name.' ('.$group->faculty->name.')'}}</option>
	    	@endforeach
	    </select>
	  </div>
	  <div class="form-group mx-sm-3 mb-2">
	    <label for="subject">Subject</label>
	    <select class="form-control" id="subject" name="subject">
	    	<option>Choose subject...</option>
	    	@foreach($professor->subjects as $subject)
	    		<option value="{{$subject->id}}" {{(isset($_GET['subject']) && $_GET['subject']==$subject->id) ? 'selected' : ''}}>{{$subject->subject}}</option>
	    	@endforeach
	    </select>
	  </div>
	  <button type="submit" class="btn btn-primary mb-2 no-print">Show journal</button>
	</form>

	@if(isset($_GET['group']) && is_numeric($_GET['group']))
		<hr>
		<div style="overflow:auto">
			<form action="{{url('professor/save_mark')}}" method="POST" class="form_padding">
				{{ csrf_field() }}
				<table class="table table-bordered table-condensed table-hover" id="mark_table">
					<thead>
						<tr>
							<th>#</th>
							<th id="name">Name</th>
							<th>Late</th>
							<th>Ab- sent</th>
							<th>Pre- sent</th>
							@if($subj->homework)
								<th>Home- work</th>
							@endif
							@if($subj->bonus)
								<th>Assignment</th>
							@endif
							<th>Middle exam</th>
							<th>Final exam</th>
							<th>Total</th>
							<th>Mark</th>
							<th class="rate">Rate</th>
							<th class="rate_credit">Rate * Credit</th>
						</tr>
					</thead>
					<tbody>
						@foreach($students as $key => $student)
							<input type="hidden" name="course[{{$key}}]" value="{{$course}}">
							<input type="hidden" name="semester[{{$key}}]" value="{{$semester}}">
							<input type="hidden" name="group[{{$key}}]" value="{{$group_id}}">
							<input type="hidden" name="subject[{{$key}}]" value="{{$subject_id}}">
							<input type="hidden" name="student[{{$key}}]" value="{{$student->id}}">
							<tr>
								<td>{{$n=$key+1}}</td>
								<td>
									<strong>{{$student->lastname.' '.$student->firstname}}</strong> (
									{{$student->student_id}})
								</td>
								<td>
									<input type="text" name="late_count[{{$key}}]" class="mark_input" id="late_count{{$student->id}}" readonly>
								</td>
								<td>
									<input type="text" name="absent_count[{{$key}}]" id="absent_count{{$student->id}}" class="mark_input" readonly>
								</td>
								<td>
									<input type="text" name="present_count[{{$key}}]" id="present_count{{$student->id}}" class="mark_input" value="20"  max="20"readonly>
								</td>
								@if($subj->homework)
									<td>
										<input type="text" name="homework[{{$key}}]" id="homework{{$student->id}}" class="mark_input" max="20" readonly> 
									</td>
								@endif	
								@if($subj->bonus)
									<td>
										<input type="text" name="bonus[{{$key}}]" id="bonus{{$student->id}}" class="mark_input" max="20" readonly> 
									</td>
								@endif	
								<td>
									<input type="text" name="middle_exam[{{$key}}]" id="middle_exam{{$student->id}}" class="mark_input"  max="30" readonly>
								</td>
								<td>
									<input type="text" name="final_exam[{{$key}}]"  id="final_exam{{$student->id}}" class="mark_input" max="30" readonly>
								</td>
								<td>
									<input type="text" name="total[{{$key}}]" id="total{{$student->id}}" class="mark_input" readonly>
								</td>
								<td>
									<input type="text" name="mark[{{$key}}]" id="mark{{$student->id}}" class="mark_input" readonly>
								</td>
								<td class="rate">
									<input type="text" name="rate[{{$key}}]" id="rate{{$student->id}}" class="mark_input" readonly>
								</td>
								<td class="rate_credit">
									<input type="text" name="credit_x_rate[{{$key}}]" id="credit_x_rate{{$student->id}}" class="mark_input" readonly>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<input type="submit" name="submit" class="btn btn-success btn-block no-print" value="save">
			</form>
		</div>
	@endif
@endsection

@push('js')
	<script type="text/javascript">
		// for(var i=0; i<$('#mark_table tr:first-child td').length; i++){
		// 	$('#mark_table td:nth-child('+i+')').hover(
		// 		function(){
		// 			$('#mark_table td:nth-child('+i+')').css({'background':'#eee'});
		// 			$('#mark_table th:nth-child('+i+')').css({'background':'#eee'});
		// 		},
		// 		function(){
		// 			$('#mark_table td:nth-child('+i+')').css({'background':'inherit'});
		// 			$('#mark_table th:nth-child('+i+')').css({'background':'inherit'});
		// 		}
		// 	)
		// 	console.log(i);	
		// }

		$('#mark_table td:nth-child(3)').hover(
			function(){
				$('#mark_table td:nth-child(3)').css({'background':'#eee'});
				$('#mark_table th:nth-child(3)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(3)').css({'background':'inherit'});
				$('#mark_table th:nth-child(3)').css({'background':'inherit'});
			}
		)

		$('#mark_table td:nth-child(4)').hover(
			function(){
				$('#mark_table td:nth-child(4)').css({'background':'#eee'});
				$('#mark_table th:nth-child(4)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(4)').css({'background':'inherit'});
				$('#mark_table th:nth-child(4)').css({'background':'inherit'});
			}
		)
		
		$('#mark_table td:nth-child(5)').hover(
			function(){
				$('#mark_table td:nth-child(5)').css({'background':'#eee'});
				$('#mark_table th:nth-child(5)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(5)').css({'background':'inherit'});
				$('#mark_table th:nth-child(5)').css({'background':'inherit'});
			}
		)
		
		$('#mark_table td:nth-child(6)').hover(
			function(){
				$('#mark_table td:nth-child(6)').css({'background':'#eee'});
				$('#mark_table th:nth-child(6)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(6)').css({'background':'inherit'});
				$('#mark_table th:nth-child(6)').css({'background':'inherit'});
			}
		)
		
		$('#mark_table td:nth-child(7)').hover(
			function(){
				$('#mark_table td:nth-child(7)').css({'background':'#eee'});
				$('#mark_table th:nth-child(7)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(7)').css({'background':'inherit'});
				$('#mark_table th:nth-child(7)').css({'background':'inherit'});
			}
		)
		
		$('#mark_table td:nth-child(8)').hover(
			function(){
				$('#mark_table td:nth-child(8)').css({'background':'#eee'});
				$('#mark_table th:nth-child(8)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(8)').css({'background':'inherit'});
				$('#mark_table th:nth-child(8)').css({'background':'inherit'});
			}
		)
		
		$('#mark_table td:nth-child(9)').hover(
			function(){
				$('#mark_table td:nth-child(9)').css({'background':'#eee'});
				$('#mark_table th:nth-child(9)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(9)').css({'background':'inherit'});
				$('#mark_table th:nth-child(9)').css({'background':'inherit'});
			}
		)

		$('#mark_table td:nth-child(10)').hover(
			function(){
				$('#mark_table td:nth-child(10)').css({'background':'#eee'});
				$('#mark_table th:nth-child(10)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(10)').css({'background':'inherit'});
				$('#mark_table th:nth-child(10)').css({'background':'inherit'});
			}
		)

		$('#mark_table td:nth-child(11)').hover(
			function(){
				$('#mark_table td:nth-child(11)').css({'background':'#eee'});
				$('#mark_table th:nth-child(11)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(11)').css({'background':'inherit'});
				$('#mark_table th:nth-child(11)').css({'background':'inherit'});
			}
		)

		$('#mark_table td:nth-child(12)').hover(
			function(){
				$('#mark_table td:nth-child(12)').css({'background':'#eee'});
				$('#mark_table th:nth-child(12)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(12)').css({'background':'inherit'});
				$('#mark_table th:nth-child(12)').css({'background':'inherit'});
			}
		)

		$('#mark_table td:nth-child(13)').hover(
			function(){
				$('#mark_table td:nth-child(13)').css({'background':'#eee'});
				$('#mark_table th:nth-child(13)').css({'background':'#eee'});
			},
			function(){
				$('#mark_table td:nth-child(13)').css({'background':'inherit'});
				$('#mark_table th:nth-child(13)').css({'background':'inherit'});
			}
		)

	</script>	
	@foreach($students as $k => $st)
		<script type="text/javascript">
			@if($subj->homework)
				$(document).on('input','#middle_exam{{$st->id}}, #final_exam{{$st->id}}',function(){
					if($(this).val() > 30){
						$(this).val(30);
					}
				});
			@else
				$(document).on('input','#middle_exam{{$st->id}}, #final_exam{{$st->id}}',function(){
					if($(this).val() > 40){
						$(this).val(40);
					}
				});
			@endif	

			$(document).on('input','#homework{{$st->id}}',function(){
				if($(this).val() > 20){
					$(this).val(20);
				}
			});

			$(document).on('input','#absent_count{{$st->id}}',function(){
			    var value = Number($(this).val());
			    
			    if(value>=4){
			        $('#present_count{{$st->id}}').val(0);
			    }
			    else if(value>=3){
			        $('#present_count{{$st->id}}').val($('#present_count{{$st->id}}').val()-6);
			    }
			    else if(value>=2){
			        $('#present_count{{$st->id}}').val($('#present_count{{$st->id}}').val()-4);
			    }
			    else if(value>=1){
			        $('#present_count{{$st->id}}').val($('#present_count{{$st->id}}').val()-2);
			    }
			    else{
			        $('#present_count{{$st->id}}').val(20);
			    }

			});

			$(document).on('input','#late_count{{$st->id}}',function(){
			    var value = Number($(this).val());
			    if(value>=12){
			        $('#present_count{{$st->id}}').val(0);
			    }
			    else if(value>=9){
			        $('#present_count{{$st->id}}').val($('#present_count{{$st->id}}').val()-6);
			    }
			    else if(value>=6){
			        $('#present_count{{$st->id}}').val($('#present_count{{$st->id}}').val()-4);
			    }
			    else if(value>=3){
			        $('#present_count{{$st->id}}').val($('#present_count{{$st->id}}').val()-2);
			    }
			    else{
			        $('#present_count{{$st->id}}').val(20);
			    }
			 
			});

			$(document).on('input', '#late_count{{$st->id}}, #absent_count{{$st->id}}, #present_count{{$st->id}}, #homework{{$st->id}}, #bonus{{$st->id}}, #middle_exam{{$st->id}}, #final_exam{{$st->id}}', function(){
			    var present_count = Number($('#present_count{{$st->id}}').val());
			    @if($subj->homework)
			    	var homework = Number($('#homework{{$st->id}}').val());
			    @else
			    	var homework = 0;
			    @endif	
			    @if($subj->bonus)
			    	var bonus = Number($('#bonus{{$st->id}}').val());
			    @else
			    	var bonus = 0;
			    @endif	
			    var middle_exam = Number($('#middle_exam{{$st->id}}').val());
			    var final_exam = Number($('#final_exam{{$st->id}}').val());

			    var total = present_count+homework+bonus+middle_exam+final_exam;
			    
			    if(total>=95 && total<=100){
			        $('#mark{{$st->id}}').val('A+');
			        $('#rate{{$st->id}}').val('4.5');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});
			    }
			    else if(total>=90 && total<=94){
			        $('#mark{{$st->id}}').val('A');
			        $('#rate{{$st->id}}').val('4.0');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});
			    }
			    else if(total>=85 && total<=89){
			        $('#mark{{$st->id}}').val('B+');
			        $('#rate{{$st->id}}').val('3.5');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});

			    }
			    else if(total>=80 && total<=84){
			        $('#mark{{$st->id}}').val('B');
			        $('#rate{{$st->id}}').val('3.0');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});

			    }
			    else if(total>=75 && total<=79){
			        $('#mark{{$st->id}}').val('C+');
			        $('#rate{{$st->id}}').val('2.5');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});

			    }
			    else if(total>=70 && total<=74){
			        $('#mark{{$st->id}}').val('C');
			        $('#rate{{$st->id}}').val('2.0');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});

			    }
			    else if(total>=65 && total<=69){
			        $('#mark{{$st->id}}').val('D+');
			        $('#rate{{$st->id}}').val('1.5');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});

			    }
			    else if(total>=60 && total<=64){
			        $('#mark{{$st->id}}').val('D');
			        $('#rate{{$st->id}}').val('1.0');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});

			    }
			    else if(total<=59){
			        $('#mark{{$st->id}}').val('F');
			        $('#rate{{$st->id}}').val('0.0');
			        $('#credit_x_rate{{$st->id}}').val($('#rate{{$st->id}}').val()*{{$subj->credit}});
			    }
			    $('#total{{$st->id}}').val(total);
			});
		</script>

	@endforeach
@endpush
