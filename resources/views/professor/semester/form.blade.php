<div class="form-group {{ $errors->has('course') ? 'has-error' : ''}}">
    <label for="course" class="control-label">{{ 'Course' }}</label>
    <select class="form-control" name="course" type="text" id="course" value="{{ isset($semester->course) ? $semester->course : ''}}" >
        <option>Choose course...</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    </select>    
    {!! $errors->first('course', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('semester') ? 'has-error' : ''}}">
    <label for="semester" class="control-label">{{ 'Semester' }}</label>
    <select class="form-control" name="semester" type="text" id="semester" value="{{ isset($semester->semester) ? $semester->semester : ''}}" >
        <option>Choose semester...</option>
        <option value="1">1</option>
        <option value="2">2</option>
    </select>    
    {!! $errors->first('semester', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('subject_id') ? 'has-error' : ''}}">
    <label for="subject_id" class="control-label">{{ 'Subject Id' }}</label>
    <select class="form-control" name="subject_id" type="text" id="subject_id" value="{{ isset($semester->subject_id) ? $semester->subject_id : ''}}" >
        <option>Choose subject...</option>
        @foreach(\App\Professor::find(Auth::user()->type_id)->subjects as $subject)
            <option value="{{$subject->id}}">{{$subject->subject}}</option>
        @endforeach
    </select>    
    {!! $errors->first('subject_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('group_id') ? 'has-error' : ''}}">
    <label for="group_id" class="control-label">{{ 'Group' }}</label>
    <select class="form-control" name="group_id" type="text" id="group_id" value="{{ isset($semester->group_id) ? $semester->group_id : ''}}" >
        <option>Choose group...</option>
        @foreach(\App\Group::all() as $group)
            <option value="{{$group->id}}">{{$group->name.'('.$group->faculty->name.')'}}</option>
        @endforeach
    </select>    
    {!! $errors->first('group_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('application_id') ? 'has-error' : ''}}">
    <label for="application_id" class="control-label">{{ 'Student' }}</label>
    <select class="form-control" name="application_id" type="text" id="application_id" value="{{ isset($semester->application_id) ? $semester->application_id : ''}}" >
    </select>    
    {!! $errors->first('application_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('professor_id') ? 'has-error' : ''}}">
    {{-- <label for="professor_id" class="control-label">{{ 'Professor Id' }}</label> --}}
    <input class="form-control" name="professor_id" type="hidden" id="professor_id" value="{{ Auth::user()->type_id }}" >
    {!! $errors->first('professor_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('late_count') ? 'has-error' : ''}}">
    <label for="late_count" class="control-label">{{ 'Late Count' }}</label>
    <input class="form-control" name="late_count" type="text" id="late_count" value="{{ isset($semester->late_count) ? $semester->late_count : ''}}" >
    {!! $errors->first('late_count', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('absent_count') ? 'has-error' : ''}}">
    <label for="absent_count" class="control-label">{{ 'Absent Count' }}</label>
    <input class="form-control" name="absent_count" type="text" id="absent_count" value="{{ isset($semester->absent_count) ? $semester->absent_count : ''}}" >
    {!! $errors->first('absent_count', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('present_count') ? 'has-error' : ''}}">
    <label for="present_count" class="control-label">{{ 'Present Count' }}</label>
    <input class="form-control" name="present_count" type="text" id="present_count" value="20" readonly>
    {!! $errors->first('present_count', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('homework') ? 'has-error' : ''}}">
    <label for="homework" class="control-label">{{ 'Homework' }}</label>
    <input class="form-control" name="homework" type="text" id="homework" value="{{ isset($semester->homework) ? $semester->homework : ''}}" >
    {!! $errors->first('homework', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('middle_exam') ? 'has-error' : ''}}">
    <label for="middle_exam" class="control-label">{{ 'Middle Exam' }}</label>
    <input class="form-control" name="middle_exam" type="text" id="middle_exam" value="{{ isset($semester->middle_exam) ? $semester->middle_exam : ''}}" >
    {!! $errors->first('middle_exam', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('final_exam') ? 'has-error' : ''}}">
    <label for="final_exam" class="control-label">{{ 'Final Exam' }}</label>
    <input class="form-control" name="final_exam" type="text" id="final_exam" value="{{ isset($semester->final_exam) ? $semester->final_exam : ''}}" >
    {!! $errors->first('final_exam', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('total') ? 'has-error' : ''}}">
    <label for="total" class="control-label">{{ 'Total' }}</label>
    <input class="form-control" name="total" type="text" id="total" value="{{ isset($semester->total) ? $semester->total : ''}}" >
    {!! $errors->first('total', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('mark') ? 'has-error' : ''}}">
    <label for="mark" class="control-label">{{ 'Mark' }}</label>
    <input class="form-control" name="mark" type="text" id="mark" value="{{ isset($semester->mark) ? $semester->mark : ''}}" >
    {!! $errors->first('mark', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
