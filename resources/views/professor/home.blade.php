@extends('layouts.professor.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 align="center">Hello, you are logged in as  <span style="color:red"> {{ $professor->firstname.' '.$professor->lastname }}</span></h4>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <tr>
                            <th>Name: </th><td>{{$professor->firstname.' '.$professor->lastname}}</td>
                        </tr>
                        <tr>
                            <th>Phone: </th><td>{{$professor->phone}}</td>
                        </tr>
                        <tr>
                            <th>Email: </th><td>{{$professor->email}}</td>
                        </tr>
                        <tr>
                            <th>Graduated: </th><td>{{$professor->university_name}}</td>
                        </tr>
                        <tr>
                            <th>Grade: </th><td>{{$professor->grade}}</td>
                        </tr>
                        <tr>
                            <th>Subjects: </th>
                            <td>
                                <ul>
                                    @foreach($professor->subjects as $subject)
                                        <li>{{$subject->subject}}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <th>Groups: </th>
                            <td>
                                <ul>
                                    @foreach($professor->groups as $group)
                                        <li>{{$group->name.' ('.$group->faculty->name.')'}}</li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
