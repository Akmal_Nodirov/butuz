<div class="form-group {{ $errors->has('firstname') ? 'has-error' : ''}}">
    <label for="firstname" class="control-label">{{ 'Firstname' }}</label>
    <input class="form-control" name="firstname" type="text" id="firstname" value="{{ isset($professor->firstname) ? $professor->firstname : ''}}" >
    {!! $errors->first('firstname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lastname') ? 'has-error' : ''}}">
    <label for="lastname" class="control-label">{{ 'Lastname' }}</label>
    <input class="form-control" name="lastname" type="text" id="lastname" value="{{ isset($professor->lastname) ? $professor->lastname : ''}}" >
    {!! $errors->first('lastname', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
    <label for="phone" class="control-label">{{ 'Phone' }}</label>
    <input class="form-control" name="phone" type="text" id="phone" value="{{ isset($professor->phone) ? $professor->phone : ''}}" >
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($professor->email) ? $professor->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('university_name') ? 'has-error' : ''}}">
    <label for="university_name" class="control-label">{{ 'University Name' }}</label>
    <input class="form-control" name="university_name" type="text" id="university_name" value="{{ isset($professor->university_name) ? $professor->university_name : ''}}" >
    {!! $errors->first('university_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('grade') ? 'has-error' : ''}}">
    <label for="grade" class="control-label">{{ 'Grade' }}</label>
    <input class="form-control" name="grade" type="text" id="grade" value="{{ isset($professor->grade) ? $professor->grade : ''}}" >
    {!! $errors->first('grade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group form-float">
    <label class="form-label">Groups</label>
    <select name="groups[]" id="group" class="form-control selectpicker" data-live-search="true" multiple>
        @foreach(\App\Group::all() as $group)
            <option value="{{ $group->id }}" 
                @if(!Request::is('professor/professor/create'))
                    @foreach($professor->groups as $profGroup)
                        {{ $group->id == $profGroup->id ? 'selected' : '' }}
                    @endforeach
                @endif
            > 
                {{ $group->name.' ('.$group->faculty->name.')' }}
            </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
@push('js')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.selectpicker').select2();
        });
    </script>
@endpush