@extends('layouts.professor.app')

@section('content')
    <div class="container">
        <div class="row">
            {{-- @include('admin.sidebar') --}}

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Professor {{ $professor->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/professor/professor') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/professor/professor/' . $professor->id . '/edit') }}" title="Edit Professor"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('dean/professor' . '/' . $professor->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Professor" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    {{-- <tr>
                                        <th>ID</th><td>{{ $professor->id }}</td>
                                    </tr> --}}
                                    <tr>
                                        <th> Firstname </th>
                                        <td> {{ $professor->firstname }} </td>
                                    </tr>
                                    <tr>
                                        <th> Lastname </th>
                                        <td> {{ $professor->lastname }} </td>
                                    </tr>
                                    <tr>
                                        <th> University Name </th>
                                        <td> {{ $professor->university_name }} </td>
                                    </tr>
                                    <tr>
                                        <th>Subjects</th>
                                        <td>
                                            <ul>
                                                @foreach($professor->subjects as $subject)
                                                    <li>
                                                        <a href="{{url('dean/subject/'.$subject->id)}}">
                                                            {{ $subject->subject }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Groups</th>
                                        <td>
                                            <ul>
                                                @foreach($professor->groups as $group)
                                                    <li>
                                                        <a href="{{url('dean/group/'.$group->id)}}">
                                                            {{ $group->name.' ('.$group->faculty->name.')' }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
