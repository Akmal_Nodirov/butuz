<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Semester extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'semester';

    /**
     * The database primary key value.
     *
     * @var string
     */
    //protected $primaryKey = 'id';
    protected $primaryKey = ['course', 'group_id', 'semester', 'subject_id', 'application_id'];
    public $incrementing = false;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['course', 'semester', 'subject_id', 'group_id', 'application_id', 'professor_id', 'late_count', 'absent_count', 'present_count', 'homework', 'middle_exam', 'final_exam', 'bonus', 'total', 'mark', 'rate', 'credit_x_rate'];

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function application()
    {
        return $this->belongsTo('App\Application');
    }

    public function professor()
    {
        return $this->belongsTo('App\Professor');
    }

    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected static function getAverageMarkBySemestr($semester, $mark)
    {
        $totals = null;
        $credits = null;
        $all_students = [];

        foreach ($semester as $k => $sem) {
            $totals += ($sem->total * $sem->subject->credit);
            $credits += $sem->subject->credit;
            $average = $credits != 0 ? round($totals / $credits) : 0;
            if ($average >= 95 && $average <= 100 && $mark == "A+") {
                $all_students[$k] = $sem;
            } elseif ($average >= 90 && $average <= 94 && $mark == "A") {
                $all_students[$k] = $sem;
            } elseif ($average >= 85 && $average <= 89 && $mark == "B+") {
                $all_students[$k] = $sem;
            } elseif ($average >= 80 && $average <= 84 && $mark == "B") {
                $all_students[$k] = $sem;
            } elseif ($average >= 75 && $average <= 79 && $mark == "C+") {
                $all_students[$k] = $sem;
            } elseif ($average >= 70 && $average <= 74 && $mark == "C") {
                $all_students[$k] = $sem;
            } elseif ($average >= 65 && $average <= 69 && $mark == "D+") {
                $all_students[$k] = $sem;
            } elseif ($average >= 60 && $average <= 64 && $mark == "D") {
                $all_students[$k] = $sem;
            } elseif ($average > 0 && $average <= 59 && $mark == "F") {
                $all_students[$k] = $sem;
            }
        }

        return $all_students;
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if (!is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}
