<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'about';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['logo', 'tel1', 'tel2', 'slogan_uz', 'slogan_ru', 'slogan_en', 'slogan_kr', 'facebook', 'telegram', 'youtube', 'address_uz', 'address_ru', 'address_en', 'address_kr', 'email', 'location'];

    
}
