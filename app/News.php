<?php

namespace App;

class News extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['image', 'title_uz', 'title_ru', 'title_en', 'title_kr', 'content_uz', 'content_ru', 'content_en', 'content_kr', 'published'];

    
}
