<?php

namespace App;


class Administration extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'administration';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['image', 'fullname_uz', 'fullname_ru', 'fullname_en', 'fullname_kr', 'phone', 'position_id', 'published'];

    public function position(){
        return $this->belongsTo(Position::class);
    }    
}
