<?php

namespace App;


class Page extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'page';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title_uz', 'title_ru', 'title_en', 'title_kr', 'content_uz', 'content_ru', 'content_en', 'content_kr', 'type', 'published'];

    public function scopeAbout($query){
        return $query->where('type',false);
    }

    public function scopeEducation($query){
        return $query->where('type',true);
    }

}
