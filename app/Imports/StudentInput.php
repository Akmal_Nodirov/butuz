<?php

namespace App\Imports;

use App\Application;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentInput implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $str = str_replace('/',' ',$row[10]);
        $spaceCount = substr_count($str, " ");
        $letterIndx = 0;

        // count number of spaces and then loop
        for($i=0; $i<=$spaceCount; $i++) {

          // get space positions
          $spaceIndx = strpos($str, " ", $letterIndx);
          
          // assign word by specifying start position and length
          if ($spaceIndx == 0) {
            $word = substr($str, $letterIndx);
          } else {
            $word = substr($str, $letterIndx, $spaceIndx - $letterIndx);
          }
          
          // push word into array
          $myArray[] = $word;
          
          // get first letter after space
          $letterIndx = $spaceIndx + 1;
        }


        // reverse the array
        $reverse = array_reverse($myArray);

        // echo it out
        $natija = "";
        foreach($reverse as $rev) {
          if($rev != ""){
            $natija .= $rev."-";
          }
          else{
            $natija = null;
          }
        }

        // students tablega
        // return new Application([
        //    'student_id'           => $row[0],
        //    'order_num'            => $row[7],
        //    'firstname'            => $row[2],
        //    'lastname'             => $row[1], 
        //    'apply_no'             => $row[8],
        //    'photo'                => $row[3],
        //    'faculty_id'           => $row[4],
        //    'group_id'             => $row[5],
        //    'examination_language' => $row[9],
        //    'status'               => 1,
        //    'date_of_birth'        => $natija,
        //    'nationality'          => $row[11],
        //    'passport'             => $row[12],
        //    'gender'               => $row[13],
        //    'phone_number'         => $row[14],
        //    'email'                => $row[15],
        //    'address'              => $row[16],
        // ]);

        return new Application([
           'student_id'           => $row[0],
           'order_num'            => $row[7],
           'firstname'            => $row[2],
           'lastname'             => $row[1], 
           'apply_no'             => $row[8],
           'photo'                => $row[3],
           'faculty_id'           => $row[4],
           'group_id'             => $row[5],
           'examination_language' => $row[9],
           'status'               => 1,
           'date_of_birth'        => $natija,
           'nationality'          => $row[11],
           'passport'             => $row[12],
           'gender'               => $row[13],
           'phone_number'         => $row[14],
           'email'                => $row[15],
           'address'              => $row[16],
        ]);
    }
}
