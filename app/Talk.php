<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Talk extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'talk';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['score', 'comment', 'criteria1', 'criteria2', 'criteria3', 'criteria4', 'application_id', 'user_id'];

    public function faculty(){
        return $this->belongsTo('App\Application');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    
}
