<?php

namespace App;

use DB;

class Model extends \Illuminate\Database\Eloquent\Model{

	public function scopePublished($query){
        return $query->where('published',true);
    }

    public function archive(){
    	return DB::table($this->table)
            ->select(DB::raw('YEAR(created_at) year, COUNT(*) count'))
            ->where('published',true)
            ->groupBy('year')
            ->orderBy('year', 'desc')
            ->get();
    }
}