<?php

namespace App;

class Position extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'position';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['position_uz', 'position_ru', 'position_en', 'position_kr', 'published'];

    public function administrations(){
        return $this->hasMany(Administration::class);
    }  
}
