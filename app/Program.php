<?php

namespace App;

class Program extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'programs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['image', 'title_uz', 'title_ru', 'title_en', 'title_kr', 'description_uz', 'description_ru', 'description_en', 'description_kr', 'published'];

    
}
