<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'professors';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'phone', 'email', 'university_name', 'grade', 'type', 'faculty_id'];

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    public function subjects(){
        return $this->belongsToMany('App\Subject');
    }

    public function semesters(){
        return $this->hasMany('App\Semester');
    }

    public function faculty(){
        return $this->belongsTo('App\Faculty', 'faculty_id', 'id');
    }
}
