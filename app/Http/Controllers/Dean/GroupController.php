<?php

namespace App\Http\Controllers\Dean;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $group = Group::with('faculty')
                ->where('course', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('faculty_id', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $group = Group::paginate($perPage);
        }

        return view('dean.group.index', compact('group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('dean.group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'course' => 'required',
            'name' => 'required',
            'faculty_id' => 'required'
        ]);
        
        $requestData = $request->all();
        
        Group::create($requestData);

        return redirect('dean/group')->with('flash_message', 'Group added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $group = Group::findOrFail($id);

        return view('dean.group.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $group = Group::findOrFail($id);

        return view('dean.group.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $group = Group::findOrFail($id);
        $group->update($requestData);

        return redirect('dean/group')->with('flash_message', 'Group updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Group::destroy($id);

        return redirect('dean/group')->with('flash_message', 'Group deleted!');
    }

    /**
     * Facultetga tegishli guruhlarni chiqarish
     */
    public function getgroup(Request $request){
        
        $groups = Group::with('faculty')
                    ->where('faculty_id',$request->faculty_id)
                    ->get();
        $g = "";
        foreach($groups as $key => $group){
            $selected = '';
            if(isset($request->group_id)){
                $selected = $request->group_id == $group->id ? 'selected' : '';
            }

            $g .= "<option value='".$group->id."' $selected >".$group->name." (".$group->faculty->name.")</option>";
        }            
        return $g;
    }
}
