<?php

namespace App\Http\Controllers\Dean;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Faculty;
use App\Group;
use App\Semester;
use App\Subject;
use App\Application;
use Illuminate\Http\Request;

class DashboardController extends Controller{

	public function index(Request $request){
		return view('dean.home');
	}

	/**
	 * Talaba natijalarini ko`rish
	 */
	public function student_results(Request $request, $student_id){
		$groups = Group::all();
		$subjects = Subject::all();
		$student = Application::findOrFail($student_id);
		$semester = null;
		if(is_numeric($request->input('course')) && is_numeric($request->input('semester'))){
			$semester = Semester::where([
				['course','=',$request->input('course')],
				['semester','=',$request->input('semester')],
				['application_id','=',$student_id]
			])
			->get();
		}
		return view('dean.dashboard.student_results',compact('groups','subjects','semester','student'));
	}

	/**
	 * Barcha talabalar natijalarini ko`rish
	 */
	public function students_results(Request $request){
		$groups = Group::all();
		$subjects = Subject::all();
		$students = Application::where('group_id',$request->input('group'))->get();
		$semester = null;
		if(is_numeric($request->input('course')) && is_numeric($request->input('semester'))){
				$semester = Semester::where([
					['course','=',$request->input('course')],
					['semester','=',$request->input('semester')],
					// ['application_id','=',$student_id]
				])
				->get();
		}
		return view('dean.dashboard.students_results',compact('groups','subjects','semester','students'));
	}
}