<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\{
    About,
    Slider,
    Program,
    Page,
    Position,
    Administration,
    User,
    Programs,
    Application,
    Announcement,
    News,
    Faq,
    Message,
    Event,
    Image,
    Faculty
};
use Illuminate\Support\Facades\Hash;
use DB;
use Session;
use Intervention\Image\Facades\Image as Im;
use Storage;

// use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Session::has('lang'))
            return view('choose_lang');

        $slider = Slider::published()->latest()->get();
        $about = Page::about()->where('id', 1)->published()->first();
        $programs = Program::published()->latest()->get();
        $announcements = Announcement::published()->latest()->limit(4)->get();
        $news = News::published()->latest()->limit(4)->get();
        $faq = Faq::published()->latest()->limit(4)->get();

        return view('index', compact('slider', 'about', 'programs', 'announcements', 'news', 'faq'));
    }

    /**
     * Homepage if logged in
     */
    public function home_page()
    {
        return $this->check_auth();
    }

    /**
     * About us page
     */
    public function about($id)
    {
        $about = Page::about()->where('id', $id)->published()->first();
        $about_pages = Page::about()->published()->get();
        return view('about', compact('about', 'about_pages', 'id'));
    }

    /**
     * Education page
     */
    public function education($id)
    {
        $education = Page::education()->where('id', $id)->published()->first();
        return view('education', compact('education'));
    }

    /**
     * Get a program by id
     */
    public function program($id)
    {
        $program = Program::findOrFail($id);
        return view('program', compact('program'));
    }

    /**
     * Administration page in about menu
     */
    public function administration()
    {
        $about_pages = Page::about()->published()->get();
        $positions = Position::published()->get();
        return view('administration', compact('about_pages', 'positions'));
    }

    /**
     * Fakultetlar sahifasi
     */
    public function faculties()
    {
        $programs = Program::published()->get();
        return view('faculties', compact('programs'));
    }

    /**
     * Get announcement by id
     */
    public function announcement($id, Announcement $announcement)
    {
        $announcement = Announcement::where('id', $id)->published()->first();
        $created_at = new \datetime($announcement->created_at);
        $archive = $announcement->archive();

        return view('announcement', compact('announcement', 'created_at', 'archive'));
    }

    /**
     * Arxiv
     */
    public function archive($type, $year)
    {
        $next_year = $year + 1;

        $table = ($type == 'event') ? 'events' : $type;

        $items = DB::table($table)
            ->whereBetween(
                'created_at', [$year . '-01-01 00:00:00', $next_year . '-01-01 00:00:00',]
            )
            ->where('published', true)
            ->latest()->paginate(5);

        $className = 'App\\' . ucfirst($type);
        $object = new $className();
        $archive = $object->archive();

        return view('archive', compact('year', 'type', 'items', 'archive'));
    }

    /**
     * All announcements
     */
    public function announcements(Announcement $announcement)
    {
        $announcements = Announcement::published()->latest()->paginate(5);
        $archive = $announcement->archive();

        return view('announcements', compact('announcements', 'archive'));
    }

    /**
     * Get news by id
     */
    public function news($id, News $news)
    {
        $news = News::where('id', $id)->published()->first();
        $created_at = new \datetime($news->created_at);
        $archive = $news->archive();

        return view('news', compact('news', 'created_at', 'archive'));
    }

    /**
     * Frequently Asked Questions (FAQ)
     */
    public function faq()
    {
        $faq = Faq::published()->latest()->get();

        return view('faq', compact('faq'));
    }

    /**
     * All news
     */
    public function all_news(News $newss)
    {
        $news = News::published()->latest()->paginate(5);
        $archive = $newss->archive();

        return view('all_news', compact('news', 'archive'));
    }

    /**
     * Contact page
     */
    public function contact()
    {
        $about = About::findOrFail(1);

        return view('contact', compact('about'));
    }

    /**
     * Sending message
     */
    public function send_message(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $message = new Message;
        $message->name = $request->name;
        $message->phone = $request->phone;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->seen = 0;
        $message->save();

        Session::flash('message_sent', 'Message was sent!');

        $about = About::findOrFail(1);

        $to = $about->email;
        $subject = 'Message sent via website. Subject:' . $request->subject;
        $txt = $request->message;
        $headers = "From: $request->email" . "\r\n" .
            "CC: $request->email";

        mail($to, $subject, $txt, $headers);

        return redirect('/contact');
    }

    /**
     * Events and gallery
     */
    public function events(Event $event)
    {
        $events = Event::published()->latest()->paginate(5);
        $archive = $event->archive();

        return view('events', compact('events', 'archive'));
    }

    /**
     * Gallery of the event
     */
    public function event($id)
    {
        $event = Event::where('id', $id)->published()->first();
        $images = Image::where('event_id', $id)->published()->latest()->get();

        return view('gallery', compact('event', 'images'));
    }

    /**
     * Choose website or application form
     */
    public function choose_application()
    {
        return view('choose_application');
    }

    /**
     * Agreement
     */
    public function agreement()
    {
        return view('agreement');
    }

    /**
     * Acceptance aggreement
     */
    public function i_agree(Request $request)
    {
        if ($request->check == 'on')
            Session::put('agreement_check', 'checked');
        return redirect('/application');
    }

    /**
     * Application sahifasi
     */
    public function application()
    {
        if (!Session::has('agreement_check'))
            return redirect('/agreement');

        $faculties = Faculty::all();

        return view('expire', compact('faculties'));
    }

    /**
     * Sending application
     */
    public function send_application(Request $request)
    {
        if (Auth::check()) {
            Auth::logout();
        }

        $this->validate($request, [
            'faculty_id' => 'required',
            'exam_lang' => 'required',
            'lastname' => 'required',
            'firstname' => 'required',
            'date_of_birth' => 'required|date_format:Y-m-d',
            'nationality' => 'required',
            'passport_number' => 'required',
            'gender' => 'required',
            'phone_number' => 'required|unique:applications|digits:9',
            'email' => 'required',
            'address' => 'required',
            'photo' => 'mimes: pdf',
            'diploma_copy' => 'mimes: pdf',
            'passport_copy' => 'mimes: pdf',
            'inn_copy' => 'mimes:pdf',
            'highschool' => 'required',
            'grad_year' => 'required|numeric',
            'major' => 'required',
            'parent_phone' => 'required|digits:9',

        ]);

        $photo = $request->file('photo');
        $diploma_copy = $request->file('diploma_copy');
        $passport_copy = $request->file('passport_copy');
        $inn_copy = $request->file('inn_copy');

        if (isset($photo)) {
            // make unipue name for image
            $photoName = 'getphoto' . time() . '.' . $photo->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            // $apply_photo = Im::make($photo)->save($photoName,100);
            Storage::disk('public')->put('applications/' . $photoName, $photo);
            $photoName = $photoName . '/' . $photo;
        } else {
            $photoName = "";
        }

        if (isset($diploma_copy)) {
            // make unipue name for image
            $diplomaName = 'getdiploma' . time() . '.' . $diploma_copy->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            // $apply_diploma = Im::make($diploma_copy)->save($diplomaName,100);
            Storage::disk('public')->put('applications/' . $diplomaName, $diploma_copy);
            $diplomaName = $dipplomaName . '/' . $diploma_copy;
        } else {
            $diplomaName = "";
        }

        if (isset($passport_copy)) {
            // make unipue name for image
            $passportName = 'getpassport' . time() . '.' . $passport_copy->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            // $apply_passport = Im::make($passport_copy)->save($passportName,100);
            Storage::disk('public')->put('applications/' . $passportName, $passport_copy);
            $passportName = $passportName . '/' . $passport_copy;
        } else {
            $passportName = "";
        }

        if (isset($inn_copy)) {
            // dd($inn_copy);
            // make unipue name for image
            $innName = 'getinn' . time() . '.' . $inn_copy->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            // $apply_inn = Im::make($inn_copy)->save($innName,100);
            Storage::disk('public')->put('applications/' . $innName, $inn_copy);
            $innName = $innName . '/' . $inn_copy;
        } else {
            $innName = "";
        }

        $application_id = Application::latest()->limit(1)->first()->id + 1;

        $application = new Application;
        $application->firstname = $request->firstname;
        $application->lastname = $request->lastname;
        $application->faculty_id = $request->faculty_id;
        $application->examination_language = $request->exam_lang;
        $application->apply_no = date('Y') . $request->faculty_id . $request->exam_lang . $application_id;
        $application->status = 0;
        $application->date_of_birth = $request->date_of_birth;
        $application->nationality = $request->nationality;
        $application->passport = str_replace(" ", "", $request->passport_number);
        $application->gender = $request->gender;
        $application->phone_number = $request->phone_number;
        $application->email = $request->email;
        $application->address = $request->address;
        $application->photo = $photoName;
        $application->diploma_copy = $diplomaName;
        $application->passport_copy = $passportName;
        $application->inn_copy = $innName;

        // $application->work_place = $request->gender;
        // $application->work_phone_number = $request->gender;
        $application->high_school_name = $request->highschool;
        $application->high_school_graduation_year = $request->grad_year;
        $application->high_school_major = $request->major;
        $application->university_name = $request->university_name;
        $application->university_graduation_year = $request->university_graduation_year;
        $application->university_major = $request->university_major;
        $application->parent_name = $request->parent_name;
        $application->relationship = $request->relationship;
        $application->parent_phone_number = $request->parent_phone;
        $application->parent_email = $request->parent_email;
        $application->date1 = $request->input('date');
        $application->score1 = $request->score;
        $application->grade1 = $request->grade;
        $application->organization_of_issue1 = $request->organization_of_issue;
        $application->examination1 = $request->examination;
        $application->date2 = $request->input('date2');
        $application->score2 = $request->score2;
        $application->grade2 = $request->grade2;
        $application->organization_of_issue2 = $request->organization_of_issue2;
        $application->examination2 = $request->examination2;
        $application->date3 = $request->input('date3');
        $application->score3 = $request->score3;
        $application->grade3 = $request->grade3;
        $application->organization_of_issue3 = $request->organization_of_issue3;
        $application->examination3 = $request->examination3;
        $application->changed = false;
        $application->save();

        $user = User::create([
            'type' => 'stu',
            'type_id' => $application_id,
            'phone' => $application->phone_number,
            'email' => $application->email,
            'password' => Hash::make($application->passport),
        ]);

        Session::flash('application_saved', 'Application was saved!');

        Auth::login($user);

        return redirect('/student/dashboard');
    }

    /**
     * Update application data
     */
    public function update_application($id, Request $request)
    {

        $this->validate($request, [
            'faculty_id' => 'required',
            'exam_lang' => 'required',
            'lastname' => 'required',
            'firstname' => 'required',
            'date_of_birth' => 'required',
            'nationality' => 'required',
            'passport_number' => 'required',
            'gender' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'address' => 'required',
            'photo' => 'mimes: pdf',
            'diploma_copy' => 'mimes: pdf',
            'passport_copy' => 'mimes: pdf',
            'inn_copy' => 'mimes: pdf',
            'highschool' => 'required',
            'grad_year' => 'required|numeric',
            'major' => 'required',
            'parent_phone' => 'required',

        ]);

        $application = Application::findOrFail($id);

        $photo = $request->file('photo');
        $diploma_copy = $request->file('diploma_copy');
        $passport_copy = $request->file('passport_copy');
        $inn_copy = $request->file('inn_copy');

        if (isset($photo)) {
            // make unipue name for image
            $photoName = 'getphoto' . time() . '.' . $photo->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            // $apply_photo = Im::make($photo)->save($photoName,100);
            Storage::disk('public')->put('applications/' . $photoName, $photo);
            $photoName = $photoName . '/' . $photo;
        } else {
            $photoName = $application->photo;
        }

        if (isset($diploma_copy)) {
            // make unipue name for image
            $diplomaName = 'getdiploma' . time() . '.' . $diploma_copy->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            $apply_diploma = Im::make($diploma_copy)->save($diplomaName, 100);
            Storage::disk('public')->put('applications/' . $diplomaName, $diploma_copy);
            $diplomaName = $dipplomaName . '/' . $diploma_copy;

        } else {
            $diplomaName = $application->diploma_copy;
        }

        if (isset($passport_copy)) {
            // make unipue name for image
            $passportName = 'getpassport' . time() . '.' . $passport_copy->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            $apply_passport = Im::make($passport_copy)->save($passportName, 100);
            Storage::disk('public')->put('applications/' . $passportName, $passport_copy);
            $passportName = $passportName . '/' . $passport_copy;

        } else {
            $passportName = $application->passport_copy;
        }

        if (isset($inn_copy)) {
            // make unipue name for image
            $innName = 'getinn' . time() . '.' . $inn_copy->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('applications')) {
                Storage::disk('public')->makeDirectory('applications');
            }

            //$apply_inn = Im::make($inn_copy)->save($innName,100);
            Storage::disk('public')->put('applications/' . $innName, $inn_copy);
            $innName = $innName . '/' . $inn_copy;

        } else {
            $innName = $application->inn_copy;
        }

        // $application_id = Application::latest()->limit(1)->first()->id + 1;

        // $application = new Application;
        $application->firstname = $request->firstname;
        $application->lastname = $request->lastname;
        $application->faculty_id = $request->faculty_id;
        $application->examination_language = $request->exam_lang;
        $application->apply_no = date('Y') . $request->faculty_id . $request->exam_lang . $id;
        $application->status = 0;
        $application->date_of_birth = $request->date_of_birth;
        $application->nationality = $request->nationality;
        $application->passport = str_replace(" ", "", $request->passport_number);
        $application->gender = $request->gender;
        $application->phone_number = $request->phone_number;
        $application->email = $request->email;
        $application->address = $request->address;
        $application->photo = $photoName;
        $application->diploma_copy = $diplomaName;
        $application->passport_copy = $passportName;
        $application->inn_copy = $innName;

        // $application->work_place = $request->gender;
        // $application->work_phone_number = $request->gender;
        $application->high_school_name = $request->highschool;
        $application->high_school_graduation_year = $request->grad_year;
        $application->high_school_major = $request->major;
        $application->university_name = $request->university_name;
        $application->university_graduation_year = $request->university_graduation_year;
        $application->university_major = $request->university_major;
        $application->parent_name = $request->parent_name;
        $application->relationship = $request->relationship;
        $application->parent_phone_number = $request->parent_phone;
        $application->parent_email = $request->parent_email;
        $application->date1 = $request->input('date');
        $application->score1 = $request->score;
        $application->grade1 = $request->grade;
        $application->organization_of_issue1 = $request->organization_of_issue;
        $application->examination1 = $request->examination;
        $application->date2 = $request->input('date2');
        $application->score2 = $request->score2;
        $application->grade2 = $request->grade2;
        $application->organization_of_issue2 = $request->organization_of_issue2;
        $application->examination2 = $request->examination2;
        $application->date3 = $request->input('date3');
        $application->score3 = $request->score3;
        $application->grade3 = $request->grade3;
        $application->organization_of_issue3 = $request->organization_of_issue3;
        $application->examination3 = $request->examination3;
        $application->changed = true;
        $application->save();

        $user = User::findOrFail(Auth::user()->id);
        // $user->type = 'stu';
        // $user->type_id = $application->id;
        $user->phone = $application->phone_number;
        $user->email = $application->email;
        $user->password = Hash::make($application->passport);
        $user->save();

        Session::flash('application_updated', 'Application was updated!');

        Auth::login($user);

        return redirect('/student/dashboard');
    }

    /**
     * Rights and duties page
     */
    public function rights_and_duties()
    {
        $about = About::findOrFail(1);
        return view('rights_and_duties', compact('about'));
    }

    /**
     * Foydalanuvchi turini tekshirish
     */
    public function check_auth()
    {
        if (Auth::user()->type == 'dean') {
            return redirect('dean/dashboard');
        } elseif (Auth::user()->type == 'prof') {
            return redirect('professor/dashboard');
        } elseif (Auth::user()->type == 'stu') {
            return redirect('student/dashboard');
        } elseif (Auth::user()->type == 'admin') {
            return redirect('admin/dashboard');
        }
    }

    /**
     * Login pagega o`tkazish
     */
    public function to_login()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect('/login');
    }

    /**
     * DBda Studentlar jadvalidan foydalanuvchilar jadvaliga ma`lumot ko`chirish
     */
    public function students_to_users()
    {
        $students = Application::all();

        $st = [];
        foreach ($students as $key => $student) {
            $st[$key]['type'] = 'stu';
            $st[$key]['type_id'] = $student->id;
            $st[$key]['phone'] = $student->phone_number;
            $st[$key]['email'] = null;
            $st[$key]['password'] = Hash::make($student->passport);
        }

        // echo "<pre>";
        // print_r($st);
        User::insert($st);

        return redirect('/');
    }
}