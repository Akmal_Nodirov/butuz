<?php

namespace App\Http\Controllers\Professor;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Semester;
use App\Professor;
use App\Application;
use App\Subject;
use Illuminate\Http\Request;
use Auth;

class SemesterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $semester = Semester::where('course', 'LIKE', "%$keyword%")
                ->orWhere('semester', 'LIKE', "%$keyword%")
                ->orWhere('subject_id', 'LIKE', "%$keyword%")
                ->orWhere('application_id', 'LIKE', "%$keyword%")
                ->orWhere('professor_id', 'LIKE', "%$keyword%")
                ->orWhere('late_count', 'LIKE', "%$keyword%")
                ->orWhere('absent_count', 'LIKE', "%$keyword%")
                ->orWhere('present_count', 'LIKE', "%$keyword%")
                ->orWhere('homework', 'LIKE', "%$keyword%")
                ->orWhere('middle_exam', 'LIKE', "%$keyword%")
                ->orWhere('final_exam', 'LIKE', "%$keyword%")
                ->orWhere('total', 'LIKE', "%$keyword%")
                ->orWhere('mark', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $semester = Semester::latest()->paginate($perPage);
        }

        return view('professor.semester.index', compact('semester','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('professor.semester.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Semester::create($requestData);

        return redirect('professor/semester')->with('flash_message', 'Semester added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $semester = Semester::findOrFail($id);

        return view('professor.semester.show', compact('semester'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $semester = Semester::findOrFail($id);

        return view('professor.semester.edit', compact('semester'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $semester = Semester::findOrFail($id);
        $semester->update($requestData);

        return redirect('professor/semester')->with('flash_message', 'Semester updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Semester::destroy($id);

        return redirect('professor/semester')->with('flash_message', 'Semester deleted!');
    }

    /**
     * Semesterga baho qo`yish
     */
    public function new_mark(Request $request){
        $professor = Professor::findOrFail(Auth::user()->type_id);
        $students = [];
        $subject = null;
        $sem = null;
        if(isset($request->group)){
            $students = Application::where('group_id',$request->group)->orderBy('order_num','asc')->get();
            $subj = Subject::findOrFail($request->subject);
            $sem = Semester::join('applications','applications.id','=','semester.application_id')
                ->orderBy('applications.'.'order_num', 'asc')
                ->where([
                    ['semester.course','=',$request->input('course')],
                    ['semester.semester','=',$request->input('semester')],
                    ['semester.group_id','=',$request->input('group')],
                    ['semester.subject_id','=',$request->input('subject')],
                ])
                // ->with('application')
                // ->orderBy('order_num')
                ->get();
            
            if(count($sem)){
                return view('professor.semester.update_mark',compact('professor','students','subj','sem'));
            }
        }
        return view('professor.semester.mark',compact('professor','students','subj','sem'));
    }

    /**
     * Semestrovka baholarini saqlash
     */
    public function save_mark(Request $request){
        
        // echo "<pre>";
        // print_r($request->input());
        // return;
        $semester = [];

        for($i=0;$i<count($request->input('group'));$i++){
            $semester[$i]['course'] = $request->input('course.'.$i);
            $semester[$i]['semester'] = $request->input('semester.'.$i);
            $semester[$i]['group_id'] = $request->input('group.'.$i);
            $semester[$i]['subject_id'] = $request->input('subject.'.$i);
            $semester[$i]['application_id'] = $request->input('student.'.$i);
            $semester[$i]['professor_id'] = Auth::user()->type_id;
            $semester[$i]['late_count'] = $request->input('late_count.'.$i);
            $semester[$i]['absent_count'] = $request->input('absent_count.'.$i);
            $semester[$i]['present_count'] = $request->input('present_count.'.$i);
            $semester[$i]['homework'] = $request->input('homework.'.$i);
            $semester[$i]['bonus'] = $request->input('bonus.'.$i);
            $semester[$i]['middle_exam'] = $request->input('middle_exam.'.$i);
            $semester[$i]['final_exam'] = $request->input('final_exam.'.$i);
            $semester[$i]['total'] = $request->input('total.'.$i);
            $semester[$i]['mark'] = $request->input('mark.'.$i);
            $semester[$i]['rate'] = $request->input('rate.'.$i);
            $semester[$i]['credit_x_rate'] = $request->input('credit_x_rate.'.$i);
        }

        Semester::insert($semester);
        
        return redirect('professor/new_mark')->with('flash_message', 'Semester inserted!');
    } 

    /**
     * Semester baholarini o`zgartirish
     */   
    public function update_mark(Request $request){
        // echo "<pre>";
        // print_r($request->input());
        // echo "</pre>";
        // exit;
        for($i=0;$i<count($request->input('group'));$i++){
            $semester = Semester::where([
                ['course','=',$request->input('course.'.$i)],
                ['semester','=',$request->input('semester.'.$i)],
                ['group_id','=',$request->input('group.'.$i)],
                ['subject_id','=',$request->input('subject.'.$i)],
                ['application_id','=',$request->input('student.'.$i)],
            ])
            ->first();
            $semester->late_count = $request->input('late_count.'.$i);
            $semester->absent_count = $request->input('absent_count.'.$i);
            $semester->present_count = $request->input('present_count.'.$i);
            $semester->homework = $request->input('homework.'.$i);
            $semester->bonus = $request->input('bonus.'.$i);
            $semester->middle_exam = $request->input('middle_exam.'.$i);
            $semester->final_exam = $request->input('final_exam.'.$i);
            $semester->total = $request->input('total.'.$i);
            $semester->mark = $request->input('mark.'.$i);
            $semester->rate = $request->input('rate.'.$i);
            $semester->credit_x_rate = $request->input('credit_x_rate.'.$i);
            $semester->save();
        }
        return redirect('professor/new_mark')->with('flash_message', 'Semester updated!');
    }
}
