<?php

namespace App\Http\Controllers\Professor;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Faculty;
use App\Professor;
use Illuminate\Http\Request;

class DashboardController extends Controller{

	public function index(Request $request){
		$professor = Professor::where('id',Auth::user()->type_id)->first();
		return view('professor.home',compact('professor'));
	}

	public function choosesemester(Request $request){
		return view('professor.dashboard.choosesemester');
	}
}