<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Administration;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class AdministrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $administration = Administration::where('image', 'LIKE', "%$keyword%")
                ->orWhere('fullname_uz', 'LIKE', "%$keyword%")
                ->orWhere('fullname_ru', 'LIKE', "%$keyword%")
                ->orWhere('fullname_en', 'LIKE', "%$keyword%")
                ->orWhere('fullname_kr', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('position_id', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $administration = Administration::latest()->paginate($perPage);
        }

        return view('admin.administration.index', compact('administration'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.administration.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'image' => 'required|mimes:jpeg,bmp,png,jpg',
            'fullname_uz' => 'required',
            'fullname_ru' => 'required',
            'fullname_en' => 'required',
            'fullname_kr' => 'required',
            // 'phone' => 'required',
            'position_id' => 'required',
            'published' => 'required'
        ]);
        
        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('administration'))
            {
                Storage::disk('public')->makeDirectory('administration');
            }

            $admin_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('administration/'.$imageName,$admin_image);

        } else {
            $imageName = "";
        }

        $administration = new Administration;
        $administration->image = $imageName;
        $administration->fullname_uz = $request->fullname_uz;
        $administration->fullname_ru = $request->fullname_ru;
        $administration->fullname_en = $request->fullname_en;
        $administration->fullname_kr = $request->fullname_kr;
        $administration->phone = $request->phone;
        $administration->position_id = $request->position_id;
        $administration->published = $request->published;
        $administration->save();

        return redirect('admin/administration')->with('flash_message', 'Administration added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $administration = Administration::findOrFail($id);

        return view('admin.administration.show', compact('administration'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $administration = Administration::findOrFail($id);

        return view('admin.administration.edit', compact('administration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Administration $administration)
    {
        
        $this->validate($request,[
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'fullname_uz' => 'required',
            'fullname_ru' => 'required',
            'fullname_en' => 'required',
            'fullname_kr' => 'required',
            // 'phone' => 'required',
            'position_id' => 'required',
            'published' => 'required'
        ]);
        
        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('administration'))
            {
                Storage::disk('public')->makeDirectory('administration');
            }
            
            if(Storage::disk('public')->exists('administration/'.$administration->image))
            {
                Storage::disk('public')->delete('administration/'.$administration->image);
            }

            $admin_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('administration/'.$imageName,$admin_image);

        } else {
            $imageName = $administration->image;
        }

        $administration->image = $imageName;
        $administration->fullname_uz = $request->fullname_uz;
        $administration->fullname_ru = $request->fullname_ru;
        $administration->fullname_en = $request->fullname_en;
        $administration->fullname_kr = $request->fullname_kr;
        $administration->phone = $request->phone;
        $administration->position_id = $request->position_id;
        $administration->published = $request->published;
        $administration->save();

        return redirect('admin/administration')->with('flash_message', 'Administration updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Administration $administration)
    {
        if(Storage::disk('public')->exists('administration/'.$administration->image))
        {
            Storage::disk('public')->delete('administration/'.$administration->image);
        }

        $administration->delete();

        return redirect('admin/administration')->with('flash_message', 'Administration deleted!');
    }
}
