<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\About;
use App\Talk;
use App\Application;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class HomeController extends Controller
{

    public function upload(Request $request)
    {

        $path = public_path() . '/images/';
        $file = $request->file('file');
        $filename = str_random(20) . '.' . $file->getClientOriginalExtension() ?: 'png';
        $img = Image::make($file);
        $img->save($path . $filename, 100);
        echo '/images/' . $filename;
    }

    public function set_payment($id, Request $request)
    {
        $application = Application::findOrFail($id);
        $application->payment_status = ($application->payment_status) ? false : true;
        $application->save();

        return redirect()->back();
    }

    public function set_pass($id, Request $request)
    {
        $application = Application::findOrFail($id);
        $application->pass = ($application->pass) ? false : true;
        $application->save();

        return redirect()->back();
    }

    public function set_test(Request $request)
    {
        $application = null;
        if ($request->get('applicant_id') > 0) {
            $application = Application::findOrFail($request->get('applicant_id'));
        }

        if ($request->get('test_score') > 0) {
            $application->test = ($request->get('test_score')) ? $request->get('test_score') : 0;
            $application->save();
        }
        $request->session()->flash('alert-success', 'Test score saved successully!');
        return redirect()->back();
    }

    public function set_test_ajax(Request $request)
    {

        $applicant_id = $request->get('applicant_id');
        $criteria_one = $request->get('criteria_one');
        $criteria_two = $request->get('criteria_two');
        $criteria_three = $request->get('criteria_three');
        $criteria_four = $request->get('criteria_four');

        $talk_model = Talk::where('application_id', $applicant_id)->first();
        $application = Application::findOrFail($applicant_id);

        if(!$talk_model){
            $talk_model = new Talk;
        }

        $talk_model->application_id = $applicant_id;
        $talk_model->criteria1 = $criteria_one;
        $talk_model->criteria2 = $criteria_two;
        $talk_model->criteria3 = $criteria_three;
        $talk_model->criteria4 = $criteria_four;
        $talk_model->score = $criteria_one + $criteria_two + $criteria_three + $criteria_four;
        $talk_model->save();

        $talk_all = Talk::where('application_id', '=', $applicant_id)->get();
        $interview = 0;
        foreach ($talk_all as $key => $value) {
            $interview += $value->score;
        }
        $application->interview = number_format($interview / 3, 2, '.', '');;
        $application->save();

        return $talk_model->score;
    }

    public function set_status_ajax(Request $request)
    {

        $applicant_id = $request->get('applicant_id');
        $status = $request->get('status');
        $application = Application::findOrFail($applicant_id);
        $application->viewable_status = $status;
        $application->save();

        return 'true';
    }

    public function send_sms(Request $request)
    {
        $textMessage = $request->get('text_message');
        $checkedlist = $request->get('checkedlist');
        $paid = $request->get('paid');
        $facultydata = $request->get('facultydata');
        $all_application_ids = explode(',', $checkedlist);
        $url = "http://avizone.uz/web/images/smslar/";
        if ($all_application_ids[0] == 'all') {
            if (!empty($facultydata)) {
                $applications = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->get();
            } else {
                $applications = Application::where('status', false)->get();
            }

            if (!empty($paid)) {
                if ($paid == "payed") {
                    $applications = Application::where('status', false)
                        ->where('payment_status', 1)
                        ->get();
                }
                if ($paid == "unpayed") {
                    $applications = Application::where('status', false)
                        ->where('payment_status', 0)
                        ->get();
                }
            } else {
                $applications = Application::where('status', false)->get();
            }
            if (!empty($facultydata) && !empty($paid)) {
                if ($paid == "payed") {
                    $applications = Application::where('status', false)
                        ->where('faculty_id', $facultydata)
                        ->where('payment_status', 1)
                        ->get();
                }
                if ($paid == "unpayed") {
                    $applications = Application::where('status', false)
                        ->where('faculty_id', $facultydata)
                        ->where('payment_status', 0)
                        ->get();
                }
            }
            foreach ($applications as $k => $v) {
                if (!isset($v->test)) {
                    $phone = $this->getCorrectPhoneNumber($v->phone_number);
                    $phone_parrent = $this->getCorrectPhoneNumber($v->parent_phone_number);
                    if ($phone) {
                        $fields = array(
                            'phone' => $phone,
                            'message' => $textMessage
                        );
                        $fields_string = '';
                        foreach ($fields as $key => $value) {
                            $fields_string .= $key . '=' . $value . '&';
                        }
                        rtrim($fields_string, '&');
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, count($fields));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }
                    if ($phone_parrent) {
                        $fields = array(
                            'phone' => $phone_parrent,
                            'message' => $textMessage
                        );
                        $fields_string = '';
                        foreach ($fields as $key => $value) {
                            $fields_string .= $key . '=' . $value . '&';
                        }
                        rtrim($fields_string, '&');
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, count($fields));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }
                }
            }
        } else {
            foreach ($all_application_ids as $key => $value) {
                $application = Application::findOrFail($value);
                $phone_num = $this->getCorrectPhoneNumber($application->phone_number);
                $phone_num_parent = $this->getCorrectPhoneNumber($application->parent_phone_number);
                if (!isset($application->test)) {
                    if ($phone_num) {
                        $fields = array(
                            'phone' => $phone_num,
                            'message' => $textMessage
                        );
                        $fields_string = '';
                        foreach ($fields as $key => $value) {
                            $fields_string .= $key . '=' . $value . '&';
                        }
                        rtrim($fields_string, '&');
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, count($fields));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }
                    if ($phone_num_parent) {
                        $fields = array(
                            'phone' => $phone_num_parent,
                            'message' => $textMessage
                        );
                        $fields_string = '';
                        foreach ($fields as $key => $value) {
                            $fields_string .= $key . '=' . $value . '&';
                        }
                        rtrim($fields_string, '&');
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, count($fields));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }
                }
            }
        }
        $request->session()->flash('alert-success', 'SMS sent successully!');
        return redirect()->back();
    }

    protected
    function getCorrectPhoneNumber($phone_number)
    {
        if ($phone_number) {
            return '998' . substr($phone_number, -9);
        }

    }


    public
    function interview(Request $request)
    {
        $user_id = $request->get('user_id');
        $applicant_id2 = $request->get('applicant_id2');
        $comment = $request->get('comment');
        $criteria1 = $request->get('criteria1');
        $criteria2 = $request->get('criteria2');
        $criteria3 = $request->get('criteria3');
        $criteria4 = $request->get('criteria4');
        $all_score = $criteria1 + $criteria2 + $criteria3 + $criteria4;

        $application = null;
        if ($applicant_id2 > 0) {
            $application = Application::findOrFail($applicant_id2);
        }
        $talk_old = Talk::where('application_id', '=', $applicant_id2)->where('user_id', '=', $user_id)->get();
        $talk_all = Talk::where('application_id', '=', $applicant_id2)->get();
        if (count($talk_old) > 0 && count($talk_all) < 4) {
            $talk_old[0]->criteria1 = $criteria1;
            $talk_old[0]->criteria2 = $criteria2;
            $talk_old[0]->criteria3 = $criteria3;
            $talk_old[0]->criteria4 = $criteria4;
            $talk_old[0]->score = $all_score;
            $talk_old[0]->comment = $comment;
            $talk_old[0]->save();
        } elseif (count($talk_all) >= 3) {
            $request->session()->flash('alert-danger', 'Only 3 Professor allowed!');
            return redirect()->back();
        } else {
            $talk = new Talk([
                'application_id' => $applicant_id2,
                'user_id' => $user_id,
                'criteria1' => $criteria1,
                'criteria2' => $criteria2,
                'criteria3' => $criteria3,
                'criteria4' => $criteria4,
                'score' => $all_score,
                'comment' => $comment,
            ]);
            $talk->save();
        }

        $talk_all = Talk::where('application_id', '=', $applicant_id2)->get();
        $interview = 0;
        foreach ($talk_all as $key => $value) {
            $interview += $value->score;
        }
        $application->interview = number_format($interview / 3, 2, '.', '');;
        $application->save();
        $request->session()->flash('alert-success', 'Test score saved successully!');
        return redirect()->back();
    }

    public
    function getInterviewData(Request $request)
    {
        $user_id = $request->get('user_id');
        $applicant_id = $request->get('applicant_id');
        $talk_old = Talk::where('application_id', '=', $applicant_id)->where('user_id', '=', $user_id)->get();
        return response()->json([
            "interview" => $talk_old
        ]);
        // return response()->json($talk_old);
    }


}
