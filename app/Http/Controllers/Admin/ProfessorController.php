<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Professor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class ProfessorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $professor = Professor::where('firstname', 'LIKE', "%$keyword%")
                ->orWhere('lastname', 'LIKE', "%$keyword%")
                ->orWhere('university_name', 'LIKE', "%$keyword%")
                ->orWhere('grade', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } 
        else {   
            $professor = Professor::latest()->paginate($perPage);
        }

        return view('admin.professor.index', compact('professor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.professor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'university_name' => 'required',
            'grade' => 'required',
            'type' => 'required|integer',
            'faculty_id' => 'integer'
        ]);

        $requestData = $request->all();
        
        $professor = Professor::create($requestData);
        
        User::create([
            'type' => 'prof',
            'type_id' => $professor->id,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => Hash::make(substr($request->phone,-7))
        ]);

        $professor->groups()->attach($request->groups);

        return redirect('admin/professor')->with('flash_message', 'Professor added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $professor = Professor::findOrFail($id);

        return view('admin.professor.show', compact('professor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $professor = Professor::findOrFail($id);

        return view('admin.professor.edit', compact('professor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
            'university_name' => 'required',
            'grade' => 'required',
            'type' => 'required|integer',
            'faculty_id' => 'integer'
        ]);

        $requestData = $request->all();
        
        $professor = Professor::findOrFail($id);
        $professor->update($requestData);

        $professor->groups()->sync($request->groups);

        return redirect('admin/professor')->with('flash_message', 'Professor updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, $id)
    {
        $professor = Professor::findOrFail($id);
        $professor->groups()->detach($request->groups);
        $professor->delete();

        return redirect('admin/professor')->with('flash_message', 'Professor deleted!');
    }
}
