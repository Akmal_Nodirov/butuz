<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Slider;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $slider = Slider::where('slide', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $slider = Slider::latest()->paginate($perPage);
        }

        return view('admin.slider.index', compact('slider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'slide' => 'required|mimes:jpeg,bmp,png,jpg',
        ]);
        
        $image = $request->file('slide');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('slider'))
            {
                Storage::disk('public')->makeDirectory('slider');
            }

            $slide = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('slider/'.$imageName,$slide);

        } else {
            $imageName = "";
        }

        $slider = new Slider;
        $slider->slide = $imageName;
        $slider->published = $request->published;
        $slider->save();

        return redirect('admin/slider')->with('flash_message', 'Slider added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $slider = Slider::findOrFail($id);

        return view('admin.slider.show', compact('slider'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);

        return view('admin.slider.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Slider $slider)
    {
        $this->validate($request,[
            'slide' => 'mimes:jpeg,bmp,png,jpg',
        ]);
        
        $image = $request->file('slide');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('slider'))
            {
                Storage::disk('public')->makeDirectory('slider');
            }

            if(Storage::disk('public')->exists('slider/'.$slider->slide))
            {
                Storage::disk('public')->delete('slider/'.$slider->slide);
            }
            $slide = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('slider/'.$imageName,$slide);

        } else {
            $imageName = $slider->slide;
        }

        $slider->slide = $imageName;
        $slider->published = $request->published;
        $slider->save();

        // $requestData = $request->all();
        
        // $slider = Slider::findOrFail($id);
        // $slider->update($requestData);

        return redirect('admin/slider')->with('flash_message', 'Slider updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Slider $slider)
    {
        if(Storage::disk('public')->exists('slider/'.$slider->slide))
        {
            Storage::disk('public')->delete('slider/'.$slider->slide);
        }

        // Slider::destroy($id);
        $slider->delete();

        return redirect('admin/slider')->with('flash_message', 'Slider deleted!');
    }
}
