<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Faculty;
use App\Group;
use App\Professor;
use App\Semester;
use App\Subject;
use App\Application;
use App\User;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        $applications = Application::where('status', 0)->groupBy('faculty_id')->select('faculty_id', DB::raw('count(*) as total'))->get();
        $applications_paid = Application::where('status', 0)->groupBy('faculty_id')->select('faculty_id', DB::raw('count(*) as total'))->where('payment_status', 1)->get();

        $applications_by_date = Application::where('status', 0)
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"))
            ->select(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as day"), DB::raw('count(*) as total'))
            ->get();

        $paid_applications = Application::where('status', 0)
            ->where('payment_status', 1)
            ->count();

        return view('admin.home', compact('applications', 'applications_by_date', 'paid_applications', 'applications_paid'));
    }

    /**
     * Talaba natijalarini ko`rish
     */
    public function student_results(Request $request, $student_id)
    {
        $groups = Group::all();
        $subjects = Subject::all();
        $student = Application::findOrFail($student_id);
        $semester = null;
        if (is_numeric($request->input('course')) && is_numeric($request->input('semester'))) {
            $semester = Semester::where([
                ['course', '=', $request->input('course')],
                ['semester', '=', $request->input('semester')],
                ['application_id', '=', $student_id]
            ])
                ->get();
        }
        return view('admin.dashboard.student_results', compact('groups', 'subjects', 'semester', 'student'));
    }

    /**
     * Barcha talabalar natijalarini ko`rish
     */
    public function students_results(Request $request)
    {
        $groups = Group::all();
        $subjects = Subject::all();
        $group = $request->input('group');
        if ($group != "0") {
            $students = Application::where('group_id', $group)->where('status', true)->get();
        } else {
            $students = Application::where('status', true)->get();
        }

        $mark = $request->input('mark');

        $semester = null;
        $sem = new Semester();
        if ($mark == "0" && is_numeric($request->input('course')) && is_numeric($request->input('semester'))) {
            $semester = Semester::where([
                ['course', '=', $request->input('course')],
                ['semester', '=', $request->input('semester')],
                // ['application_id','=',$student_id]
            ])->get();
        }

        if ($mark != "0" && is_numeric($request->input('course')) && is_numeric($request->input('semester'))) {
            $course = $request->input('course');
            $semester = $request->input('semester');

//            $students = Semester::where([
//                ['mark', '=', $mark],
//            ])->get();
            // $students = Semester::getAverageMarkBySemestr($semester, $mark);
            $studentsAll = $this->getAverageMarkBySemestr($course, $semester, $mark);

            $semester = Semester::where([
                ['course', '=', $course],
                ['semester', '=', $semester],
            ])
                ->whereIn('application_id', $studentsAll)
                ->get();
        }

        return view('admin.dashboard.students_results', compact('groups', 'subjects', 'semester', 'students'));
        //return view('admin.dashboard.students_resultstest',compact('groups','subjects','semester','students'));
    }

    protected function getAverageMarkBySemestr($course, $semester, $mark)
    {
        $semesterGetAllAppl = Semester::where([
            ['course', '=', $course],
            ['semester', '=', $semester],
        ])->get();

        $students = [];
        foreach ($semesterGetAllAppl as $key => $sem) {
            // $students[$key]=[
            //   'course' => $sem->course,
            //   'semester' => $sem->semester,
            //   'group_id' => $sem->group_id,
            //   'subject_id' => $sem->subject_id,
            //   'application_id' => $sem->application_id,
            //   'professor_id' => $sem->professor_id,
            //   'late_count' => $sem->late_count,
            //   'absent_count' => $sem->absent_count,
            //   'present_count' => $sem->present_count,
            //   'homework' => $sem->homework,
            //   'middle_exam' => $sem->middle_exam,
            //   'final_exam' => $sem->final_exam,
            //   'bonus' => $sem->bonus,
            //   'total' => $sem->total,
            //   'mark' => $sem->mark,
            //   'rate' => $sem->rate,
            // ];
            $students[$key] = $sem->application_id;
        }
        // $uniqApplicationId=array_unique(array_map(function($i){
        //   return $i['application_id'];
        // }, $students));
        //


        $uniq_students = array_unique($students);

        $totals = null;
        $credits = null;
        $all_students = [];
        foreach ($uniq_students as $k => $id) {
            $totals = 0;
            $credits = 0;
            foreach ($semesterGetAllAppl as $key => $sem) {
                if ($sem->application_id == $id) {
                    $totals += ($sem->total * $sem->subject->credit);
                    $credits += $sem->subject->credit;
                }
            }
            $average = $credits != 0 ? round($totals / $credits) : 0;
            if ($average >= 95 && $average <= 100 && $mark == "A+") {
                $all_students[$k] = $id;
            } elseif ($average >= 90 && $average <= 94 && $mark == "A") {
                $all_students[$k] = $id;
            } elseif ($average >= 85 && $average <= 89 && $mark == "B+") {
                $all_students[$k] = $id;
            } elseif ($average >= 80 && $average <= 84 && $mark == "B") {
                $all_students[$k] = $id;
            } elseif ($average >= 75 && $average <= 79 && $mark == "C+") {
                $all_students[$k] = $id;
            } elseif ($average >= 70 && $average <= 74 && $mark == "C") {
                $all_students[$k] = $id;
            } elseif ($average >= 65 && $average <= 69 && $mark == "D+") {
                $all_students[$k] = $id;
            } elseif ($average >= 60 && $average <= 64 && $mark == "D") {
                $all_students[$k] = $id;
            } elseif ($average > 0 && $average <= 59 && $mark == "F") {
                $all_students[$k] = $id;
            }
        }

        return $all_students;
    }

    protected function getAverageByMark($sem, $mark)
    {
        $semester = [];
        $totals = null;
        $credits = null;
        return $semester;
    }


    public function applicant(Request $request)
    {
        date_default_timezone_set("Asia/Tashkent");

        $keyword = $request->get('search');
        $payment_status = $request->get('payment_status');
        $inserted_time = $request->get('inserted_time');
        $faculty_id = $request->get('faculty_id');
        $facultydata = $request->get('facultydata');
        $paid = $request->get('paid');
        $perPage = 20;

        if (!empty($keyword)) {

            $applicants = Application::where('status', false)
                ->where(function ($query) use ($keyword) {
                    $query->where('firstname', 'LIKE', "%$keyword%")
                        ->orWhere('lastname', 'LIKE', "%$keyword%")
                        ->orWhere('phone_number', 'LIKE', "%$keyword%")
                        ->orWhere('passport', 'LIKE', "%$keyword%")
                        ->orWhere('apply_no', 'LIKE', "%$keyword%");
                })
                ->latest()->paginate($perPage);

        } else {
            $applicants = Application::where('status', false)->latest()->paginate($perPage);
        }

        if (!empty($payment_status)) {
            $applicants = Application::where('status', false)
                ->orderBy('payment_status', $payment_status)
                ->paginate($perPage);
        }

        if (!empty($inserted_time)) {
            $applicants = Application::where('status', false)
                ->orderBy('created_at', $inserted_time)
                ->paginate($perPage);
        }
        if (!empty($faculty_id)) {
            $applicants = Application::where('status', false)
                ->orderBy('faculty_id', $faculty_id)
                ->paginate($perPage);
        }
        if (!empty($facultydata)) {
            $applicants = Application::where('status', false)
                ->where('faculty_id', $facultydata)
                ->orderBy('faculty_id', $faculty_id)
                ->paginate($perPage);
        }

        if (!empty($paid)) {
            if ($paid == "payed") {
                $applicants = Application::where('status', false)
                    ->where('payment_status', 1)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }
            if ($paid == "unpayed") {
                $applicants = Application::where('status', false)
                    ->where('payment_status', 0)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }
        }
        if (!empty($facultydata) && !empty($paid)) {

            if ($paid == "payed") {
                $applicants = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->where('payment_status', 1)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }
            if ($paid == "unpayed") {
                $applicants = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->where('payment_status', 0)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }

        }

        $faculties = Faculty::orderBy('name', 'asc')->get();
//   if (!empty($payment_status)) {
//       $applicants = Application::where('status',false)
//        ->orderBy('payment_status',$payment_status)
//        ->paginate($perPage);
//   }
//   elseif (!empty($inserted_time)) {
//       $applicants = Application::where('status',false)
//        ->orderBy('created_at',$inserted_time)
//        ->paginate($perPage);
//   }
//   else{
// $applicants = Application::where('status',false)->latest()->paginate($perPage);
//   }


        return view('admin.dashboard.applicant', compact('applicants', 'faculties'));
    }

    public function app_result(Request $request)
    {
        date_default_timezone_set("Asia/Tashkent");
        $keyword = $request->get('search');
        $facultydata = $request->get('facultydata');
        $perPage = 20;

//        $app = Application::all();
//        foreach ($app as $item){
//            if($item->interview === NULL){
//                $item->interview = 0;
//                $item->save();
//
//            }
//            if($item->test === NULL){
//                $item->test = 0;
//                $item->save();
//            }
//            if($item->viewable_status === NULL){
//                $item->viewable_status = 0;
//                $item->save();
//            }
//        }

        $applicants =  Application::where('status', false)
            ->where('payment_status', 1)
//            ->where('viewable_status', 1)
            ->select(DB::raw('*, test + interview as total'))
            ->orderBy('total', 'desc')
            ->orderBy('interview', 'desc')
            ->latest()
            ->paginate($perPage);

        // GET Faculity
        if (!empty($facultydata)) {
            $applicants = Application::where('status', false)
                ->where('faculty_id', $facultydata)
                ->where('payment_status', 1)
//                ->where('viewable_status', 1)
                ->select(DB::raw('*, test + interview as total'))
                ->orderBy('total', 'desc')
                ->orderBy('interview', 'desc')
                ->latest()
                ->paginate($perPage);
        }
        // GET Search
        if (!empty($keyword)) {
            $applicants = Application::where('status', false)
                ->where(function ($query) use ($keyword) {
                    $query->where('firstname', 'LIKE', "%$keyword%")
                        ->orWhere('lastname', 'LIKE', "%$keyword%")
                        ->orWhere('phone_number', 'LIKE', "%$keyword%")
                        ->orWhere('passport', 'LIKE', "%$keyword%")
                        ->orWhere('apply_no', 'LIKE', "%$keyword%");
                })
                ->where('payment_status', 1)
//                ->where('viewable_status', 1)
                ->select(DB::raw('*, test + interview as total'))
                ->orderBy('total', 'desc')
                ->orderBy('interview', 'desc')
                ->latest()
                ->paginate($perPage);
        }

        $faculties = Faculty::orderBy('name', 'asc')->get();

        return view('admin.dashboard.appresult', compact('applicants', 'faculties'));
    }

    public function applicantsms(Request $request)
    {
        date_default_timezone_set("Asia/Tashkent");

        $keyword = $request->get('search');
        $payment_status = $request->get('payment_status');
        $inserted_time = $request->get('inserted_time');
        $faculty_id = $request->get('faculty_id');
        $facultydata = $request->get('facultydata');
        $paid = $request->get('paid');

        $perPage = 20;

        if (!empty($keyword)) {

            $applicants = Application::where('status', false)
                ->where(function ($query) use ($keyword) {
                    $query->where('firstname', 'LIKE', "%$keyword%")
                        ->orWhere('lastname', 'LIKE', "%$keyword%")
                        ->orWhere('phone_number', 'LIKE', "%$keyword%")
                        ->orWhere('passport', 'LIKE', "%$keyword%")
                        ->orWhere('apply_no', 'LIKE', "%$keyword%");
                })
                ->latest()->paginate($perPage);

        } else {
            $applicants = Application::where('status', false)->latest()->paginate($perPage);
        }

        if (!empty($payment_status)) {
            $applicants = Application::where('status', false)
                ->orderBy('payment_status', $payment_status)
                ->paginate($perPage);
        }

        if (!empty($inserted_time)) {
            $applicants = Application::where('status', false)
                ->orderBy('created_at', $inserted_time)
                ->paginate($perPage);
        }
        if (!empty($faculty_id)) {
            $applicants = Application::where('status', false)
                ->orderBy('faculty_id', $faculty_id)
                ->paginate($perPage);
        }
        if (!empty($facultydata)) {
            $applicants = Application::where('status', false)
                ->where('faculty_id', $facultydata)
                ->orderBy('faculty_id', $faculty_id)
                ->paginate($perPage);
        }

        if (!empty($paid)) {
            if ($paid == "payed") {
                $applicants = Application::where('status', false)
                    ->where('payment_status', 1)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }
            if ($paid == "unpayed") {
                $applicants = Application::where('status', false)
                    ->where('payment_status', 0)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }
        }
        if (!empty($facultydata) && !empty($paid)) {

            if ($paid == "payed") {
                $applicants = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->where('payment_status', 1)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }
            if ($paid == "unpayed") {
                $applicants = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->where('payment_status', 0)
                    ->orderBy('payment_status', $paid)
                    ->paginate($perPage);
            }

        }

        $faculties = Faculty::orderBy('name', 'asc')->get();
//   if (!empty($payment_status)) {
//       $applicants = Application::where('status',false)
//        ->orderBy('payment_status',$payment_status)
//        ->paginate($perPage);
//   }
//   elseif (!empty($inserted_time)) {
//       $applicants = Application::where('status',false)
//        ->orderBy('created_at',$inserted_time)
//        ->paginate($perPage);
//   }
//   else{
// $applicants = Application::where('status',false)->latest()->paginate($perPage);
//   }


        return view('admin.dashboard.applicantsms', compact('applicants', 'faculties'));
    }

    public
    function excel(Request $request)
    {
        $facultydata = $request->get('facultydata');
        $paid = $request->get('paid');

        date_default_timezone_set("Asia/Tashkent");


        if (!empty($facultydata)) {
            $applications = Application::where('status', false)
                ->where('faculty_id', $facultydata)
                ->get();
        }else {
            $applications = Application::where('status', false)->get();
        }

        if (!empty($paid)) {
            if ($paid == "payed") {
                $applications = Application::where('status', false)
                    ->where('payment_status', 1)
                    ->get();
            }
            if ($paid == "unpayed") {
                $applications = Application::where('status', false)
                    ->where('payment_status', 0)
                    ->get();
            }
        }else {
            $applications = Application::where('status', false)->get();
        }
        if (!empty($facultydata) && !empty($paid)) {
            if ($paid == "payed") {
                $applications = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->where('payment_status', 1)
                    ->get();
            }
            if ($paid == "unpayed") {
                $applications = Application::where('status', false)
                    ->where('faculty_id', $facultydata)
                    ->where('payment_status', 0)
                    ->get();
            }
        }

        $export_data[] = array(
            'Student Id',
            'Order Num',
            'First Name',
            'Last Name',
            'Apply No.',
            'Photo',
            'Faculty Name',
            'Group',
            'Examination Language',
            'Status',
            'Date Of Birth',
            'Nationality',
            'Passport',
            'Gender',
            'Phone Number',
            'Email',
            'Address',
            'Diploma Copy',
            'Passport Copy',
            'Inn Copy',
            'Work Place',
            'Work Phone Number',
            'High School Name',
            'High School Graduation Year',
            'High School Major',
            'University Name',
            'University Graduation Year',
            'University Major',
            'Parent Name',
            'Relationship',
            'Parent Phone Number',
            'Parent Email',
            'Examination1',
            'Organization Of Issue1',
            'Grade1',
            'Score1',
            'Date1',
            'Examination2',
            'Organization Of Issue2',
            'Grade2',
            'Score2',
            'Date2',
            'Examination3',
            'Organization Of Issue3',
            'Grade3',
            'Score3',
            'Date3',
            'Changed',

            'Payment Status',
        );
        foreach ($applications as $key => $value) {

            $export_data[] = array(
                'Student Id' => $value['student_id'],
                'Order Num' => $value['order_num'],
                'First Name' => $value['firstname'],
                'Last Name' => $value['lastname'],
                'Apply No.' => $value['apply_no'],
                'Photo' => $value['photo'],
                'Faculty Name' => $value->faculty->name,
                'Group' => $value['group']['name'],
                'Examination Language' => $this->getExamLang($value['examination_language']),
                'Status' => ($value['status']) ? 'Applicant' : 'Student',
                'Date Of Birth' => $value['date_of_birth'],
                'Nationality' => $value['nationality'],
                'Passport' => $value['passport'],
                'Gender' => $value['gender'] ? 'Male' : 'Female',
                'Phone Number' => $value['phone_number'],
                'Email' => $value['email'],
                'Address' => $value['address'],
                'Diploma Copy' => $value['diploma_copy'],
                'Passport Copy' => $value['passport_copy'],
                'Inn Copy' => $value['inn_copy'],
                'work_place' => $value['work_place'],
                'work_phone_number' => $value['work_phone_number'],
                'high_school_name' => $value['high_school_name'],
                'high_school_graduation_year' => $value['high_school_graduation_year'],
                'high_school_major' => $value['high_school_major'],
                'university_name' => $value['university_name'],
                'university_graduation_year' => $value['university_graduation_year'],
                'university_major' => $value['university_major'],
                'parent_name' => $value['parent_name'],
                'relationship' => $value['relationship'],
                'parent_phone_number' => $value['parent_phone_number'],
                'parent_email' => $value['parent_email'],

                'Examination1' => $value['examination1'],
                'Organization Of Issue1' => $value['organization_of_issue1'],
                'Grade1' => $value['grade1'],
                'Score1' => $value['score1'],
                'Date1' => $value['date1'],

                'Examination2' => $value['examination2'],
                'Organization Of Issue2' => $value['organization_of_issue2'],
                'Grade2' => $value['grade2'],
                'Score2' => $value['score2'],
                'Date2' => $value['date2'],

                'Examination3' => $value['examination3'],
                'Organization Of Issue3' => $value['organization_of_issue3'],
                'Grade3' => $value['grade3'],
                'Score3' => $value['score3'],
                'Date3' => $value['date3'],
                'Changed' => $value['changed'] ? 'Changed' : 'Not changed',

                'Payment Status' => ($value['payment_status']) ? 'Paid' : 'Not Paid',
            );

        }
        return Excel::create("Applicants Data", function ($excel) use ($export_data) {
            $excel->setTitle("Applicants Data");

            $excel->sheet("Applicants Data", function ($sheet) use ($export_data) {
                $sheet->fromArray($export_data, null, 'A1', false, false);
                $sheet->cells('A1:AW1', function ($cells) {
                    $cells->setFontWeight('bold');
                    $cells->setBorder('node', 'none', 'solid', 'none');
                });
            });
        })->export('xls');
    }

    protected
    function getExamLang($lang_id)
    {
        switch ($lang_id) {
            case '1':
                return "Uzbek";
                break;
            case '2':
                return "Russian";
                break;
            case '3':
                return "Korean";
                break;
        }
    }

    public function interviewers(Request $request)
    {
        date_default_timezone_set("Asia/Tashkent");

        $user = Auth::user();
        $faculty = 'No faculty selected';
        if($user->professor && $user->professor->faculty){
            $faculty = $user->professor->faculty;
            $faculty_id = $faculty->id;
        }

        $perPage = 20;

        $applicants = Application::query()
            ->when($faculty_id > 0, function ($q) use ($faculty_id) {
                $q->where('faculty_id', $faculty_id);
            })
            ->where('status', false)
            ->orderBy('order_num')
            ->paginate($perPage);


        //$faculties = Faculty::orderBy('name', 'asc')->get();

        return view('admin.dashboard.interviewers', compact('applicants', 'faculty'));
    }

}
