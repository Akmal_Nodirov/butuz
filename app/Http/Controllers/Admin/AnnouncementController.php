<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Announcement;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $announcement = Announcement::where('image', 'LIKE', "%$keyword%")
                ->orWhere('title_uz', 'LIKE', "%$keyword%")
                ->orWhere('title_ru', 'LIKE', "%$keyword%")
                ->orWhere('title_en', 'LIKE', "%$keyword%")
                ->orWhere('title_kr', 'LIKE', "%$keyword%")
                ->orWhere('content_uz', 'LIKE', "%$keyword%")
                ->orWhere('content_ru', 'LIKE', "%$keyword%")
                ->orWhere('content_en', 'LIKE', "%$keyword%")
                ->orWhere('content_kr', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $announcement = Announcement::latest()->paginate($perPage);
        }

        return view('admin.announcement.index', compact('announcement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.announcement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request){
        $this->validate($request,[
            'image' => 'required|mimes:jpeg,bmp,png,jpg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'content_uz' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_kr' => 'required',
            'published' => 'required'
        ]);
        
        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('announcements'))
            {
                Storage::disk('public')->makeDirectory('announcements');
            }

            $news = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('announcements/'.$imageName,$news);

        } else {
            $imageName = "";
        }

        $announcement = new Announcement;
        $announcement->image = $imageName;
        $announcement->title_uz = $request->title_uz;
        $announcement->title_ru = $request->title_ru;
        $announcement->title_en = $request->title_en;
        $announcement->title_kr = $request->title_kr;
        $announcement->content_uz = $request->content_uz;
        $announcement->content_ru = $request->content_ru;
        $announcement->content_en = $request->content_en;
        $announcement->content_kr = $request->content_kr;
        $announcement->published = $request->published;
        $announcement->save();

        return redirect('admin/announcement')->with('flash_message', 'Announcement added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $announcement = Announcement::findOrFail($id);

        return view('admin.announcement.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $announcement = Announcement::findOrFail($id);

        return view('admin.announcement.edit', compact('announcement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
           
        $this->validate($request,[
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'content_uz' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_kr' => 'required',
            // 'published' => 'required'
        ]);
        
        $image = $request->file('image');
        $announcement = Announcement::find($id);
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('announcements'))
            {
                Storage::disk('public')->makeDirectory('announcements');
            }

            if(Storage::disk('public')->exists('announcements/'.$announcement->image))
            {
                Storage::disk('public')->delete('announcements/'.$announcement->image);
            }

            $announcement_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('announcements/'.$imageName,$announcement_image);

        } 
        else {
            $imageName = $announcement->image;
        }

        $announcement->image = $imageName;
        $announcement->title_uz = $request->title_uz;
        $announcement->title_ru = $request->title_ru;
        $announcement->title_en = $request->title_en;
        $announcement->title_kr = $request->title_kr;
        $announcement->content_uz = $request->content_uz;
        $announcement->content_ru = $request->content_ru;
        $announcement->content_en = $request->content_en;
        $announcement->content_kr = $request->content_kr;
        $announcement->published = $request->published;
        $announcement->save();

        return redirect('admin/announcement')->with('flash_message', 'Announcement updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Announcement $announcement)
    {
        if(Storage::disk('public')->exists('news/'.$announcement->image))
        {
            Storage::disk('public')->delete('news/'.$announcement->image);
        }

        $announcement->delete();

        return redirect('admin/announcement')->with('flash_message', 'Announcement deleted!');
    }
}
