<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Application;
use App\User;
use App\Talk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Storage;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\StudentInput;


class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $application = Application::whereHas('group',function($q)use($keyword){
                $q->where('groups.name','LIKE',"%$keyword%");
            })
            ->where('status',true)
            ->orWhere('firstname', 'LIKE', "%$keyword%")
            ->orWhere('lastname', 'LIKE', "%$keyword%")
            ->orWhere('student_id', 'LIKE', "%$keyword%")
            ->orWhere('apply_no', 'LIKE', "%$keyword%")
            // ->orWhere('photo', 'LIKE', "%$keyword%")
            // ->orWhere('faculty_id', 'LIKE', "%$keyword%")
            // ->orWhere('group_id', 'LIKE', "%$keyword%")
            // ->orWhere('examination_language', 'LIKE', "%$keyword%")
            // ->orWhere('status', 'LIKE', "%$keyword%")
            // ->orWhere('date_of_birth', 'LIKE', "%$keyword%")
            // ->orWhere('nationality', 'LIKE', "%$keyword%")
            // ->orWhere('passport', 'LIKE', "%$keyword%")
            // ->orWhere('gender', 'LIKE', "%$keyword%")
            // ->orWhere('phone_number', 'LIKE', "%$keyword%")
            // ->orWhere('email', 'LIKE', "%$keyword%")
            // ->orWhere('address', 'LIKE', "%$keyword%")
            // ->orWhere('work_place', 'LIKE', "%$keyword%")
            // ->orWhere('work_phone_number', 'LIKE', "%$keyword%")
            // ->orWhere('high_school_name', 'LIKE', "%$keyword%")
            // ->orWhere('high_school_graduation_year', 'LIKE', "%$keyword%")
            // ->orWhere('high_school_major', 'LIKE', "%$keyword%")
            // ->orWhere('university_name', 'LIKE', "%$keyword%")
            // ->orWhere('university_graduation_year', 'LIKE', "%$keyword%")
            // ->orWhere('university_major', 'LIKE', "%$keyword%")
            // ->orWhere('parent_name', 'LIKE', "%$keyword%")
            // ->orWhere('relationship', 'LIKE', "%$keyword%")
            // ->orWhere('parent_phone_number', 'LIKE', "%$keyword%")
            // ->orWhere('parent_email', 'LIKE', "%$keyword%")
            // ->orWhere('date3', 'LIKE', "%$keyword%")
            // ->orWhere('examination1', 'LIKE', "%$keyword%")
            // ->orWhere('organization_of_issue1', 'LIKE', "%$keyword%")
            // ->orWhere('grade1', 'LIKE', "%$keyword%")
            // ->orWhere('score1', 'LIKE', "%$keyword%")
            // ->orWhere('date1', 'LIKE', "%$keyword%")
            // ->orWhere('examination2', 'LIKE', "%$keyword%")
            // ->orWhere('organization_of_issue2', 'LIKE', "%$keyword%")
            // ->orWhere('grade2', 'LIKE', "%$keyword%")
            // ->orWhere('score2', 'LIKE', "%$keyword%")
            // ->orWhere('date2', 'LIKE', "%$keyword%")
            // ->orWhere('examination3', 'LIKE', "%$keyword%")
            // ->orWhere('organization_of_issue3', 'LIKE', "%$keyword%")
            // ->orWhere('grade3', 'LIKE', "%$keyword%")
            // ->orWhere('score3', 'LIKE', "%$keyword%")
            // ->orWhere('date3', 'LIKE', "%$keyword%")
            // ->orWhere('changed', 'LIKE', "%$keyword%")
            ->latest()->paginate($perPage);
        } else {
            $application = Application::latest()->where('status',true)->paginate($perPage);
        }

        return view('admin.application.index', compact('application'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.application.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'photo' => 'required|mimes:jpg,png,gif,jpeg',
            'passport' => 'required',
            'nationality' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'faculty_id' => 'required',
            'examination_language' => 'required',
            'date_of_birth' => 'required',
        ]);

        
        $image = $request->file('photo');
        $name = $request->firstname.'_'.$request->lastname;
        if(isset($image)){
            // make unique name for image
            $currentDate = Carbon::now()->toDateString();
            $imagename = $name.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();
            
            // Check category dir exists
            if(!Storage::disk('public')->exists('photos')){
                Storage::disk('public')->makeDirectory('photos');
            }
            
            // Resize images for category and upload
            $photo = Image::make($image)->save($imagename,100);
            Storage::disk('public')->put('photos/'.$imagename,$photo);
        }
        else{
            $imagename = '';
        }

        // $requestData = $request->all();
        
        $student_id = Application::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'apply_no' => $request->apply_no,
            'photo' => $imagename,
            'faculty_id' => $request->faculty_id,
            'group_id' => $request->group_id,
            'examination_language' => $request->examination_language,
            'status' => $request->status,
            'date_of_birth' => $request->date_of_birth,
            'nationality' => $request->nationality,
            'passport' => $request->passport,
            'gender' => $request->gender,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'address' => $request->address,
            'work_place' => $request->work_place,
            'work_phone_number' => $request->work_phone_number,
            'high_school_name' => $request->high_school_name,
            'high_school_graduation_year' => $request->high_school_graduation_year,
            'high_school_major' => $request->high_school_major,
            'university_name' => $request->university_name,
            'university_graduation_year' => $request->university_graduation_year,
            'university_major' => $request->university_major,
            'parent_name' => $request->parent_name,
            'relationship' => $request->relationship,
            'parent_phone_number' => $request->parent_phone_number,
            'parent_email' => $request->parent_email,
            'date3' => $request->date3,
            'date2' => $request->date2,
            'date1' => $request->date1,
            'score3' => $request->score3,
            'score2' => $request->score2,
            'score1' => $request->score1,
            'grade3' => $request->grade3,
            'grade2' => $request->grade2,
            'grade1' => $request->grade1,
            'organization_of_issue3' => $request->organization_of_issue3,
            'organization_of_issue2' => $request->organization_of_issue2,
            'organization_of_issue1' => $request->organization_of_issue1,
            'examination3' => $request->examination3,
            'examination2' => $request->examination2,
            'examination1' => $request->examination1,
        ])->id;

        $user = new User;
        $user->type = 'stu';
        $user->type_id = $student_id;
        $user->phone = $request->phone_number;
        $user->email = $request->email;
        $user->password = Hash::make($request->passport);
        $user->save();   
            
        return redirect('admin/application')->with('flash_message', 'Application added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $application = Application::findOrFail($id);
        $talk_old = Talk::where('application_id', '=', $application->id)->get();
        return view('admin.application.show', compact('application', 'talk_old'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function talk($id, Request $request)
    {
        $application = Application::findOrFail($id);
        

        return view('admin.application.talk', compact('application'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $application = Application::findOrFail($id);

        return view('admin.application.edit', compact('application'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'firstname' => 'required',
            'lastname' => 'required',
            'gender' => 'required',
            'photo' => 'mimes:jpg,png,gif,jpeg',
            'passport' => 'required',
            'nationality' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'faculty_id' => 'required',
            'examination_language' => 'required',
            'date_of_birth' => 'required',
        ]);

        $requestData = $request->all();
        
        $application = Application::findOrFail($id);
        $application->update($requestData);

        return redirect('admin/application')->with('flash_message', 'Application updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Application::destroy($id);

        return redirect('admin/application')->with('flash_message', 'Application deleted!');
    }

    public function import(){

        Excel::import(new StudentInput, 'public/files/application2.xlsx');
        
        return redirect('/')->with('success', 'All good!');
        // echo 111;   
    }

    public function interview(){
        $perPage = 25;
        $application = Application::latest()->where('status',true)->paginate($perPage);
        return view('admin.application.interview', compact('application'));
    }
}
