<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\About;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $about = About::where('logo', 'LIKE', "%$keyword%")
                ->orWhere('tel1', 'LIKE', "%$keyword%")
                ->orWhere('tel2', 'LIKE', "%$keyword%")
                ->orWhere('slogan_uz', 'LIKE', "%$keyword%")
                ->orWhere('slogan_ru', 'LIKE', "%$keyword%")
                ->orWhere('slogan_en', 'LIKE', "%$keyword%")
                ->orWhere('slogan_kr', 'LIKE', "%$keyword%")
                ->orWhere('about_uz', 'LIKE', "%$keyword%")
                ->orWhere('about_ru', 'LIKE', "%$keyword%")
                ->orWhere('about_en', 'LIKE', "%$keyword%")
                ->orWhere('about_kr', 'LIKE', "%$keyword%")
                ->orWhere('facebook', 'LIKE', "%$keyword%")
                ->orWhere('telegram', 'LIKE', "%$keyword%")
                ->orWhere('youtube', 'LIKE', "%$keyword%")
                ->orWhere('address_uz', 'LIKE', "%$keyword%")
                ->orWhere('address_ru', 'LIKE', "%$keyword%")
                ->orWhere('address_en', 'LIKE', "%$keyword%")
                ->orWhere('address_kr', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('location', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $about = About::latest()->paginate($perPage);
        }

        return view('admin.about.index', compact('about'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.about.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        About::create($requestData);

        return redirect('admin/about')->with('flash_message', 'About added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $about = About::findOrFail($id);

        return view('admin.about.show', compact('about'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $about = About::findOrFail($id);

        return view('admin.about.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, About $about)
    {
        $this->validate($request,[
            'logo' => 'mimes:jpeg,bmp,png,jpg',
            'tel1' => 'required',
            'tel2' => 'required',
            'slogan_uz' => 'required',
            'slogan_ru' => 'required',
            'slogan_en' => 'required',
            'slogan_kr' => 'required',
            'facebook' => 'required',
            'telegram' => 'required',
            'youtube' => 'required',
            'address_uz' => 'required',
            'address_ru' => 'required',
            'address_en' => 'required',
            'address_kr' => 'required',
            'email' => 'required',
            'location' => 'required',
        ]);
        
        $image = $request->file('logo');
        if(isset($image))
        {
            //make unipue name for image
            $imageName  = 'logo.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('logo'))
            {
                Storage::disk('public')->makeDirectory('logo');
            }

            //delete old post image
            if(Storage::disk('public')->exists('logo/'.$about->logo))
            {
                Storage::disk('public')->delete('logo/'.$about->logo);
            }
            $logo = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('logo/'.$imageName,$logo);

        } else {
            $imageName = $about->logo;
        }

        // $requestData = $request->all();
        
        // $about = About::findOrFail($id);
        // $about->update($requestData);

        $about->logo = $imageName;
        $about->tel1 = $request->tel1;
        $about->tel2 = $request->tel2;
        $about->slogan_uz = $request->slogan_uz;
        $about->slogan_ru = $request->slogan_ru;
        $about->slogan_en = $request->slogan_en;
        $about->slogan_kr = $request->slogan_kr;
        $about->facebook = $request->facebook;
        $about->telegram = $request->telegram;
        $about->youtube = $request->youtube;
        $about->address_uz = $request->address_uz;
        $about->address_ru = $request->address_ru;
        $about->address_en = $request->address_en;
        $about->address_kr = $request->address_kr;
        $about->rights_uz = $request->rights_uz;
        $about->rights_ru = $request->rights_ru;
        $about->rights_en = $request->rights_en;
        $about->rights_kr = $request->rights_kr;
        $about->email = $request->email;
        $about->location = $request->location;
        $about->save();

        return redirect('admin/about/1/edit')->with('flash_message', 'About updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        About::destroy($id);

        return redirect('admin/about')->with('flash_message', 'About deleted!');
    }
}
