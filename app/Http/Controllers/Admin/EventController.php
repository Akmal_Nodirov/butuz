<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Event;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $event = Event::where('image', 'LIKE', "%$keyword%")
                ->orWhere('title_uz', 'LIKE', "%$keyword%")
                ->orWhere('title_ru', 'LIKE', "%$keyword%")
                ->orWhere('title_en', 'LIKE', "%$keyword%")
                ->orWhere('title_kr', 'LIKE', "%$keyword%")
                ->orWhere('description_uz', 'LIKE', "%$keyword%")
                ->orWhere('description_ru', 'LIKE', "%$keyword%")
                ->orWhere('description_en', 'LIKE', "%$keyword%")
                ->orWhere('description_kr', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $event = Event::latest()->paginate($perPage);
        }

        return view('admin.event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'image' => 'required|image|mimes:jpg,png,jpeg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'content_uz' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_kr' => 'required',
        ]);

        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('events'))
            {
                Storage::disk('public')->makeDirectory('events');
            }

            $event_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('events/'.$imageName,$event_image);

        } else {
            $imageName = "";
        }

        $event = new Event;
        $event->image = $imageName;
        $event->title_uz = $request->title_uz;
        $event->title_ru = $request->title_ru;
        $event->title_en = $request->title_en;
        $event->title_kr = $request->title_kr;
        $event->content_uz = $request->content_uz;
        $event->content_ru = $request->content_ru;
        $event->content_en = $request->content_en;
        $event->content_kr = $request->content_kr;
        $event->published = $request->published;
        $event->save();

        return redirect('admin/event')->with('flash_message', 'Event added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.event.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);

        return view('admin.event.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Event $event)
    {
        
        $this->validate($request,[
            'image' => 'image|mimes:jpg,png,jpeg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'content_uz' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_kr' => 'required',
        ]);

        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('events'))
            {
                Storage::disk('public')->makeDirectory('events');
            }

            if(Storage::disk('public')->exists('events/'.$event->image))
            {
                Storage::disk('public')->delete('events'.$event->image);
            }

            $event_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('events/'.$imageName,$event_image);

        } else {
            $imageName = $event->image;
        }

        $event->image = $imageName;
        $event->title_uz = $request->title_uz;
        $event->title_ru = $request->title_ru;
        $event->title_en = $request->title_en;
        $event->title_kr = $request->title_kr;
        $event->content_uz = $request->content_uz;
        $event->content_ru = $request->content_ru;
        $event->content_en = $request->content_en;
        $event->content_kr = $request->content_kr;
        $event->published = $request->published;
        $event->save();

        return redirect('admin/event')->with('flash_message', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Event $event)
    {
        if(Storage::disk('public')->exists('events/'.$event->image))
        {
            Storage::disk('public')->delete('events'.$event->image);
        }
        $event->delete();

        return redirect('admin/event')->with('flash_message', 'Event deleted!');
    }
}
