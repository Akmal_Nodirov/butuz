<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Program;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $program = Program::where('image', 'LIKE', "%$keyword%")
                ->orWhere('title_uz', 'LIKE', "%$keyword%")
                ->orWhere('title_ru', 'LIKE', "%$keyword%")
                ->orWhere('title_en', 'LIKE', "%$keyword%")
                ->orWhere('title_kr', 'LIKE', "%$keyword%")
                ->orWhere('description_uz', 'LIKE', "%$keyword%")
                ->orWhere('description_ru', 'LIKE', "%$keyword%")
                ->orWhere('description_en', 'LIKE', "%$keyword%")
                ->orWhere('description_kr', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $program = Program::latest()->paginate($perPage);
        }

        return view('admin.program.index', compact('program'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.program.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'image' => 'required|image|mimes:jpeg,png,bmp,jpg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
            'description_kr' => 'required',
            'published' => 'required',
        ]);
        
        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('programs'))
            {
                Storage::disk('public')->makeDirectory('programs');
            }

            $program_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('programs/'.$imageName,$program_image);

        } else {
            $imageName = "";
        }

        $program = new Program;
        $program->image = $imageName;
        $program->title_uz = $request->title_uz;
        $program->title_ru = $request->title_ru;
        $program->title_en = $request->title_en;
        $program->title_kr = $request->title_kr;
        $program->description_uz = $request->description_uz;
        $program->description_ru = $request->description_ru;
        $program->description_en = $request->description_en;
        $program->description_kr = $request->description_kr;
        $program->published = $request->published;
        $program->save();

        return redirect('admin/program')->with('flash_message', 'Program added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $program = Program::findOrFail($id);

        return view('admin.program.show', compact('program'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $program = Program::findOrFail($id);

        return view('admin.program.edit', compact('program'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'image' => 'mimes:jpeg,png,bmp,jpg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'description_uz' => 'required',
            'description_ru' => 'required',
            'description_en' => 'required',
            'description_kr' => 'required',
            'published' => 'required',
        ]);
        
        $image = $request->file('image');
        
        $program = Program::find($id);
        if(isset($image)){

            // make unique name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('programs'))
            {
                Storage::disk('public')->makeDirectory('programs');
            }

            if(Storage::disk('public')->exists('programs/'.$program->image))
            {
                Storage::disk('public')->delete('programs/'.$program->image);
            }

            $program_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('programs/'.$imageName,$program_image);

        } else {
            $imageName = $program->image;
        }

        $program->image = $imageName;
        $program->title_uz = $request->title_uz;
        $program->title_ru = $request->title_ru;
        $program->title_en = $request->title_en;
        $program->title_kr = $request->title_kr;
        $program->description_uz = $request->description_uz;
        $program->description_ru = $request->description_ru;
        $program->description_en = $request->description_en;
        $program->description_kr = $request->description_kr;
        $program->published = $request->published;
        $program->save();

        return redirect('admin/program')->with('flash_message', 'Program updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Program $program)
    {
        if(Storage::disk('public')->exists('programs/'.$program->image))
        {
            Storage::disk('public')->delete('programs/'.$program->image);
        }

        $program->delete();

        return redirect('admin/program')->with('flash_message', 'Program deleted!');
    }
}
