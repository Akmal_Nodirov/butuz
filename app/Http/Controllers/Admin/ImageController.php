<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Image;
use App\Event;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image as Im;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $image = Image::where('image', 'LIKE', "%$keyword%")
                ->orWhere('event_id', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $image = Image::latest()->paginate($perPage);
        }

        return view('admin.image.index', compact('image'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $event = Event::latest()->get();
        return view('admin.image.create',compact('event'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'image' => 'required|image|mimes:jpg,png,jpeg',
            'event_id' => 'required',
        ]);

        $image = $request->file('image');
        if(isset($image)){
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('images'))
            {
                Storage::disk('public')->makeDirectory('images');
            }

            $im = Im::make($image)->save($imageName,100);
            Storage::disk('public')->put('images/'.$imageName,$im);

        } else {
            $imageName = "";
        }

        $image = new Image;
        $image->image = $imageName;
        $image->event_id = $request->event_id;
        $image->published = $request->published;
        $image->save();


        return redirect('admin/image')->with('flash_message', 'Image added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $event = Event::latest()->get();
        $image = Image::findOrFail($id);

        return view('admin.image.show', compact('event','image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $image = Image::findOrFail($id);
        $event = Event::latest()->get();

        return view('admin.image.edit', compact('event','image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'image' => 'image|mimes:jpg,png,jpeg',
            'event_id' => 'required',
        ]);

        $ima = Image::find($id);

        $image = $request->file('image');
        if(isset($image)){
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('images'))
            {
                Storage::disk('public')->makeDirectory('images');
            }

            if(Storage::disk('public')->exists('images/'.$ima->image))
            {
                Storage::disk('public')->delete('images/'.$ima->image);
            }
            $im = Im::make($image)->save($imageName,100);
            Storage::disk('public')->put('images/'.$imageName,$im);

        } else {
            $imageName = $ima->image;
        }

        $ima->image = $imageName;
        $ima->event_id = $request->event_id;
        $ima->published = $request->published;
        $ima->save();

        return redirect('admin/image')->with('flash_message', 'Image updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Image $image)
    {
        if(Storage::disk('public')->exists('images/'.$image->image))
        {
            Storage::disk('public')->delete('images/'.$image->image);
        }
        $image->delete();

        return redirect('admin/image')->with('flash_message', 'Image deleted!');
    }
}
