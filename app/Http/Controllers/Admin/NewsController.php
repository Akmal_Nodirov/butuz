<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\News;
use Illuminate\Http\Request;
use Storage;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $news = News::where('image', 'LIKE', "%$keyword%")
                ->orWhere('title_uz', 'LIKE', "%$keyword%")
                ->orWhere('title_ru', 'LIKE', "%$keyword%")
                ->orWhere('title_en', 'LIKE', "%$keyword%")
                ->orWhere('title_kr', 'LIKE', "%$keyword%")
                ->orWhere('content_uz', 'LIKE', "%$keyword%")
                ->orWhere('content_ru', 'LIKE', "%$keyword%")
                ->orWhere('content_en', 'LIKE', "%$keyword%")
                ->orWhere('content_kr', 'LIKE', "%$keyword%")
                ->orWhere('published', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $news = News::latest()->paginate($perPage);
        }

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'image' => 'required|mimes:jpeg,bmp,png,jpg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'content_uz' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_kr' => 'required',
            'published' => 'required'
        ]);
        
        $image = $request->file('image');
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('news'))
            {
                Storage::disk('public')->makeDirectory('news');
            }

            $news = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('news/'.$imageName,$news);

        } else {
            $imageName = "";
        }

        $news = new News;
        $news->image = $imageName;
        $news->title_uz = $request->title_uz;
        $news->title_ru = $request->title_ru;
        $news->title_en = $request->title_en;
        $news->title_kr = $request->title_kr;
        $news->content_uz = $request->content_uz;
        $news->content_ru = $request->content_ru;
        $news->content_en = $request->content_en;
        $news->content_kr = $request->content_kr;
        $news->published = $request->published;
        $news->save();

        return redirect('admin/news')->with('flash_message', 'News added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'image' => 'mimes:jpeg,bmp,png,jpg',
            'title_uz' => 'required',
            'title_ru' => 'required',
            'title_en' => 'required',
            'title_kr' => 'required',
            'content_uz' => 'required',
            'content_ru' => 'required',
            'content_en' => 'required',
            'content_kr' => 'required',
            // 'published' => 'required'
        ]);
        
        $image = $request->file('image');
        $news = News::find($id);
        if(isset($image))
        {
            // make unipue name for image
            $imageName  = 'getimage'.time().'.'.$image->getClientOriginalExtension();

            if(!Storage::disk('public')->exists('news'))
            {
                Storage::disk('public')->makeDirectory('news');
            }

            if(Storage::disk('public')->exists('news/'.$news->image))
            {
                Storage::disk('public')->delete('news/'.$news->image);
            }

            $news_image = Image::make($image)->save($imageName,100);
            Storage::disk('public')->put('news/'.$imageName,$news_image);

        } 
        else {
            $imageName = $news->image;
        }

        $news->image = $imageName;
        $news->title_uz = $request->title_uz;
        $news->title_ru = $request->title_ru;
        $news->title_en = $request->title_en;
        $news->title_kr = $request->title_kr;
        $news->content_uz = $request->content_uz;
        $news->content_ru = $request->content_ru;
        $news->content_en = $request->content_en;
        $news->content_kr = $request->content_kr;
        $news->published = $request->published;
        $news->save();

        return redirect('admin/news')->with('flash_message', 'News updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(News $news)
    {
        if(Storage::disk('public')->exists('news/'.$news->image))
        {
            Storage::disk('public')->delete('news/'.$news->image);
        }

        $news->delete();

        return redirect('admin/news')->with('flash_message', 'News deleted!');
    }
}
