<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Faculty;
use App\Application;
use App\Semester;
use Illuminate\Http\Request;

class DashboardController extends Controller{

	public function index(Request $request){
		$student = Application::where('id',Auth::user()->type_id)->first();
		return view('student.home',compact('student'));
	}

	public function choosesemester(Request $request){
		$semester = null;
		$student = Application::where('id',Auth::user()->type_id)->first();
		if((isset($_GET['course']) && is_numeric($_GET['course'])) && (isset($_GET['semester']) && is_numeric($_GET['semester']))){
			$semester = Semester::where([
				['course','=',$request->input('course')],
				['semester','=',$request->input('semester')],
				['application_id','=',Auth::user()->type_id]
			])
			->get();
		}
		return view('student.dashboard.choosesemester',compact('semester','student'));
	}
	
	public function print(){
	    $application = Application::findOrFail(Auth::user()->type_id);
	    return view('student.dashboard.print',compact('application'));
	}
}