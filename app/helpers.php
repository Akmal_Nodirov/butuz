<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 08.04.2019
 * Time: 9:07
 */

function sendSms($textMessage)
{

    header("Content-Type: text/html; charset=UTF-8");
    header('Cache-Control: no-store, no-cache');
    header('Expires: ' . date('r'));

    ini_set('soap.wsdl_cache_ttl', 0);
    ini_set("soap.wsdl_cache_enabled", "0");

    include("HttpSMSFunctions.php");
    
    try {
       
$params = [
            'authentication' => SOAP_AUTHENTICATION_DIGEST,
            'cache_wsdl' => 0,
            'trace' => true,
            'exceptions' => true,
            'classmap' => $classmap,
            'ssl' => [
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];
    
        // Connection SOAP Server
        $client = new SoapClient('httpsmsv1.wsdl', $params);

        /*
         *
         * Send SMS ( TransmitSMS )
         *
         * */

        // General params
        $transmit = new TransmitSMSArguments();
        $transmit->username = "puchon";
        $transmit->password = "c4Og0nMd";
        //$transmit->host = "http://but.uz/httpsmsv1.wsdl";

        $service = new ServiceArguments();
        $service->service = 1;
        $transmit->service = $service;
        
        // SMS params
        $messages = array();

        $message = new SMSArguments();
        $message->id = 1;
        $message->phone = "998932498889";
        $message->text = $textMessage;
        $messages[0] = $message;

        $transmit->messages = $messages;
        $transmit->count = count($messages); // 1

        // Send SMS
        $transmitResult = TransmitSMS($client, $transmit);
        echo "<pre>";
        print_r($transmitResult);
        die;
        if ($transmitResult->status == 0) {
            echo "Success";
        } else {
            echo "Error: code={$transmitResult->status}, Message={$transmitResult->errorMsg}";
        }
        

        //
        //          Others exsamples and SOAP functions
        //
        /*
                echo "<pre>";
                $transmitResult = TransmitSMS($client, $transmit);
                echo "send result status = {$transmitResult->status}\n\n";
                var_dump($transmitResult);
                echo "</pre>";
          */

        /*
         *
         * Functions and Types
         *
         * */

        /*echo "<pre>";
        var_dump($client->__getLastRequest());
        var_dump($client->__getLastResponse());
        var_dump($client->__getFunctions());
        var_dump($client->__getTypes());
        echo "</pre>";*/
    } catch (SoapFault $e) {
        echo "<pre>";
        var_dump($e);
        echo "</pre>";

    }
}