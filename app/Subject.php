<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subjects';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['subject', 'type', 'homework', 'bonus', 'credit', 'code'];

    public function professors(){
        return $this->belongsToMany('App\Professor');
    }

    public function semesters(){
        return $this->hasMany('App\Semester');
    }
}
