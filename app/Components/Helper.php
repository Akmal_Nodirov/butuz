<?php
	namespace App\Components;

	class Helper{

		public static function generateNumber($facultet, $lan_txt, $user_id)
	    {
	        $facultet_code = $facultet;

	        $lan_code=0;
	        switch ($lan_txt) {
	            case 'uz':
	                $lan_code = 1;
	            break;

	            case 'ru':
	                $lan_code = 2;
	            break;

	            case 'ko':
	                $lan_code = 3;
	            break;

	            default:
	                $lan_code = 0;
	            break;
	        }

	        $number = "";
	        $num = $facultet_code.''.$lan_code.$user_id;
	        if(strlen($num)<=7){
	            $start=7-strlen($num);
	            $number = array_fill(1, $start, '0');
	            $number = $facultet_code.''.$lan_code.implode("",$number).$user_id;
	        }else{
	            $number = $facultet_code.''.$lan_code.$user_id;
	        }

	        return $number;
	    }
	}