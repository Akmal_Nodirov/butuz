<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'applications';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['order_num','student_id','firstname', 'lastname', 'apply_no', 'photo', 'faculty_id', 'group_id', 'examination_language', 'status', 'date_of_birth', 'nationality', 'passport', 'gender', 'phone_number', 'email', 'address', 'work_place', 'work_phone_number', 'high_school_name', 'high_school_graduation_year', 'high_school_major', 'university_name', 'university_graduation_year', 'university_major', 'parent_name', 'relationship', 'parent_phone_number', 'parent_email', 'date3', 'examination1', 'organization_of_issue1', 'grade1', 'score1', 'date1', 'examination2', 'organization_of_issue2', 'grade2', 'score2', 'date2', 'examination3', 'organization_of_issue3', 'grade3', 'score3', 'date3', 'changed', 'viewable_status','test','interview'];

    public function faculty(){
        return $this->belongsTo('App\Faculty');
    }

    public function group(){
        return $this->belongsTo('App\Group');
    }

    public function semesters(){
        return $this->hasMany('App\Semester');
    }

    public function talk(){
        return $this->hasOne("App\Talk");
    }

}
