<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['course', 'name', 'faculty_id'];

    public function faculty(){
        return $this->belongsTo('App\Faculty');
    }

    public function applications(){
        return $this->hasMany('App\Application');
    }    

    public function professors(){
        return $this->belongsToMany('App\Professor');
    }
}