<?php

namespace App;

class Faq extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'faq';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['question_uz', 'question_ru', 'question_en', 'question_kr', 'answer_uz', 'answer_ru', 'answer_en', 'answer_kr', 'published'];

    
}
