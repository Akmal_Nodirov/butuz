
// $('#student_id').select2({
//     placeholder: "Choose tags...",
//     minimumInputLength: 2,
//     ajax: {
//         url: '/dean/semester/find',
//         dataType: 'json',
//         data: function (params) {
//             return {
//                 q: $.trim(params.term)
//             };
//         },
//         processResults: function (data) {
//             return {
//                 results: data
//             };
//         },
//         cache: true
//     }
// });
$(document).on('input','#late_count',function(){
    var value = Number($(this).val());
    if(value>=12){
        $('#present_count').val(0);
    }
    else if(value>=9){
        $('#present_count').val($('#present_count').val()-6);
    }
    else if(value>=6){
        $('#present_count').val($('#present_count').val()-4);
    }
    else if(value>=3){
        $('#present_count').val($('#present_count').val()-2);
    }
    else{
        $('#present_count').val(20);
    }

    // switch(value){
    //     case 3:
    //         $('#present_count').val($('#present_count').val()-2);
    //         break;
    //     case 6:
    //         $('#present_count').val($('#present_count').val()-4);
    //         break;
    //     case 9:
    //         $('#present_count').val($('#present_count').val()-6);
    //         break;
    //     case 12:
    //         $('#present_count').val(0);
    //         break;            
    //     default:
    //         $('#present_count').val(20);
    // }
});

$(document).on('input','#absent_count',function(){
    var value = Number($(this).val());
    
    if(value>=4){
        $('#present_count').val(0);
    }
    else if(value>=3){
        $('#present_count').val($('#present_count').val()-6);
    }
    else if(value>=2){
        $('#present_count').val($('#present_count').val()-4);
    }
    else if(value>=1){
        $('#present_count').val($('#present_count').val()-2);
    }
    else{
        $('#present_count').val(20);
    }

    // switch(value){
    //     case 1:
    //         $('#present_count').val($('#present_count').val()-2);
    //         break;
    //     case 2:
    //         $('#present_count').val($('#present_count').val()-4);
    //         break;
    //     case 3:
    //         $('#present_count').val($('#present_count').val()-6);
    //         break;
    //     case 4:
    //         $('#present_count').val(0);
    //         break;            
    //     default:
    //         $('#present_count').val(20);
    // }
});

$(document).on('input', '#present_count, #homework, #middle_exam, #final_exam', function(){
    var present_count = Number($('#present_count').val());
    var homework = Number($('#homework').val());
    var middle_exam = Number($('#middle_exam').val());
    var final_exam = Number($('#final_exam').val());

    var total = present_count+homework+middle_exam+final_exam;
    
    if(total>=95 && total<=100){
        $('#mark').val('A+');
    }
    else if(total>=90 && total<=94){
        $('#mark').val('A');
    }
    else if(total>=85 && total<=89){
        $('#mark').val('B+');
    }
    else if(total>=80 && total<=84){
        $('#mark').val('B');
    }
    else if(total>=75 && total<=79){
        $('#mark').val('C+');
    }
    else if(total>=70 && total<=74){
        $('#mark').val('C');
    }
    else if(total>=65 && total<=69){
        $('#mark').val('D+');
    }
    else if(total>=60 && total<=64){
        $('#mark').val('D');
    }
    else if(total<=59){
        $('#mark').val('D');
    }
    $('#total').val(total);
});

$(document).on('change','#faculty_id', function(){
    $.get(
        '/dean/getgroup',
        {
            'faculty_id' : $(this).val()
        },
        function(response){
            $('#group_id').html(response);
        }
    )
}).change();

$(document).on('change','#group_id', function(){
    $.get(
        '/professor/getstudent',
        {
            'group_id' : $(this).val()
        },
        function(response){
            $('#application_id').html(response);
        }
    )
}).change();

function sendFile(file, el) {
    var  data = new FormData();
    data.append("file", file);
    var url = '/admin/upload';
    $.ajax({
        data: data,
        type: "POST",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function(url2) {
            el.summernote('insertImage', url2);
            // alert('uploaded');
        }
    });
}