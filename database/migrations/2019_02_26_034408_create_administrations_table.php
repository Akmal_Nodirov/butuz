<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdministrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('image')->nullable();
            $table->string('fullname_uz')->nullable();
            $table->string('fullname_ru')->nullable();
            $table->string('fullname_en')->nullable();
            $table->string('fullname_kr')->nullable();
            $table->string('phone')->nullable();
            $table->string('position_id')->nullable();
            $table->string('published');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('administrations');
    }
}
