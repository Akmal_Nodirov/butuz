<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('question_uz')->nullable();
            $table->text('question_ru')->nullable();
            $table->text('question_en')->nullable();
            $table->text('question_kr')->nullable();
            $table->text('answer_uz')->nullable();
            $table->text('answer_ru')->nullable();
            $table->text('answer_en')->nullable();
            $table->text('answer_kr')->nullable();
            $table->string('published');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('faqs');
    }
}
