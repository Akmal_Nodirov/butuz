<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('logo')->nullable();
            $table->string('tel1')->nullable();
            $table->string('tel2')->nullable();
            $table->string('slogan_uz')->nullable();
            $table->string('slogan_ru')->nullable();
            $table->string('slogan_en')->nullable();
            $table->string('slogan_kr')->nullable();
            $table->text('about_uz')->nullable();
            $table->text('about_ru')->nullable();
            $table->text('about_en')->nullable();
            $table->text('about_kr')->nullable();
            $table->string('facebook')->nullable();
            $table->string('telegram')->nullable();
            $table->string('youtube')->nullable();
            $table->text('address_uz')->nullable();
            $table->text('address_ru')->nullable();
            $table->text('address_en')->nullable();
            $table->text('address_kr')->nullable();
            $table->string('email')->nullable();
            $table->text('location')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('abouts');
    }
}
