<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('apply_no');
            $table->string('photo')->nullable();
            $table->integer('faculty_id');
            $table->integer('group_id')->nullable();
            $table->string('examination_language')->nullable();
            $table->boolean('status')->default(0);
            $table->date('date_of_birth')->nullable();
            $table->string('nationality')->nullable();
            $table->string('passport')->nullable();
            $table->boolean('gender')->nullable();
            $table->bigInteger('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('work_place')->nullable();
            $table->string('work_phone_number')->nullable();
            $table->string('high_school_name')->nullable();
            $table->integer('high_school_graduation_year')->nullable();
            $table->string('high_school_major')->nullable();
            $table->string('university_name')->nullable();
            $table->integer('university_graduation_year')->nullable();
            $table->string('university_major')->nullable();
            $table->string('parent_name')->nullable();
            $table->string('relationship')->nullable();
            $table->bigInteger('parent_phone_number')->nullable();
            $table->string('parent_email')->nullable();
            $table->date('date3')->nullable();
            $table->date('date2')->nullable();
            $table->date('date1')->nullable();
            $table->string('score3')->nullable();
            $table->string('score2')->nullable();
            $table->string('score1')->nullable();
            $table->string('grade3')->nullable();
            $table->string('grade2')->nullable();
            $table->string('grade1')->nullable();
            $table->string('organization_of_issue3')->nullable();
            $table->string('organization_of_issue2')->nullable();
            $table->string('organization_of_issue1')->nullable();
            $table->string('examination3')->nullable();
            $table->string('examination2')->nullable();
            $table->string('examination1')->nullable();
            $table->boolean('changed')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
