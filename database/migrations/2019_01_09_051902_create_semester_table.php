<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSemesterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semester', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course');
            $table->integer('semester');
            $table->integer('subject_id');
            $table->integer('application_id');
            $table->integer('professor_id');
            $table->integer('late_count');
            $table->integer('absent_count');
            $table->integer('present_count');
            $table->integer('homework')->nullable();
            $table->integer('middle_exam');
            $table->integer('final_exam');
            $table->integer('total');
            $table->string('mark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semester');
    }
}
