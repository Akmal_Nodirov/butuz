<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/lang/{lang}',function($lang){
	Session::put('lang',$lang);
	return redirect()->back();
});

Route::get('/language/{lang}',function($lang){
	Session::put('lang',$lang);
	return redirect('/choose_application');
});

View::composer('layouts.app',function ($view) {
    $about = App\About::findOrFail(1);
    $pages = App\Page::published()->get();
    $view->with([
    	'about' => $about,
    	'pages' => $pages
    ]);
});

Route::get('/check_auth', 'HomeController@check_auth');
// Route::get('/students_to_users', 'HomeController@students_to_users');

Auth::routes();

Route::get('/home', 'HomeController@home_page')->name('home');
Route::get('/about/{id}','HomeController@about');
Route::get('/program/{id}','HomeController@program');
Route::get('/administration','HomeController@administration');
Route::get('/education/{id}','HomeController@education');
Route::get('/faculties','HomeController@faculties');
Route::get('/announcement/{id}','HomeController@announcement');
Route::get('/archive/{type}/{year}', 'HomeController@archive');
Route::get('/announcements','HomeController@announcements');
Route::get('/news/{id}','HomeController@news');
Route::get('/all_news','HomeController@all_news');
Route::get('/faq','HomeController@faq');
Route::get('/contact','HomeController@contact');
Route::post('/send_message','HomeController@send_message');
Route::get('/events','HomeController@events');
Route::get('/event/{id}','HomeController@event');
Route::get('/choose_application','HomeController@choose_application');
Route::get('/agreement','HomeController@agreement');
Route::post('/i_agree','HomeController@i_agree');
Route::get('/application','HomeController@application');
Route::post('/send_application', 'HomeController@send_application');
Route::get('/rights_and_duties', 'HomeController@rights_and_duties');
// Route::get('/test','HomeController@test');
Route::put('/update_application/{id}', 'HomeController@update_application');
Route::get('/to_login','HomeController@to_login');

Route::group(['as'=>'dean.','prefix'=>'dean','namespace'=>'Dean','middleware'=>['auth','dean']], function(){
	Route::get('dashboard','DashboardController@index')->name('dashboard');
	Route::resource('application', 'ApplicationController');
	Route::resource('faculty', 'FacultyController');
	Route::get('/getgroup','GroupController@getgroup');
	Route::resource('group', 'GroupController');
	Route::resource('professor', 'ProfessorController');
	Route::resource('semester', 'SemesterController');
	Route::resource('subject', 'SubjectController');
	Route::resource('user', 'UserController');
	Route::get('professorsBySubject/{subject_id}','SubjectController@professorsBySubject');
	Route::get('application_import','ApplicationController@import');
	Route::get('settings','UserController@settings');
	Route::put('update_password','UserController@update_password');
	Route::get('student_results/{student_id}','DashboardController@student_results');
	Route::get('/new_mark','SemesterController@new_mark');
	Route::post('/save_mark','SemesterController@save_mark');
	Route::put('/update_mark','SemesterController@update_mark');
	Route::get('/students_results','DashboardController@students_results');
});

Route::group(['as'=>'professor.','prefix'=>'professor','namespace'=>'Professor','middleware'=>['auth','professor']], function(){
	Route::get('dashboard','DashboardController@index')->name('dashboard');
	Route::get('/choose_semester','DashboardController@choosesemester');
	Route::get('/getstudent','ApplicationController@getstudent');
	Route::get('/new_mark','SemesterController@new_mark');
	Route::post('/save_mark','SemesterController@save_mark');
	Route::put('/update_mark','SemesterController@update_mark');
	Route::get('settings','UserController@settings');
	Route::put('update_password','UserController@update_password');
});

Route::group(['as'=>'student.','prefix'=>'student','namespace'=>'Student','middleware'=>['auth','student']], function(){
	Route::get('dashboard','DashboardController@index')->name('dashboard');
	Route::get('/choose_semester','DashboardController@choosesemester');
	Route::get('settings','UserController@settings');
	Route::put('update_password','UserController@update_password');
	Route::get('print','DashboardController@print');
	Route::get('change_data','ApplicationController@change_data');
});

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth']], function(){
	Route::get('dashboard','DashboardController@index')->name('dashboard');
	Route::resource('application', 'ApplicationController');
	Route::resource('/talk', 'ApplicationController');
	Route::put('set_test','HomeController@set_test');
	Route::get('set_test_ajax','HomeController@set_test_ajax');
	Route::get('set_status_ajax','HomeController@set_status_ajax');
	Route::put('send_sms','HomeController@send_sms');

	Route::put('interview','HomeController@interview');
	Route::get('getInterviewData', 'HomeController@getInterviewData');
	Route::resource('faculty', 'FacultyController');
	Route::get('/getgroup','GroupController@getgroup');
	Route::resource('group', 'GroupController');
	Route::resource('professor', 'ProfessorController');
	Route::resource('semester', 'SemesterController');
	Route::resource('subject', 'SubjectController');
	Route::resource('user', 'UserController');
	Route::get('professorsBySubject/{subject_id}','SubjectController@professorsBySubject');
	Route::get('application_import','ApplicationController@import');
	Route::get('settings','UserController@settings');
	Route::put('update_password','UserController@update_password');
	Route::get('student_results/{student_id}','DashboardController@student_results');
	Route::get('/new_mark','SemesterController@new_mark');
	Route::post('/save_mark','SemesterController@save_mark');
	Route::put('/update_mark','SemesterController@update_mark');
	Route::get('/students_results','DashboardController@students_results');

	Route::resource('about', 'AboutController');
	Route::resource('slider', 'SliderController');
	Route::resource('news', 'NewsController');
	Route::resource('program', 'ProgramController');
	Route::resource('faq', 'FaqController');
	Route::resource('event', 'EventController');
	Route::resource('image', 'ImageController');
	Route::resource('message', 'MessageController');
	Route::resource('announcement', 'AnnouncementController');
	Route::resource('page', 'PageController');
	Route::resource('position', 'PositionController');
	Route::resource('administration', 'AdministrationController');
	Route::get('applicant','DashboardController@applicant');
	Route::get('appresult','DashboardController@app_result');
	Route::get('interviewers','DashboardController@interviewers');
	Route::get('applicantsms','DashboardController@applicantsms');
	Route::get('excel','DashboardController@excel');
	Route::post('upload','HomeController@upload')->name('upload');
	Route::put('set_payment/{id}','HomeController@set_payment');
	Route::put('set_pass/{id}','HomeController@set_pass');
    // Route::get('/applicants','HomeController@applicants');

});

// View::composer('layouts.admin.app',function ($view) {
//     $unread_messages = App\Message::where('seen',false)->count();
//     $unanswered_messages = App\Message::where('answer', '=', '')->count();
//     $view->with([
//     	'unaread_messages' => $unread_messages,
//     	'unanswered_messages' => $unanswered_messages
//     ]);
// });
